<?php
date_default_timezone_set('America/Bogota');
require_once 'conexion.php';

$sql = "SELECT iv.*, 
(SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
(SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
(SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
(SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado,
IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg, 
(SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
FROM inventario iv WHERE iv.id_categoria = 1;";

$result = $mysqli->query($sql);
$fecha_hoy = date('Y-m-d');


while ($row = $result->fetch_assoc()) {
    $id_inventario = $row['id'];
    $descripcion = $row['descripcion'];
    $marca = $row['marca'];
    $modelo = $row['modelo'];
    $codigo = $row['codigo'];
    $usuario = $row['usuario'];
    $area = $row['area'];
    $estado = $row['nombre_estado'];
    $observacion = $row['observacion'];
    $id_user = $row['id_user'];
    $id_area = $row['id_area'];
    $id_categoria = $row['id_categoria'];
    $frecuencia = $row['frecuencia'];

    $fecha = date('Y-m-d', strtotime($row['ultimo_mant']));
    $nuevafecha = strtotime('+' . $frecuencia . ' month', strtotime($fecha));
    $nuevafecha = date('Y-m-d', $nuevafecha);

    if ($fecha_hoy == $nuevafecha) {
        $reporte = $mysqli->query("INSERT INTO reportes (id_inventario, estado, id_area, id_user, tipo_reporte) 
                    VALUES ('$id_inventario', '6', '$id_area', '$id_user', '2');");

        $actualizar = $mysqli->query("UPDATE inventario SET estado = 6 WHERE id = '$id_inventario';");
    }
}
$result->free_result();
