<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permisos = $instancia_permiso->permisosUsuarioControl(2, 23, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="#" onclick="window.history.go(-1); return false;" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Gestión humana
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php
                        $permisos = $instancia_permiso->permisosUsuarioControl(3, 24, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>recursos/certificados">
                                <div class="card border-left-success shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Certificados</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-list-alt fa-2x text-success"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(3, 25, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>recursos/solicitados">
                                <div class="card border-left-info shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Certificados solicitados</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-poll-h fa-2x text-info"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 26, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>contabilidad/index">
                                <div class="card border-left-yellow shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Volantes de pago</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-money-check-alt fa-2x text-yellow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(4, 27, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>contabilidad/historial?usuario=<?= base64_encode($id_log) ?>">
                                <div class="card border-left-yellow shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Historial volantes de pago</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-money-check-alt fa-2x text-yellow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>