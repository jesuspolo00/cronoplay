<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlInventario::singleton_inventario();
$instancia_areas = ControlAreas::singleton_areas();
$permisos = $instancia_permiso->permisosUsuarioControl(2, 15, 1, $id_log);

$datos_areas = $instancia_areas->mostrarAreasControl($id_super_empresa);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>mantenimientos/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Mantenimientos preventivos (Areas)
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="form-group col-lg-5">
                                <select name="area" class="form-control" id="" required data-tooltip="tooltip" title="Areas">
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_areas as $area) {
                                        $id_area = $area['id'];
                                        $nombre = $area['nombre'];
                                        $estado = $area['activo'];

                                        $ver = ($estado == 1) ? '' : 'd-none';

                                    ?>
                                        <option value="<?= $id_area ?>"><?= $nombre ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-sm mt-1 ml-1">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-search"></i>
                                    &nbsp;
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                    <?php
                    if (isset($_POST['area'])) {
                        $id_area = $_POST['area'];
                        $datos_articulo = $instancia->articulosComputoAreaControl($id_area);
                    ?>
                        <div class="table-responsive mt-2">
                            <button type="button" class="btn btn-success btn-sm mb-4" data-toggle="modal" data-target="#pro_area">
                                <i class="fa fa-clock"></i>
                                &nbsp;
                                Programar area
                            </button>
                            <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">ID</th>
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Area</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Marca</th>
                                        <th scope="col">Modelo</th>
                                        <th scope="col">Codigo</th>
                                        <th scope="col">Frecuencia mantenimiento</th>
                                        <th scope="col">Fecha proximo mantenimiento</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar">
                                    <?php
                                    if (count($datos_articulo) <= 0) {
                                    ?>
                                        <tr class="text-center">
                                            <td colspan="8">No hay resultados</td>
                                        </tr>
                                        <?php
                                    } else {
                                        foreach ($datos_articulo as $inventario) {
                                            $id_inventario = $inventario['id'];
                                            $descripcion = $inventario['descripcion'];
                                            $marca = $inventario['marca'];
                                            $modelo = $inventario['modelo'];
                                            $codigo = $inventario['codigo'];
                                            $usuario = $inventario['usuario'];
                                            $area = $inventario['area'];
                                            $estado = $inventario['estado'];
                                            $observacion = $inventario['observacion'];
                                            $id_user = $inventario['id_user'];
                                            $id_area = $inventario['id_area'];
                                            $id_categoria = $inventario['id_categoria'];
                                            $frecuencia = $inventario['frecuencia'];


                                            $fecha = date('Y-m-d H:i:s', strtotime($inventario['ultimo_mant']));
                                            $nuevafecha = strtotime('+' . $frecuencia . ' month', strtotime($fecha));
                                            $nuevafecha = date('Y-m-d H:i:s', $nuevafecha);

                                            if ($id_categoria == 1) {
                                                $hoja_vida = '<a href="' . BASE_URL . 'hoja_vida/index?inventario=' . base64_encode($id_inventario) . '">' . $descripcion . '</a>';
                                            } else {
                                                $hoja_vida = $descripcion;
                                            }

                                            if ($estado == 3 || $estado == 1) {
                                                $visible_descargar = 'd-none';
                                                $visible_mant = '';
                                            } else {
                                                $visible_mant = 'd-none';
                                                $visible_descargar = '';
                                            }

                                        ?>
                                            <tr class="text-center">
                                                <td><?= $id_inventario ?></td>
                                                <td><?= $usuario ?></td>
                                                <td><?= $area ?></td>
                                                <td><?= $hoja_vida ?></td>
                                                <td><?= $marca ?></td>
                                                <td><?= $modelo ?></td>
                                                <td><?= $codigo ?></td>
                                                <td><?= $frecuencia ?> Meses</td>
                                                <td><?= $nuevafecha ?></td>
                                                <td>
                                                    <button class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Programar mantenimiento" data-toggle="modal" data-target="#mant_pro<?= $id_inventario ?>">
                                                        <i class="fa fa-clock"></i>
                                                    </button>
                                                </td>
                                                <td class="<?= $visible_mant ?>">
                                                    <button class="btn btn-warning btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Mantenimiento" data-toggle="modal" data-target="#mant_inv<?= $id_inventario ?>">
                                                        <i class="fas fa-wrench"></i>
                                                    </button>
                                                </td>
                                                <td class="<?= $visible_descargar ?>">
                                                    <a href="<?= BASE_URL ?>imprimir/reporte?inventario=<?= base64_encode($id_inventario) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Descargar reporte">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                </td>
                                            </tr>


                                            <!-- Mantenimiento inventario -->
                                            <div class="modal fade" id="mant_inv<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-md" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Reportar mantenimiento articulo</h5>
                                                        </div>
                                                        <form method="POST">
                                                            <div class="modal-body border-0">
                                                                <div class="row p-2">
                                                                    <input type="hidden" value="<?= $id_super_empresa ?>" name="id_super_empresa">
                                                                    <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_mant">
                                                                    <input type="hidden" value="<?= $id_log ?>" name="id_log_mant">
                                                                    <input type="hidden" value="<?= $id_user ?>" name="id_user_mant">
                                                                    <input type="hidden" value="<?= $id_area ?>" name="id_area_mant">
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Area</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $area ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Descripcion</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Marca</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Modelo</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Responsable</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $usuario ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>No. Serie</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-12">
                                                                        <label>Fecha</label>
                                                                        <input type="datetime" name="fecha" class="form-control" required value="<?= date("Y-m-d H:i:s"); ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-12">
                                                                        <label>Observacion</label>
                                                                        <textarea name="observacion" class="form-control" maxlength="1000" cols="30" rows="5"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer border-0">
                                                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                                    <i class="fa fa-times"></i>
                                                                    &nbsp;
                                                                    Cerrar
                                                                </button>
                                                                <button type="submit" class="btn btn-success btn-sm">
                                                                    <i class="fas fa-wrench"></i>
                                                                    &nbsp;
                                                                    Mantenimiento
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!------------------------------------------------------->


                                            <!-- Mantenimiento inventario -->
                                            <div class="modal fade" id="mant_pro<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-md" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Programar mantenimiento articulo</h5>
                                                        </div>
                                                        <form method="POST">
                                                            <div class="modal-body border-0">
                                                                <div class="row p-2">
                                                                    <input type="hidden" value="<?= $id_super_empresa ?>" name="id_super_empresa">
                                                                    <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario">
                                                                    <input type="hidden" value="<?= $id_log ?>" name="id_log">
                                                                    <input type="hidden" value="<?= $id_user ?>" name="id_user">
                                                                    <input type="hidden" value="<?= $id_area ?>" name="id_area">
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Area</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $area ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Descripcion</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Marca</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Modelo</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>Responsable</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $usuario ?>">
                                                                    </div>
                                                                    <div class="form-group col-lg-6">
                                                                        <label>No. Serie</label>
                                                                        <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <label>Frecuencia mantenimiento (Meses)</label>
                                                                        <input type="text" name="frec_mant" class="form-control numeros" maxlength="2" minlength="1" required value="<?= $frecuencia; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer border-0">
                                                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                                    <i class="fa fa-times"></i>
                                                                    &nbsp;
                                                                    Cerrar
                                                                </button>
                                                                <button type="submit" class="btn btn-success btn-sm">
                                                                    <i class="fas fa-redo-alt"></i>
                                                                    &nbsp;
                                                                    Actualizar
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!------------------------------------------------------->
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- Programar Area -->
<div class="modal fade" id="pro_area" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Programar area</h5>
            </div>
            <form method="POST">
                <div class="modal-body border-0">
                    <div class="row p-2">
                        <div class="col-lg-12">
                            <?php
                            foreach ($datos_articulo as $id) {
                                $id_inventario = $id['id'];
                            ?>
                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_area[]">
                            <?php
                            }
                            ?>
                            <input type="hidden" value="<?= $id_log ?>" name="id_log_area">
                            <div class="form-group">
                                <label>Cantidad de articulos</label>
                                <input type="text" class="form-control" disabled value="<?= count($datos_articulo) ?>">
                            </div>
                            <div class="form-group">
                                <label>Frecuencia de mantenimiento</label>
                                <input type="text" class="form-control numeros" required name="frec_mant" maxlength="2" minlength="1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cerrar
                    </button>
                    <button type="submit" class="btn btn-success btn-sm">
                        <i class="fas fa-redo-alt"></i>
                        &nbsp;
                        Actualizar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
if (isset($_POST['id_inventario_mant'])) {
    $instancia->mantenimientoArticuloControl();
}

if (isset($_POST['id_inventario'])) {
    $instancia->programarMantenimientoArticuloControl();
}

if (isset($_POST['id_log_area'])) {
    $instancia->programarMantenimientoAreaControl();
}
