<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permisos = $instancia_permiso->permisosUsuarioControl(2, 1, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary">Configuracion</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>usuarios/index">
                                <div class="card border-left-success shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-users fa-2x text-success"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }

                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 3, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>inventario/index">
                                <div class="card border-left-dark shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Inventario</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-barcode fa-2x text-dark"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>areas/index">
                                <div class="card border-left-info shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Areas</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-map-marker-alt fa-2x text-info"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 9, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>areas/reasignar">
                                <div class="card border-left-orange shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Re-asignar area</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-sync-alt fa-2x text-orange"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 10, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>mantenimientos/index">
                                <div class="card border-left-danger shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Mantenimientos preventivos</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-tools fa-2x text-danger"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 11, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>categorias/index">
                                <div class="card border-left-warning shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Categorias</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-list-ul fa-2x text-warning"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 12, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>listado/index">
                                <div class="card border-left-purple shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Listado</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-list-ol fa-2x text-purple"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 14, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>historial/index">
                                <div class="card border-left-secondary shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Historial inventario</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-history fa-2x text-secondary"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 17, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>inventario/trabajoCasa">
                                <div class="card border-left-blue shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Trabajo en casa</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-briefcase fa-2x text-blue"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 18, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>material/index">
                                <div class="card border-left-pink shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Material didáctico</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-cubes fa-2x text-pink"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 19, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>pmb/index">
                                <div class="card border-left-brown shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Libros prestados</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-book fa-2x text-brown"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 22, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>admisiones/index">
                                <div class="card border-left-pink-white shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Admisiones</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-notes-medical fa-2x text-pink-white"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 23, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>recursos/index">
                                <div class="card border-left-green-dark shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Gestión humana</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-id-card-alt fa-2x text-green-dark"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 26, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>contabilidad/index">
                                <div class="card border-left-yellow shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Contabilidad</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-money-check-alt fa-2x text-yellow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>