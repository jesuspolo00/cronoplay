<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();
$permisos = $instancia_permiso->permisosUsuarioControl(1, 16, 1, $id_log);

$datos_articulos = $instancia->mostrarArticulosUsuarioControl($id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Inventario
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group input-group-sm mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th colspan="7">Listado inventario</th>
                                </tr>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">AREA</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">MARCA</th>
                                    <th scope="col">CODIGO</th>
                                    <!-- <th scope="col">ESTADO</th> -->
                                    <th scope="col">OBSERVACION</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_articulos as $articulo) {
                                    $id_inventario = $articulo['id'];
                                    $descripcion = $articulo['descripcion'];
                                    $marca = $articulo['marca'];
                                    $modelo = $articulo['modelo'];
                                    $usuario = $articulo['usuario'];
                                    $area = $articulo['area'];
                                    $codigo = $articulo['codigo'];
                                    $estado = $articulo['nombre_estado'];
                                    $id_estado = $articulo['estado'];
                                    $observacion = $articulo['observacion'];
                                    $id_area = $articulo['id_area'];
                                    $id_user = $articulo['id_user'];

                                    if ($id_estado == 2) {
                                        $visible_rep = 'd-none';
                                        $span = '<span class="badge badge-success">Reportado</span>';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 3) {
                                        $visible_rep = '';
                                        $span = '';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 6) {
                                        $visible_rep = 'd-none';
                                        $span = '<span class="badge badge-warning">Mantenimiento preventivo</span>';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 1) {
                                        $visible_rep = '';
                                        $span = '';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 5) {
                                        $visible_trab = 'd-none';
                                        $visible_rep = 'd-none';
                                        $span = '<span class="badge badge-danger">Descontinuado</span>';
                                    }

                                    if ($id_estado != 8 && $id_estado != 5) {
                                        $visible_rep = '';
                                        $visible_trab = '';
                                        $span = '';
                                        $bg_color = '';

                                ?>
                                        <tr class="text-center <?= $bg_color ?>">
                                            <td><?= $area ?></td>
                                            <td><?= $descripcion ?></td>
                                            <td><?= $marca ?></td>
                                            <td><?= $codigo ?></td>
                                            <!-- <td><?= $estado ?></td> -->
                                            <td><?= $observacion ?></td>
                                            <td>
                                                <button class="btn btn-success btn-sm <?= $visible_rep ?>" data-tooltip="tooltip" data-placement="bottom" title="Reportar" data-toggle="modal" data-target="#rep_inv<?= $id_inventario ?>">
                                                    <i class="fas fa-clipboard-check"></i>
                                                </button>
                                                <?= $span ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm <?= $visible_trab ?>" data-tooltip="tooltip" data-placement="bottom" title="Trabajo en casa" data-toggle="modal" data-target="#trab_home<?= $id_inventario ?>">
                                                    <i class="fas fa-briefcase"></i>
                                                </button>
                                            </td>
                                            <td>
                                                <a href="<?= BASE_URL ?>listado/historial?inventario=<?= base64_encode($id_inventario) ?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver mas">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>

                                        <!-- Reportar inventario -->
                                        <div class="modal fade" id="rep_inv<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-md" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Reportar articulo</h5>
                                                    </div>
                                                    <form method="POST">
                                                        <div class="modal-body border-0">
                                                            <div class="row p-2">
                                                                <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa_rep">
                                                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_rep">
                                                                <input type="hidden" value="<?= $id_log ?>" name="id_log_rep">
                                                                <input type="hidden" value="<?= $id_user ?>" name="id_user_rep">
                                                                <input type="hidden" value="<?= $id_area ?>" name="id_area_rep">
                                                                <div class="form-group col-lg-6">
                                                                    <label>Area</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $area ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Descripcion</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Marca</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Modelo</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Responsable</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $usuario ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>No. Serie</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label>Observacion</label>
                                                                    <textarea name="observacion" class="form-control" maxlength="1000" cols="30" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <i class="fas fa-clipboard-check"></i>
                                                                &nbsp;
                                                                Reportar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!------------------------------------------------------->


                                        <!-- Reportar inventario -->
                                        <div class="modal fade" id="trab_home<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-md" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Trabajo en casa</h5>
                                                    </div>
                                                    <form method="POST">
                                                        <div class="modal-body border-0">
                                                            <div class="row p-2">
                                                                <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa_trab_home">
                                                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_trab_home">
                                                                <input type="hidden" value="<?= $id_log ?>" name="id_log_trab_home">
                                                                <input type="hidden" value="<?= $id_user ?>" name="id_user_trab_home">
                                                                <input type="hidden" value="<?= $id_area ?>" name="id_area_trab_home">
                                                                <div class="form-group col-lg-6">
                                                                    <label>Area</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $area ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Descripcion</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Marca</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Modelo</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Responsable</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $usuario ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>No. Serie</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <i class="fas fa-briefcase"></i>
                                                                &nbsp;
                                                                Aceptar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!------------------------------------------------------->

                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive mt-4">
                        <table class="table table-hover table-striped text-lowercase table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th colspan="7">Trabajo en casa</th>
                                </tr>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">AREA</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">MARCA</th>
                                    <th scope="col">CODIGO</th>
                                    <!-- <th scope="col">ESTADO</th> -->
                                    <th scope="col">OBSERVACION</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_articulos as $articulo) {
                                    $id_inventario = $articulo['id'];
                                    $descripcion = $articulo['descripcion'];
                                    $marca = $articulo['marca'];
                                    $modelo = $articulo['modelo'];
                                    $usuario = $articulo['usuario'];
                                    $area = $articulo['area'];
                                    $codigo = $articulo['codigo'];
                                    $estado = $articulo['nombre_estado'];
                                    $id_estado = $articulo['estado'];
                                    $observacion = $articulo['observacion'];
                                    $id_area = $articulo['id_area'];
                                    $id_user = $articulo['id_user'];

                                    if ($id_estado == 2) {
                                        $visible_rep = 'd-none';
                                        $span = '<span class="badge badge-success">Reportado</span>';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 3) {
                                        $visible_rep = '';
                                        $span = '';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 6) {
                                        $visible_rep = 'd-none';
                                        $span = '<span class="badge badge-warning">Mantenimiento preventivo</span>';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 1) {
                                        $visible_rep = '';
                                        $span = '';
                                        $visible_trab = '';
                                        $span_casa = '';
                                        $bg_color = '';
                                    }

                                    if ($id_estado == 5) {
                                        $visible_rep = 'd-none';
                                        $visible_trab = 'd-none';
                                        $span = '<span class="badge badge-danger">Descontinuado</span>';
                                    }

                                    if ($id_estado == 8) {
                                        $visible_rep = '';
                                        $visible_trab = 'd-none';
                                        $span_casa = '<span class="badge badge-primary">Trabajo en casa</span>';
                                ?>
                                        <tr class="text-center">
                                            <td><?= $area ?></td>
                                            <td><?= $descripcion ?></td>
                                            <td><?= $marca ?></td>
                                            <td><?= $codigo ?></td>
                                            <!-- <td><?= $estado ?></td> -->
                                            <td><?= $observacion ?></td>
                                            <td>
                                                <?= $span_casa ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-sm <?= $visible_rep ?>" data-tooltip="tooltip" data-placement="bottom" title="Reportar" data-toggle="modal" data-target="#rep_inv<?= $id_inventario ?>">
                                                    <i class="fas fa-clipboard-check"></i>
                                                </button>
                                                <?= $span ?>
                                            </td>
                                        </tr>

                                        <!-- Reportar inventario -->
                                        <div class="modal fade" id="rep_inv<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-md" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Reportar articulo</h5>
                                                    </div>
                                                    <form method="POST">
                                                        <div class="modal-body border-0">
                                                            <div class="row p-2">
                                                                <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa_rep">
                                                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_rep">
                                                                <input type="hidden" value="<?= $id_log ?>" name="id_log_rep">
                                                                <input type="hidden" value="<?= $id_user ?>" name="id_user_rep">
                                                                <input type="hidden" value="<?= $id_area ?>" name="id_area_rep">
                                                                <div class="form-group col-lg-6">
                                                                    <label>Area</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $area ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Descripcion</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Marca</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Modelo</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Responsable</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $usuario ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>No. Serie</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label>Observacion</label>
                                                                    <textarea name="observacion" class="form-control" maxlength="1000" cols="30" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <i class="fas fa-clipboard-check"></i>
                                                                &nbsp;
                                                                Reportar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!------------------------------------------------------->


                                        <!-- Reportar inventario -->
                                        <div class="modal fade" id="trab_home<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-md" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Trabajo en casa</h5>
                                                    </div>
                                                    <form method="POST">
                                                        <div class="modal-body border-0">
                                                            <div class="row p-2">
                                                                <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa_trab_home">
                                                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_trab_home">
                                                                <input type="hidden" value="<?= $id_log ?>" name="id_log_trab_home">
                                                                <input type="hidden" value="<?= $id_user ?>" name="id_user_trab_home">
                                                                <input type="hidden" value="<?= $id_area ?>" name="id_area_trab_home">
                                                                <div class="form-group col-lg-6">
                                                                    <label>Area</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $area ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Descripcion</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Marca</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Modelo</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>Responsable</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $usuario ?>">
                                                                </div>
                                                                <div class="form-group col-lg-6">
                                                                    <label>No. Serie</label>
                                                                    <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <i class="fas fa-briefcase"></i>
                                                                &nbsp;
                                                                Aceptar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!------------------------------------------------------->

                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_inventario_rep'])) {
    $instancia->reportarArticuloListadoControl();
}


if (isset($_POST['id_inventario_trab_home'])) {
    $instancia->trabajoCasaListadoArticuloControl();
}
