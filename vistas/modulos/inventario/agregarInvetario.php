<!-- Agregar Area -->
<div class="modal fade" id="agregar_inventario" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Agregar Articulo</h5>
            </div>
            <form method="POST" id="agr_inventario" enctype="multipart/form-data">
                <div class="modal-body border-0">
                    <div class="row p-2">
                        <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa" id="super_empresa">
                        <input type="hidden" value="<?= $id_log ?>" name="id_log" id="id_log">
                        <div class="form-group col-lg-6">
                            <label>Descripcion <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="descripcion" name="descripcion" maxlength="50" minlength="1">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Marca</label>
                            <input type="text" class="form-control" name="marca" id="marca" maxlength="50" minlength="1">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Modelo</label>
                            <input type="text" class="form-control" name="modelo" id="modelo" maxlength="50" minlength="1">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Precio</label>
                            <input type="text" class="form-control numeros" name="precio" id="precio" maxlength="50" minlength="1">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Fecha de compra</label>
                            <input type="date" class="form-control" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Usuario <span class="text-danger">*</span></label>
                            <select name="usuario" class="form-control" id="usuario">
                                <option value="" selected>Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_usuario as $usuarios) {
                                    $id_user = $usuarios['id_user'];
                                    $nombre_completo = $usuarios['nombre'] . ' ' . $usuarios['apellido'];
                                    $estado = $usuarios['estado'];

                                    $visible_user = ($estado == 'activo') ? '' : 'd-none';
                                ?>
                                    <option value="<?= $id_user ?>" class="<?= $visible_user ?>"><?= $nombre_completo ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Area <span class="text-danger">*</span></label>
                            <select name="area" class="form-control" id="area">
                                <option value="" selected>Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_areas as $areas) {
                                    $id_area = $areas['id'];
                                    $nombre_area = $areas['nombre'];
                                    $estado = $areas['activo'];

                                    $visible_area = ($estado == 1) ? '' : 'd-none';

                                ?>
                                    <option value="<?= $id_area ?>" class="<?= $visible_area ?>"><?= $nombre_area ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Cantidad <span class="text-danger">*</span></label>
                            <input type="text" class="form-control numeros" name="cantidad" id="cantidad" name="nombre" maxlength="2" minlength="1">
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Categoria <span class="text-danger">*</span></label>
                            <select name="categoria" class="form-control" id="categoria">
                                <option value="" selected>Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_categoria as $categorias) {
                                    $id_categoria = $categorias['id'];
                                    $nombre_categoria = $categorias['nombre'];

                                ?>
                                    <option value="<?= $id_categoria ?>"><?= $nombre_categoria ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Evidencia <span class="text-danger">*</span></label>
                            <input id="file" type="file" class="file" name="archivo" accept=".png,.jpg,.jpeg" data-preview-file-type="any">
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <form method="POST">
                        <input type="hidden" value="<?= $id_log ?>" name="id_temp_log">
                        <input type="hidden" value="<?= $id_super_empresa ?>" name="id_temp_super_empresa">
                        <button type="submit" class="btn btn-primary btn-sm mr-auto" id="carta_entrega" disabled>
                            <i class="fas fa-file-alt" id="carta_entrega"></i>
                            &nbsp;
                            Generar Carta de entrega
                        </button>
                    </form>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="enviar_inventario" class="btn btn-success btn-sm">
                        <i class="fa fa-save"></i>
                        &nbsp;
                        Registrar
                    </button>
                </div>
            </form>
            <div class="modal-body border-0">
                <div class="row p-2">
                    <div class="table-responsive mt-2 p-2">
                        <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center border">
                                    <td colspan="6">
                                        <h4 class="text-primary font-weight-bold">Articulos agregados</h4>
                                    </td>
                                </tr>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Marca</th>
                                    <th scope="col">Modelo</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Fecha Compra</th>
                                    <th scope="col">Cantidad</th>
                                </tr>
                            </thead>
                            <tbody class="buscar" id="tabla_inventario">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> </div>
</div>