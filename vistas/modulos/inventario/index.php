<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlInventario::singleton_inventario();
$instancia_usuarios = ControlUsuarios::singleton_usuarios();
$instancia_areas = ControlAreas::singleton_areas();

$datos_perfil = $instancia_perfil->mostrarPerfilesControl($id_super_empresa);
$datos_usuario = $instancia_usuarios->mostrarUsuariosControl($id_super_empresa);
$datos_areas = $instancia_areas->mostrarAreasControl($id_super_empresa);
$datos_categoria = $instancia->mostrarCategoriasControl($id_super_empresa);
$permisos = $instancia_permiso->permisosUsuarioControl(2, 3, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Inventario
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_inventario">Agregar articulo</a>
                            <a class="dropdown-item" href="<?= BASE_URL ?>inventario/panelControl">Panel de control</a>
                            <a class="dropdown-item" href="<?= BASE_URL ?>inventario/reasignar">Re-asignar</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row ml-5">
                            <div class="col-lg-4">
                                <select name="area_buscar" class="form-control filtro_change" data-tooltip="tooltip" title="Area">
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_areas as $areas) {
                                        $id_area = $areas['id'];
                                        $nombre_area = $areas['nombre'];
                                    ?>
                                        <option value="<?= $id_area ?>"><?= $nombre_area ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <select name="usuario_buscar" class="form-control filtro_change" data-tooltip="tooltip" title="Usuario">
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_usuario as $usuarios) {
                                        $id_usuario = $usuarios['id_user'];
                                        $nombre_user = $usuarios['nombre'] . ' ' . $usuarios['apellido'];

                                    ?>
                                        <option value="<?= $id_usuario ?>"><?= $nombre_user ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" name="nombre_buscar" placeholder="Buscar..." class="form-control filtro" data-tooltip="tooltip" title="Articulo">
                            </div>
                            <div class="col-lg-sm mt-1 text-center">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-search"></i>
                                    &nbsp;
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                    <?php
                    if (isset($_POST['area_buscar'])) {

                        $area = $_POST['area_buscar'];
                        $usuario = $_POST['usuario_buscar'];
                        $articulo = $_POST['nombre_buscar'];

                        $datos = array(
                            'area' => $area,
                            'usuario' => $usuario,
                            'articulo' => $articulo
                        );

                        $buscar = $instancia->buscarInventarioControl($datos);
                    ?>
                        <div class="col-lg-12 mt-4 ml-4 form-inline">
                            <a href="<?= BASE_URL ?>imprimir/imprimirInventario?area=<?= base64_encode($area) ?>&usuario=<?= base64_encode($usuario) ?>&articulo=<?= base64_encode($articulo) ?>" class="btn btn-secondary btn-sm" target="_blank">
                                <i class="fa fa-print"></i>
                                &nbsp;
                                Imprimir
                            </a>
                            <form action="<?= BASE_URL ?>reportes/excelInventario" method="GET">
                                <input type="hidden" name="area" value="<?= base64_encode($area) ?>">
                                <input type="hidden" name="usuario" value="<?= base64_encode($usuario) ?>">
                                <input type="hidden" name="articulo" value="<?= base64_encode($articulo) ?>">
                                <button class="btn btn-sm btn-success ml-3 mt-2 mb-2" data-toggle="tooltip" data-placement="right" title="Descargar Reporte" data-trigger="hover" type="submit">
                                    <i class="fa fa-file-excel"></i>
                                    &nbsp;
                                    Descargar reporte
                                </button>
                            </form>
                        </div>
                        <div class="table-responsive mt-4">
                            <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">USUARIO</th>
                                        <th scope="col">AREA</th>
                                        <th scope="col">CANT</th>
                                        <th scope="col">DESCRIPCION</th>
                                        <th scope="col">MARCA</th>
                                        <th scope="col">ESTADO</th>
                                        <th scope="col">OBSERVACION</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar text-lowercase">
                                    <?php
                                    foreach ($buscar as $inventario) {
                                        $id_inventario = $inventario['id'];
                                        $nombre = $inventario['descripcion'];
                                        $cantidad = $inventario['cantidad'];
                                        $usuario = $inventario['usuario'];
                                        $marca = $inventario['marca'];
                                        $estado = $inventario['estado_nombre'];
                                        $observacion = $inventario['observacion'];
                                        $id_area = $inventario['id_area'];
                                        $area = $inventario['area'];

                                    ?>
                                        <tr class="text-center">
                                            <td><?= $usuario ?></td>
                                            <td><?= $area ?></td>
                                            <td><?= $cantidad ?></td>
                                            <td><?= $nombre ?></td>
                                            <td><?= $marca ?></td>
                                            <td><?= $estado ?></td>
                                            <td><?= $observacion ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInvetario.php';

if (isset($_POST['id_temp_log'])) {
    $instancia->guardarInventarioControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/inventario/funcionesInventario.js"></script></script>