<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'reportes' . DS . 'ControlReportes.php';

$instancia = ControlReporte::singleton_reporte();
$reporte_dano = $instancia->mostrarReporteDanoControl($id_super_empresa);
$reporte_mant = $instancia->mostrarReporteMantControl($id_super_empresa);

$permisos = $instancia_permiso->permisosUsuarioControl(1, 7, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Reportes
                    </h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item col-lg-6 text-center">
                            <a class="nav-link active" id="dano-tab" data-toggle="tab" href="#dano" role="tab" aria-controls="dano" aria-selected="true">Da&ntilde;os</a>
                        </li>
                        <li class="nav-item col-lg-6 text-center">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#mantenimiento" role="tab" aria-controls="profile" aria-selected="false">Mantenimientos</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active p-2" id="dano" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-8"></div>
                                <div class="col-lg-4 mt-3">
                                    <form>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control filtro" placeholder="Buscar">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text rounded-right" id="basic-addon1">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center font-weight-bold">
                                                <th scope="col">ID</th>
                                                <th scope="col">Area</th>
                                                <th scope="col">Descripcion</th>
                                                <th scope="col">Marca</th>
                                                <th scope="col">Modelo</th>
                                                <th scope="col">Codigo</th>
                                                <th scope="col">Estado</th>
                                                <th scope="col">Fecha Reportado</th>
                                                <th scope="col">Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody class="buscar text-lowercase">
                                            <?php
                                            foreach ($reporte_dano as $dano) {
                                                $id_inventario = $dano['id'];
                                                $descripcion = $dano['descripcion'];
                                                $marca = $dano['marca'];
                                                $modelo = $dano['modelo'];
                                                $fecha_reporte = $dano['fecha_reporte'];
                                                $estado = $dano['estado'];
                                                $observacion = $dano['observacion'];
                                                $codigo = $dano['codigo'];
                                                $id_user = $dano['id_user'];
                                                $id_reporte = $dano['id_reporte'];
                                                $id_area = $dano['id_area'];
                                                $area = $dano['area'];

                                            ?>
                                                <tr class="text-center">
                                                    <td><?= $id_inventario ?></td>
                                                    <td><?= $area ?></td>
                                                    <td><?= $descripcion ?></td>
                                                    <td><?= $marca ?></td>
                                                    <td><?= $modelo ?></td>
                                                    <td><?= $codigo ?></td>
                                                    <td><?= $estado ?></td>
                                                    <td><?= $fecha_reporte ?></td>
                                                    <td><?= $observacion ?></td>
                                                    <td>
                                                        <a href="<?= BASE_URL ?>imprimir/reporte?inventario=<?= base64_encode($id_inventario) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar reporte" data-placement="bottom">
                                                            <i class="fa fa-download"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#sol_dano<?= $id_inventario ?>" data-tooltip="tooltip" title="Solucionar" data-placement="bottom">
                                                            <i class="fas fa-wrench"></i>
                                                        </button>
                                                    </td>
                                                </tr>


                                                <!-- Agregar Area -->
                                                <div class="modal fade" data-backdrop="static" data-keyboard="false" id="sol_dano<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Solucionar reporte</h5>
                                                            </div>
                                                            <form method="POST">
                                                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_sol" id="id_inventario">
                                                                <input type="hidden" value="<?= $id_log ?>" name="id_log_sol" id="id_log">
                                                                <input type="hidden" value="<?= $id_user ?>" name="id_user_sol" id="id_user">
                                                                <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa" id="super_empresa">
                                                                <input type="hidden" value="<?= $id_reporte ?>" name="id_reporte" id="id_reporte">
                                                                <input type="hidden" value="<?= $id_area ?>" name="id_area" id="id_area">
                                                                <input type="hidden" value="1" id="tipo_reporte">
                                                                <div class="modal-body border-0">
                                                                    <div class="row p-2">
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Descripcion</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Marca</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Modelo</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Codigo</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Estado</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $estado ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Fecha reportado</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $fecha_reporte ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Observacion</label>
                                                                                <textarea cols="30" rows="4" id="observacion" class="form-control" maxlength="1000"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Firma responsable</label>
                                                                                <canvas id="sig-canvas" width="350" height="80" class="border">
                                                                                    Get a better browser, bro.
                                                                                </canvas>
                                                                                <img src="" class="d-none" id="laimagen">
                                                                                <button type="button" class="btn btn-warning btn-sm" id="clean">
                                                                                    <i class="fas fa-times-circle"></i>
                                                                                    &nbsp;
                                                                                    Limpiar
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Solucionado por</label>
                                                                                <canvas id="sig-canvas2" width="350" height="80" class="border">
                                                                                    Get a better browser, bro.
                                                                                </canvas>
                                                                                <img src="" class="d-none" id="laimagen2">
                                                                                <button type="button" class="btn btn-warning btn-sm" id="clean2">
                                                                                    <i class="fas fa-times-circle"></i>
                                                                                    &nbsp;
                                                                                    Limpiar
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer border-0">
                                                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                                        <i class="fa fa-times"></i>
                                                                        &nbsp;
                                                                        Cerrar
                                                                    </button>
                                                                    <button type="button" class="btn btn-success btn-sm solucionar">
                                                                        <i class="fa fa-save"></i>
                                                                        &nbsp;
                                                                        Solucionar
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>


                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade p-2" id="mantenimiento" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-8"></div>
                                <div class="col-lg-4 mt-3">
                                    <form>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control filtro" placeholder="Buscar">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text rounded-right" id="basic-addon1">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped text-lowercase" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center font-weight-bold">
                                                <th scope="col">ID</th>
                                                <th scope="col">Area</th>
                                                <th scope="col">Descripcion</th>
                                                <th scope="col">Marca</th>
                                                <th scope="col">Modelo</th>
                                                <th scope="col">Codigo</th>
                                                <th scope="col">Estado</th>
                                                <th scope="col">Fecha Reportado</th>
                                                <th scope="col">Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody class="buscar">
                                            <?php
                                            foreach ($reporte_mant as $mant) {
                                                $id_inventario = $mant['id'];
                                                $descripcion = $mant['descripcion'];
                                                $marca = $mant['marca'];
                                                $modelo = $mant['modelo'];
                                                $fecha_reporte = $mant['fecha_reporte'];
                                                $estado = $mant['estado'];
                                                $observacion = $mant['observacion'];
                                                $codigo = $mant['codigo'];
                                                $id_user = $mant['id_user'];
                                                $id_reporte = $mant['id_reporte'];
                                                $id_area = $mant['id_area'];
                                                $area = $mant['area'];

                                            ?>
                                                <tr class="text-center">
                                                    <td><?= $id_inventario ?></td>
                                                    <td><?= $area ?></td>
                                                    <td><?= $descripcion ?></td>
                                                    <td><?= $marca ?></td>
                                                    <td><?= $modelo ?></td>
                                                    <td><?= $codigo ?></td>
                                                    <td><?= $estado ?></td>
                                                    <td><?= $fecha_reporte ?></td>
                                                    <td><?= $observacion ?></td>
                                                    <td>
                                                        <a href="<?= BASE_URL ?>imprimir/reporte?inventario=<?= base64_encode($id_inventario) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar reporte" data-placement="bottom">
                                                            <i class="fa fa-download"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#sol_mant<?= $id_inventario ?>" data-tooltip="tooltip" title="Solucionar" data-placement="bottom">
                                                            <i class="fas fa-wrench"></i>
                                                        </button>
                                                    </td>
                                                </tr>


                                                <!-- Agregar Area -->
                                                <div class="modal fade" data-backdrop="static" data-keyboard="false" id="sol_mant<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Solucionar reporte</h5>
                                                            </div>
                                                            <form method="POST">
                                                                <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_sol" id="id_inventario">
                                                                <input type="hidden" value="<?= $id_log ?>" name="id_log_sol" id="id_log">
                                                                <input type="hidden" value="<?= $id_user ?>" name="id_user_sol" id="id_user">
                                                                <input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa" id="super_empresa">
                                                                <input type="hidden" value="<?= $id_reporte ?>" name="id_reporte" id="id_reporte">
                                                                <input type="hidden" value="<?= $id_area ?>" name="id_area" id="id_area">
                                                                <input type="hidden" value="2" id="tipo_reporte">
                                                                <div class="modal-body border-0">
                                                                    <div class="row p-2">
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Descripcion</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $descripcion ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Marca</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $marca ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Modelo</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $modelo ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Codigo</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $codigo ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Estado</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $estado ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Fecha reportado</label>
                                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?= $fecha_reporte ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Fecha respuesta</label>
                                                                                <input type="datetime" class="form-control" name="fecha_res" value="<?= date('Y-m-d H:i:s') ?>" id="fecha_res">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Observacion</label>
                                                                                <textarea cols="30" rows="4" id="observacion" class="form-control" maxlength="1000"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Firma responsable</label>
                                                                                <canvas id="sig-canvas" width="350" height="80" class="border">
                                                                                    Get a better browser, bro.
                                                                                </canvas>
                                                                                <img src="" class="d-none" id="laimagen">
                                                                                <button type="button" class="btn btn-warning btn-sm" id="clean">
                                                                                    <i class="fas fa-times-circle"></i>
                                                                                    &nbsp;
                                                                                    Limpiar
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label>Solucionado por</label>
                                                                                <canvas id="sig-canvas2" width="350" height="80" class="border">
                                                                                    Get a better browser, bro.
                                                                                </canvas>
                                                                                <img src="" class="d-none" id="laimagen2">
                                                                                <button type="button" class="btn btn-warning btn-sm" id="clean2">
                                                                                    <i class="fas fa-times-circle"></i>
                                                                                    &nbsp;
                                                                                    Limpiar
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer border-0">
                                                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                                        <i class="fa fa-times"></i>
                                                                        &nbsp;
                                                                        Cerrar
                                                                    </button>
                                                                    <button type="button" class="btn btn-success btn-sm solucionar">
                                                                        <i class="fa fa-save"></i>
                                                                        &nbsp;
                                                                        Solucionar
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?= PUBLIC_PATH ?>js/firmas/firma1.js"></script>