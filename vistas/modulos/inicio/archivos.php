<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

$instancia = ControlPadres::singleton_padres();

$formato_lleno = $instancia->consultarFormatoLlenoControl($id_log);
$ultimo_id_formato = $instancia->consultarIdFormatoControl($id_log);

if ($formato_lleno['id'] != '') {
    echo '
    <script>
        window.location.replace("../salir");
    </script>
    ';
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Carta de recomendacion
                    </h4>
                </div>
                <div class="card-body">
                    <form class="p-3" method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="<?= $id_log ?>" name="id_acudiente">
                        <input type="hidden" value="<?= $ultimo_id_formato['id'] ?>" name="id_formato">
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label>Foto reciente del estudiante <span class="text-danger">*</span></label>
                                <input id="file" type="file" class="file" name="fot_est" accept=".png,.jpg,.jpeg" data-preview-file-type="any" required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Foto padre <span class="text-danger">*</span></label>
                                <input id="file" type="file" class="file" name="fot_padre" accept=".png,.jpg,.jpeg" data-preview-file-type="any" required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Foto madre <span class="text-danger">*</span></label>
                                <input id="file" type="file" class="file" name="fot_madre" accept=".png,.jpg,.jpeg" data-preview-file-type="any" required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Certificado bancario <span class="text-danger">*</span></label>
                                <input id="file" type="file" class="file" name="cert_banc" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" required>
                            </div>
                        </div>
                        <div class="row p-1 mt-4">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6 text-right">
                                <button class="btn btn-success btn-sm" type="submit">
                                    <i class="fa fa-save"></i>
                                    &nbsp;
                                    Finalizar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_acudiente'])) {
    $instancia->guardarArchivosControl();
}
