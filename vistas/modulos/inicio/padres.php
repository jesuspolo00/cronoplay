<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

$instancia = ControlPadres::singleton_padres();

$formato_lleno = $instancia->consultarFormatoLlenoControl($id_log);

if ($formato_lleno['id'] != '') {
    echo '
    <script>
        window.location.replace("../salir");
    </script>
    ';
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Solicitud de ingreso
                    </h4>
                </div>
                <div class="card-body">
                    <form class="p-3" method="POST">
                        <input type="hidden" value="<?= $id_log ?>" name="id_acudiente">

                        <!------------------------------------------------------------------------>
                        <h5 class="font-weight-bold">1. INFORMACI&Oacute;N DEL ESTUDIANTE</h5>
                        <div class="row mt-4">
                            <div class="form-group col-lg-4">
                                <label>Aplica a grado</label>
                                <input type="text" class="form-control" name="grado_ap">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Nombre completo del (la) estudiante</label>
                                <input type="text" class="form-control" name="nom_est">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Lugar y fecha de nacimiento</label>
                                <input type="text" class="form-control" name="lug_nac">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Edad</label>
                                <input type="text" class="form-control" name="edad_est">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Nombre del colegio al que asistio el a&ntilde;o pasado</label>
                                <input type="text" class="form-control" name="col_est">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Grado</label>
                                <input type="text" class="form-control" name="grado_est">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Religi&oacute;n</label>
                                <input type="text" class="form-control" name="rel_est">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>N&uacute;mero de hermanos</label>
                                <input type="text" class="form-control" name="num_hermanos">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Lugar que ocupa entre ellos</label>
                                <input type="text" class="form-control" name="lug_ocup">
                            </div>
                        </div>
                        <!------------------------------------------------------------------------>

                        <!------------------------------------------------------------------------>
                        <h5 class="font-weight-bold mt-4">2. INFORMACI&Oacute;N DE LA FAMILIA</h5>
                        <div class="row mt-4">
                            <div class="form-group col-lg-4">
                                <label>Nombre del padre</label>
                                <input type="text" class="form-control" name="nom_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Direccion de residencia</label>
                                <input type="text" class="form-control" name="dir_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono residencia</label>
                                <input type="text" class="form-control" name="tel_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Ocupaci&oacute;n</label>
                                <input type="text" class="form-control" name="ocup_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono oficina</label>
                                <input type="text" class="form-control" name="tel_of_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Correo electr&oacute;nico</label>
                                <input type="text" class="form-control" name="correo_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Celular</label>
                                <input type="text" class="form-control" name="cel_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Empresa donde trabaja</label>
                                <input type="text" class="form-control" name="emp_padre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Cargo</label>
                                <input type="text" class="form-control" name="cargo_padre">
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="form-group col-lg-4">
                                <label>Nombre del madre</label>
                                <input type="text" class="form-control" name="nom_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Direccion de residencia</label>
                                <input type="text" class="form-control" name="dir_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono</label>
                                <input type="text" class="form-control" name="tel_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Ocupaci&oacute;n</label>
                                <input type="text" class="form-control" name="ocup_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono oficina</label>
                                <input type="text" class="form-control" name="tel_of_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Correo electr&oacute;nico</label>
                                <input type="text" class="form-control" name="correo_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Celular</label>
                                <input type="text" class="form-control" name="cel_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Empresa donde trabaja</label>
                                <input type="text" class="form-control" name="emp_madre">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Cargo</label>
                                <input type="text" class="form-control" name="cargo_madre">
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="form-group col-lg-4">
                                <label>Vive el(la) estudiante con ambos padres ?</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="si" value="si" name="vive_ambos" required>
                                    <label class="custom-control-label" for="si">Si</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="no" value="no" name="vive_ambos" required>
                                    <label class="custom-control-label" for="no">No</label>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>En ausencia de ambos quien responde por el(la) estudiante ?</label>
                                <input type="text" class="form-control" name="resp_est">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nom_resp">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Parentesco</label>
                                <input type="text" class="form-control" name="paren_resp">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Direccion residencia</label>
                                <input type="text" class="form-control" name="dir_resp">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono residencia</label>
                                <input type="text" class="form-control" name="tel_resp">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Celular</label>
                                <input type="text" class="form-control" name="cel_resp">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Familiares en el colegio (especifique)</label>
                                <input type="text" class="form-control" name="fam_col">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Curso</label>
                                <input type="text" class="form-control" name="fam_curso">
                            </div>
                        </div>
                        <!------------------------------------------------------------------------>

                        <h5 class="font-weight-bold mt-4">3. INFORMACI&Oacute;N ADICIONAL</h5>
                        <div class="row mt-4">
                            <div class="form-group col-lg-4">
                                <label>Nombre del m&eacute;dico/pediatra</label>
                                <input type="text" class="form-control" name="nom_medico">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono</label>
                                <input type="text" class="form-control" name="tel_medico">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>¿ Sufre alguna enfermedad o alergia ?</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="enf_est_si" value="si" name="enf_est" required>
                                    <label class="custom-control-label" for="enf_est_si">Si</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="enf_est_no" value="no" name="enf_est" required>
                                    <label class="custom-control-label" for="enf_est_no">No</label>
                                </div>
                                <label class="mt-4">¿Cuál?</label>
                                <input type="text" class="form-control" name="cual_enf">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>¿ Necesita alg&uacute;n cuidado especial ?</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="cuid_esp_si" value="si" name="cuid_esp" required>
                                    <label class="custom-control-label" for="cuid_esp_si">Si</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="cuid_esp_no" value="no" name="cuid_esp" required>
                                    <label class="custom-control-label" for="cuid_esp_no">No</label>
                                </div>
                                <label class="mt-4">¿Cuál?</label>
                                <input type="text" class="form-control" name="cual_esp">
                            </div>
                            <div class="form-group col-lg-12">
                                <label>¿ Recibe alg&uacute;n tipo de ayuda profesional ?</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="ayu_prof_si" value="si" name="ayu_prof" required>
                                    <label class="custom-control-label" for="ayu_prof_si">Si</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="ayu_prof_no" value="no" name="ayu_prof" required>
                                    <label class="custom-control-label" for="ayu_prof_no">No</label>
                                </div>
                                <label class="mt-4">Seleccione a continuación</label>
                                <div class="col-lg-12 form-inline">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="Terapia Ocupacional" value="Terapia Ocupacional" name="profe_prof" required>
                                        <label class="custom-control-label" for="Terapia Ocupacional">Terapia Ocupacional</label>
                                    </div>
                                    <div class="custom-control custom-radio ml-4">
                                        <input type="radio" class="custom-control-input" id="Terapia del Lenguaje" value="Terapia del Lenguaje" name="profe_prof" required>
                                        <label class="custom-control-label" for="Terapia del Lenguaje">Terapia del Lenguaje</label>
                                    </div>
                                    <div class="custom-control custom-radio ml-4">
                                        <input type="radio" class="custom-control-input" id="Terapia Psicológica" value="Terapia Psicológica" name="profe_prof" required>
                                        <label class="custom-control-label" for="Terapia Psicológica">Terapia Psicológica</label>
                                    </div>
                                    <div class="custom-control custom-radio ml-4">
                                        <input type="radio" class="custom-control-input" id="Fonoaudiología" value="Fonoaudiología" name="profe_prof" required>
                                        <label class="custom-control-label" for="Fonoaudiología">Fonoaudiología</label>
                                    </div>
                                    <div class="custom-control custom-radio ml-4">
                                        <input type="radio" class="custom-control-input" id="Otros" value="Otros" name="profe_prof" required>
                                        <label class="custom-control-label" for="Otros">Otros</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 mt-4">
                                <label>Nombre del profesional que lo (la) trata</label>
                                <input type="text" class="form-control" name="nom_prof">
                            </div>
                            <div class="form-group col-lg-4 mt-4">
                                <label>Tel&eacute;fono</label>
                                <input type="text" class="form-control" name="tel_prof">
                            </div>
                            <div class="form-group col-lg-4 mt-4">
                                <label>Al terminar el colegio preescolar su hijo aspira ingresar al colegio calendario</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="cal_col_si" value="A" name="cal_col" required>
                                    <label class="custom-control-label" for="cal_col_si">A</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="cal_col_no" value="B" name="cal_col" required>
                                    <label class="custom-control-label" for="cal_col_no">B</label>
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Nombre del colegio</label>
                                <input type="text" class="form-control" name="nom_cal_col">
                            </div>
                        </div>
                        <!------------------------------------------------------------------------>


                        <h5 class="font-weight-bold mt-4">4. REFERENCIAS FAMILIARES Y/O PERSONALES</h5>
                        <div class="row mt-4">
                            <div class="form-group col-lg-4">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nom_ref">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Parentesco</label>
                                <input type="text" class="form-control" name="paren_ref">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Direcci&oacute;n residencia</label>
                                <input type="text" class="form-control" name="dir_ref">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Tel&eacute;fono residencia</label>
                                <input type="text" class="form-control" name="tel_ref">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>¿Quien le recomienda el jard&iacute;n?</label>
                                <input type="text" class="form-control" name="reco_jar">
                            </div>
                            <div class="form-group col-lg-12">
                                <label>Por qu&eacute; est&aacute;n interesados en pertenecer a nuestra familia Play and Learn?</label>
                                <textarea name="int_per" id="" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                        <!------------------------------------------------------------------------>
                        <h5 class="font-weight-bold mt-4">5. REQUISITOS PARA PROCEDIMIENTO DE ADMISIÓN</h5>
                        <div class="row mt-4">
                            <div class="form-group col-lg-12">
                                <label><span class="text-danger">*</span> Solicitud de ingreso debidamente diligenciada con foto reciente del estudiante, padre y madre.</label>
                                <br>
                                <label><span class="text-danger">*</span> Una referencia bancaria.</label>
                                <br>
                                <label><span class="text-danger">*</span> Diligenciar los dos formatos de carta de recomendacion personal de familaias vinculadas a nuestra institucion.</label>
                                <br>
                                <label><span class="text-danger">*</span> Constancia de estudios del colegio actual.</label>
                                <br>
                                <label><span class="text-danger">*</span> Informe evaluativo ultimo a&nacute;o cursado.</label>
                            </div>
                            <div class="form-group col-lg-12">
                                <label>Observaciones</label>
                                <textarea name="observacion" id="" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="row p-1">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    Siguiente
                                    &nbsp;
                                    <i class="fa fa-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once VISTA_PATH . 'script_and_final.php';
if (isset($_POST['id_acudiente'])) {
    $instancia->guardarFormatoControl();
}
?>