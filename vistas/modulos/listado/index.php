<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';


$instancia = ControlInventario::singleton_inventario();
$instancia_usuario = ControlUsuarios::singleton_usuarios();
$instancia_areas = ControlAreas::singleton_areas();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 12, 1, $id_log);

$datos_articulo = $instancia->mostrarDatosArticulosControl($id_super_empresa);
$datos_usuario = $instancia_usuario->mostrarUsuariosControl($id_super_empresa);
$datos_areas = $instancia_areas->mostrarAreasControl($id_super_empresa);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Listado
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">ID</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Area</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Marca</th>
                                    <th scope="col">Modelo</th>
                                    <th scope="col">Codigo</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_articulo as $inventario) {
                                    $id_inventario = $inventario['id'];
                                    $descripcion = $inventario['descripcion'];
                                    $marca = $inventario['marca'];
                                    $modelo = $inventario['modelo'];
                                    $codigo = $inventario['codigo'];
                                    $usuario = $inventario['usuario'];
                                    $area = $inventario['area'];
                                    $estado = $inventario['estado'];
                                    $observacion = $inventario['observacion'];
                                    $id_user = $inventario['id_user'];
                                    $id_area = $inventario['id_area'];
                                    $precio = $inventario['precio'];

                                    if ($inventario['id_categoria'] == 1) {
                                        $hoja_vida = '<a href="' . BASE_URL . 'hoja_vida/index?inventario=' . base64_encode($id_inventario) . '">' . $descripcion . '</a>';
                                    } else {
                                        $hoja_vida = $descripcion;
                                    }


                                ?>
                                    <tr class="text-center">
                                        <td><?= $id_inventario ?></td>
                                        <td><?= $usuario ?></td>
                                        <td><?= $area ?></td>
                                        <td><?= $hoja_vida ?></td>
                                        <td><?= $marca ?></td>
                                        <td><?= $modelo ?></td>
                                        <td><?= $codigo ?></td>
                                        <td class="<?= $visible_mant ?>">
                                            <a href="<?= BASE_URL ?>listado/historial?inventario=<?= base64_encode($id_inventario) ?>" class="btn btn-warning btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Ver historial">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Editar" data-toggle="modal" data-target="#edit<?= $id_inventario ?>">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </td>
                                    </tr>


                                    <!-- Editar inventario -->
                                    <div class="modal fade" id="edit<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Editar inventario</h5>
                                                </div>
                                                <form method="POST">
                                                    <input type="hidden" value="<?= $id_inventario ?>" name="id_inventario_edit">
                                                    <div class="modal-body border-0">
                                                        <div class="row p-3">
                                                            <div class="col-lg-6 form-group">
                                                                <label>Descripcion</label>
                                                                <input type="text" class="form-control" name="descripcion_edit" value="<?= $descripcion ?>" required>
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label>Marca</label>
                                                                <input type="text" class="form-control" name="marca_edit" value="<?= $marca ?>">
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label>Modelo</label>
                                                                <input type="text" class="form-control" name="modelo_edit" value="<?= $modelo ?>">
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label>Precio</label>
                                                                <input type="text" class="form-control" name="precio_edit" value="<?= $precio ?>">
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label>Usuario</label>
                                                                <select name="user_edit" class="form-control">
                                                                    <?php
                                                                    foreach ($datos_usuario as $usuario) {
                                                                        $id_usuario = $usuario['id_user'];
                                                                        $nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];

                                                                        $select = ($id_usuario == $id_user) ? 'selected' : '';
                                                                    ?>
                                                                        <option value="<?= $id_usuario ?>" <?= $select ?>><?= $nombre_completo ?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label>Area</label>
                                                                <select name="area_edit" class="form-control">
                                                                    <?php
                                                                    foreach ($datos_areas as $area) {
                                                                        $id_area_sel = $area['id'];
                                                                        $nombre = $area['nombre'];

                                                                        $select = ($id_area_sel == $id_area) ? 'selected' : '';

                                                                    ?>
                                                                        <option value="<?= $id_area ?>" <?= $select ?>><?= $nombre ?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer border-0">
                                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                            <i class="fa fa-times"></i>
                                                            &nbsp;
                                                            Cerrar
                                                        </button>
                                                        <button type="submit" class="btn btn-success btn-sm">
                                                            <i class="fas fa-sync-alt"></i>
                                                            &nbsp;
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_inventario_edit'])) {
    $instancia->editarInventarioControl();
}
