<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'hoja_vida' . DS . 'ControlHojaVida.php';

$instancia_hoja_vida = ControlHojaVida::singleton_hoja_vida();
$instancia = ControlInventario::singleton_inventario();
$permisos = $instancia_permiso->permisosUsuarioControl(2, 13, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['inventario'])) {
    $id_inventario = base64_decode($_GET['inventario']);
    $datos_articulo = $instancia->mostrarDatosArticuloIdControl($id_inventario, $id_super_empresa);
    $datos_historial = $instancia->historialArticuloControl($id_inventario);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="#" onclick="window.history.go(-1); return false;" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Detalle articulo (<?= $datos_articulo['descripcion'] ?>)
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Nombre del equipo</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['descripcion'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Marca</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['marca'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Modelo</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['modelo'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Codigo</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['codigo'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Area</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['area'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Usuario responsable</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['usuario'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Fecha asignado</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?= $datos_articulo['fechareg'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group mt-2">
                                    <form method="POST" action="<?= BASE_URL ?>imprimir/codigo">
                                        <input type="hidden" value="<?= $datos_articulo['codigo'] ?>" name="codigo">
                                        <button type="submit" class="btn btn-primary btn-sm mt-4">
                                            <i class="fas fa-barcode"></i>
                                            &nbsp;
                                            Descargar codigo
                                        </button>
                                        <a href="<?= PUBLIC_PATH ?>upload/<?= $datos_articulo['evidencia'] ?>" class="btn btn-info btn-sm mt-4 ml-2" target="_blank">
                                            <i class="fa fa-eye"></i>
                                            &nbsp;
                                            Ver evidencia
                                        </a>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mt-4">
                            <table class="table table-hover table-striped mt-2 table-borderless table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold border">
                                        <td scope="col" colspan="9">Historial de reportes/mantenimientos</td>
                                    </tr>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Area</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Marca</th>
                                        <th scope="col">Reporte</th>
                                        <th scope="col">Observacion</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Respuesta</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar text-lowercase">
                                    <?php
                                    foreach ($datos_historial as $historial) {
                                        $id_historial = $historial['id_historial'];
                                        $usuario = $historial['usuario'];
                                        $area = $historial['area'];
                                        $descripcion = $historial['descripcion'];
                                        $marca = $historial['marca'];
                                        $reporte = $historial['estado_nombre'];
                                        $fecha = $historial['fecha_reporte'];
                                        $observacion = $historial['observacion_reporte'];
                                        $fecha_respuesta = $historial['fecha_respuesta'];
                                        $id_reporte = $historial['id_reporte'];
                                        $estado = $historial['estado_reporte'];

                                        $ver = ($estado == 3) ? '' : 'd-none';
                                        $fecha_ver = ($estado == 3) ? $fecha_respuesta : $fecha;

                                        $fecha_reporte = $instancia_hoja_vida->mostrarFechaReportadoControl($id_reporte);
                                        $datetime1 = new DateTime($fecha_reporte['fechareg']);
                                        $datetime2 = new DateTime($fecha_respuesta);
                                        $interval = $datetime1->diff($datetime2);
                                        $respuesta = $interval->format('%d Dias %h Horas %i Minutos %s Segundos');

                                        $respuesta = ($fecha_respuesta == '') ? '' : $respuesta;

                                    ?>
                                        <tr class="text-center">
                                            <td><?= $usuario ?></td>
                                            <td><?= $area ?></td>
                                            <td><?= $descripcion ?></td>
                                            <td><?= $marca ?></td>
                                            <td><?= $reporte ?></td>
                                            <td><?= $observacion ?></td>
                                            <td><?= $fecha_ver ?></td>
                                            <td><?= $respuesta ?></td>
                                            <?php
                                            if ($_SESSION['rol'] == 2) {
                                            ?>
                                                <td class="<?= $ver ?>">
                                                    <a href="<?= BASE_URL ?>imprimir/solucion?reporte=<?= base64_encode($historial['id_reportado']) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar reporte">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                </td>
                                            <?php
                                            }
                                            ?>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';
}
