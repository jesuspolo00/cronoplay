<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 ) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
?>
<div class="container-fluid">
	<div class="text-center">
		<div class="error mx-auto" data-text="404">404</div>
		<p class="lead text-gray-800 mb-5">Page not found!</p>
		<p class="text-gray-500 mb-0">Pagina no encontrada</p>
		<a href="<?= BASE_URL ?>inicio">&larr; Volver al inicio</a>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>