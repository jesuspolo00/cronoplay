<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlUsuarios::singleton_usuarios();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil = $instancia_perfil->mostrarPerfilesControl($id_super_empresa);
$datos_usuario = $instancia->mostrarUsuariosControl($id_super_empresa);
$permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Usuarios
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_usuario">Agregar Usuario</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">#</th>
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Perfil</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_usuario as $usuario) {
                                    $id_user = $usuario['id_user'];
                                    $nombre = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $documento = $usuario['documento'];
                                    $correo = $usuario['correo'];
                                    $telefono = $usuario['telefono'];
                                    $asignatura = $usuario['asignatura'];
                                    $user = $usuario['user'];
                                    $estado = $usuario['estado'];
                                    $id_perfil = $usuario['perfil'];
                                    $perfil = $usuario['nom_perfil'];
                                    $pass_old = $usuario['pass'];

                                    if ($estado != 'activo') {
                                        $class = 'btn-success btn-sm activar_user';
                                        $icon = '<i class="fa fa-check"></i>';
                                        $tooltip = 'Activar usuario';
                                    } else {
                                        $class = 'btn-danger btn-sm inactivar_user';
                                        $icon = '<i class="fa fa-times"></i>';
                                        $tooltip = 'Inactivar usuario';
                                    }

                                ?>
                                    <tr class="text-center">
                                        <td><?= $id_user ?></td>
                                        <td><?= $documento ?></td>
                                        <td><?= $nombre ?></td>
                                        <td><?= $correo ?></td>
                                        <td><?= $telefono ?></td>
                                        <td><?= $user ?></td>
                                        <td><?= $perfil ?></td>
                                        <td>
                                            <button type="button" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Editar usuario" data-toggle="modal" data-target="#editar_usuario<?= $id_user ?>">
                                                <i class="fa fa-user-edit"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn <?= $class ?>" id="<?= $id_user ?>" data-tooltip="tooltip" data-placement="bottom" title="<?= $tooltip ?>">
                                                <?= $icon ?>
                                            </button>
                                        </td>
                                    </tr>



                                    <!--Agregar usuario-->
                                    <div class="modal fade" id="editar_usuario<?= $id_user ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
                                        <div class="modal-dialog modal-lg p-2" role="document">
                                            <div class="modal-content">
                                                <form method="POST" id="form_enviar_editar<?= $id_user ?>">
                                                    <input type="hidden" value="<?= $id_user ?>" name="id_user">
                                                    <input type="hidden" value="<?= $pass_old ?>" name="pass_old">
                                                    <div class="modal-header p-3">
                                                        <h4 class="modal-title text-primary font-weight-bold">Editar Usuario</h4>
                                                    </div>
                                                    <div class="modal-body border-0">
                                                        <div class="row  p-3">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Documento</label>
                                                                    <input type="text" class="form-control numeros" maxlength="50" minlength="1" name="documento_edit" value="<?= $documento ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Nombre</label>
                                                                    <input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre_edit" value="<?= $usuario['nombre'] ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Apellido</label>
                                                                    <input type="text" class="form-control letras" maxlength="50" minlength="1" name="apellido_edit" value="<?= $usuario['apellido'] ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Telefono</label>
                                                                    <input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono_edit" value="<?= $telefono ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Correo</label>
                                                                    <input type="email" class="form-control" maxlength="50" minlength="1" disabled value="<?= $correo ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Asignatura</label>
                                                                    <input type="text" class="form-control" maxlength="50" minlength="1" name="asignatura_edit" value="<?= $asignatura ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Usuario</label>
                                                                    <input type="text" class="form-control" maxlength="50" minlength="1" value="<?= $user ?>" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Contrase&ntilde;a</label>
                                                                    <input type="password" class="form-control" maxlength="16" minlength="8" name="pass_editar" id="pass_editar<?= $id_user ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Confirmar Contrase&ntilde;a</label>
                                                                    <input type="password" class="form-control" maxlength="16" minlength="8" name="conf_pass_editar" id="conf_pass_editar<?= $id_user ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Perfil</label>
                                                                    <select class="form-control" name="perfil_edit" required>
                                                                        <option selected class="d-none" value="<?= $id_perfil ?>"><?= $perfil ?></option>
                                                                        <?php
                                                                        foreach ($datos_perfil as $perfiles) {
                                                                            $id_perfil = $perfiles['id_perfil'];
                                                                            $nom_perfil = $perfiles['nombre'];

                                                                            if ($id_perfil != 1) {
                                                                        ?>
                                                                                <option value="<?= $id_perfil ?>"><?= $nom_perfil ?></option>
                                                                        <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer border-0">
                                                        <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-success btn-sm enviar_editar" data-id="<?= $id_user ?>">
                                                            <i class="fa fa-edit"></i>
                                                            &nbsp;
                                                            Guardar cambios
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>



                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

if (isset($_POST['nombre'])) {
    $instancia->guardarUsuarioControl();
}

if (isset($_POST['nombre_edit'])) {
    $instancia->editarUsuarioControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/validaciones.js"></script>
<script src="<?= PUBLIC_PATH ?>js/usuario/funcionesUsuario.js"></script></script>