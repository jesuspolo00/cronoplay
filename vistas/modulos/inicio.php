<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'contabilidad' . DS . 'ControlContabilidad.php';

$instancia = ControlContabilidad::singleton_contabilidad();
$datos_nomina = $instancia->mostrarNominasControl($id_log);

$cont = 0;

if ($_SESSION['rol'] == 6) {
	include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'padres.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<!-- <div class="col-lg-6">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						Volantes de pago
					</h4>
					<div class="dropdown no-arrow">
						<a class="btn btn-primary btn-sm" href="<?= BASE_URL ?>contabilidad/historial?usuario=<?= base64_encode($id_log) ?>" data-tooltip="tooltip" title="Ver historial completo">
							<i class="fa fa-eye"></i>
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive mt-4">
						<table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<td scope="col" colspan="4">Nominas registradas</td>
								</tr>
								<tr class="text-center font-weight-bold">
									<th scope="col">Mes</th>
									<th scope="col">Usuario responsable</th>
									<th scope="col">Fecha subido</th>
								</tr>
							</thead>
							<tbody class="buscar text-lowercase">
								<?php
								if (count($datos_nomina) <= 0) {
								?>
									<tr class="text-center">
										<td colspan="4">No hay resultados para mostrar</td>
									</tr>
									<?php
								} else {
									foreach ($datos_nomina as $nomina) {
										$id_nomina = $nomina['id'];
										$nombre = $nomina['nombre'];
										$usuario = $nomina['usuario'];
										$anio_mes = $nomina['anio_mes'];
										$fechareg = $nomina['fechareg'];

										$ver = ($cont == 3) ? 'd-none' : '';
									?>
										<tr class="text-center <?= $ver ?>">
											<td><?= $anio_mes ?></td>
											<td><?= $usuario ?></td>
											<td><?= $fechareg ?></td>
											<td>
												<a href="<?= PUBLIC_PATH ?>upload/<?= $nomina['nombre'] ?>" target="_blank" download class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar archivo" data-placement="bottom">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
								<?php
										$cont++;
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> -->
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						Noticias
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<!-- 						<div class="col-lg-6 text-center">
							<img src="<?= PUBLIC_PATH ?>img/corcho.png" class="img-fluid">
						</div>
						<div class="col-lg-6 text-center">
							<img src="<?= PUBLIC_PATH ?>img/corcho.png" class="img-fluid">
						</div> -->
						<div class="col-lg-3"></div>
						<div id="carouselExampleControls" class="carousel slide col-lg-6" data-ride="carousel">
							<div class="carousel-inner">
								<div class="carousel-item ml-2 active">
									<img class="d-block w-100 img-fluid" src="<?= PUBLIC_PATH ?>img/corcho.png" alt="First slide">
								</div>
								<div class="carousel-item ml-2">
									<img class="d-block w-100 img-fluid" src="<?= PUBLIC_PATH ?>img/corcho.png" alt="Second slide">
								</div>
								<div class="carousel-item ml-2">
									<img class="d-block w-100 img-fluid" src="<?= PUBLIC_PATH ?>img/corcho.png" alt="Third slide">
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
