<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'admisiones' . DS . 'ControlAdmisiones.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';
$instancia = ControlAdmisiones::singleton_admisiones();
$instancia_padres = ControlPadres::singleton_padres();

$datos_acudiente = $instancia->mostrarAcudientesControl($id_super_empresa);

$permisos = $instancia_permiso->permisosUsuarioControl(2, 22, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Admisiones
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_acudiente">Agregar acudiente</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control filtro" placeholder="Buscar...">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <button class="btn btn-primary btn-sm mb-4" data-toggle="modal" data-target="#agregar_acudiente">
                            <i class="fa fa-plus"></i>
                            &nbsp;
                            Agregar acudiente
                        </button>
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Estudiante</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_acudiente as $usuario) {
                                    $id_user = $usuario['id_user'];
                                    $nombre = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $documento = $usuario['documento'];
                                    $correo = $usuario['correo'];
                                    $telefono = $usuario['telefono'];
                                    $user = $usuario['user'];
                                    $estado = $usuario['estado'];
                                    $estudiante = $usuario['estudiante'];

                                    $formato_solicitud = $instancia_padres->consultarSolicitudIngresoControl($id_user);
                                    $id_formato = $formato_solicitud['id'];

                                    $recomendacion = $instancia_padres->archivoRecomendacionControl($id_user, $id_formato);

                                    if ($formato_solicitud['id'] != "") {
                                        $ver_solicitud = '';
                                        $ver = '';
                                        $span = '';
                                        $ver_span = 'd-none';
                                        $confirmar = '';
                                    }

                                    if ($formato_solicitud['confirmado'] == 'no') {
                                        $ver_solicitud = 'd-none';
                                        $ver = 'd-none';
                                        $ver_span = 'd-none';
                                        $span = '';
                                        $confirmar = '';
                                    }

                                    if ($formato_solicitud['confirmado'] == 'si') {
                                        $ver_solicitud = '';
                                        $ver = '';
                                        $ver_span = 'd-none';
                                        $span = '';
                                        $confirmar = 'd-none';
                                    }

                                    if ($formato_solicitud['id'] == "") {
                                        $ver_solicitud = 'd-none';
                                        $ver = 'd-none';
                                        $ver_span = '';
                                        $span = '<span class="badge badge-warning text-dark p-1">Pendiente</span>';
                                        $confirmar = 'd-none';
                                    }

                                    if ($formato_solicitud['archivado'] == "si") {
                                        $ver_solicitud = 'd-none';
                                        $ver = 'd-none';
                                        $ver_span = '';
                                        $span = '<span class="badge badge-danger text-white p-1">No admitido</span>';
                                        $confirmar = 'd-none';
                                    }


                                    if ($formato_solicitud['preseleccionado'] == "si") {
                                        $ver_solicitud = '';
                                        $ver = 'd-none';
                                        $ver_span = '';
                                        $span = '<span class="badge badge-success text-white p-1">Preseleccionado</span>';
                                        $confirmar = 'd-none';
                                    }
                                ?>
                                    <tr class="text-center">
                                        <td><?= $documento ?></td>
                                        <td><?= $nombre ?></td>
                                        <td><?= $correo ?></td>
                                        <td><?= $telefono ?></td>
                                        <td><?= $user ?></td>
                                        <td><?= $estudiante ?></td>
                                        <td class="<?= $ver_solicitud ?>">
                                            <a href="<?= BASE_URL ?>imprimir/solicitud?acudiente=<?= base64_encode($id_user) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar formato de solicitud" data-placement="bottom">
                                                <i class="fas fa-file-pdf"></i>
                                            </a>
                                            &nbsp;
                                            <a href="<?= PUBLIC_PATH ?>upload/<?= $recomendacion['nombre'] ?>" class="btn btn-info btn-sm" download>
                                                <i class="fas fa-file-pdf"></i>
                                            </a>
                                            &nbsp;
                                            <a href="<?= BASE_URL ?>confirmar/fotos_formato?id_formato=<?= base64_encode($id_formato) ?>" class="btn btn-secondary btn-sm" data-tooltip="tooltip" title="Descargar fotos" data-placement="bottom">
                                                <i class="fas fa-images"></i>
                                            </a>
                                        </td>
                                        <td class="<?= $ver_span ?>">
                                            <?= $span ?>
                                        </td>
                                        <td class="<?= $confirmar ?>">
                                            <a href="<?= BASE_URL ?>confirmar/solicitud?acudiente=<?= base64_encode($id_user) ?>" class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Confirmar formato">
                                                <i class="fas fa-clipboard-check"></i>
                                            </a>
                                        </td>
                                        <td class="<?= $ver ?>">
                                            <button class="btn btn-success btn-sm habilitar_form" data-tooltip="tooltip" title="Habilitar formato" data-placement="bottom" id="<?= $id_user ?>">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            &nbsp;
                                            <button class="btn btn-danger btn-sm archivar" data-tooltip="tooltip" title="Archivar" data-placement="bottom" id="<?= $id_user ?>">
                                                <i class="fas fa-archive"></i>
                                            </button>
                                            &nbsp;
                                            <button class="btn btn-warning btn-sm seleccionar" data-tooltip="tooltip" title="Preseleccionar" data-placement="bottom" id="<?= $id_user ?>" data-id="<?= $id_log ?>">
                                                <i class="fas fa-hand-pointer"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'admisiones' . DS . 'agregar.php';

if (isset($_POST['documento'])) {
    $instancia->guardarAcudienteControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/admisiones/funcionesAdmisiones.js"></script>