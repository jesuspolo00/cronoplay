<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlPerfil::singleton_perfil();

$datos = $instancia->mostrarDatosPerfilControl($id_log);
$datos_nivel = $instancia->mostrarNivelesControl($id_super_empresa);
$ver_nivel = ($nivel == 5) ? 'd-none' : '';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-primary">Perfil</h4>
				</div>
				<div class="card-body">
					<form method="POST" id="form_enviar" enctype="multipart/form-data">
						<input type="hidden" value="<?= $datos['id_user'] ?>" name="id_user">
						<input type="hidden" value="<?= $datos['pass'] ?>" name="pass_old">
						<div class="row">
							<div class=" col-lg-12 mb-4 text-center">
								<img src="<?= PUBLIC_PATH . $foto_perfil ?>" class="img-thumbnail rounded" width="180" alt="">
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Numero de Documento</label>
									<input type="text" class="form-control numeros" maxlength="50" minlength="1" value="<?= $datos['documento'] ?>" disabled>
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Nombre</label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" value="<?= $datos['nombre'] ?>" name="nombre">
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Apellido</label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" value="<?= $datos['apellido'] ?>" name="apellido">
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Correo</label>
									<input type="email" class="form-control" maxlength="50" minlength="1" value="<?= $datos['correo'] ?>" name="correo" readonly>
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Telefono</label>
									<input type="text" class="form-control numeros" maxlength="50" minlength="1" value="<?= $datos['telefono'] ?>" name="telefono">
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Usuario</label>
									<input type="text" class="form-control" maxlength="50" minlength="1" value="<?= $datos['user'] ?>" name="usuario" readonly>
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="password">
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label for="">Confirmar Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_password">
								</div>
							</div>
							<div class="col-lg-6 form-group <?= $ver_nivel ?>">
								<label>Nivel</label>
								<select name="nivel" class="form-control" required>
									<option value="0" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_nivel as $nivel) {
										$id_nivel = $nivel['id'];
										$nombre = $nivel['nombre'];
										$estado = $nivel['activo'];

										$ver = ($estado == 1) ? '' : 'd-none';
										$select = ($datos['id_nivel'] == $id_nivel) ? 'selected' : '';
									?>
										<option value="<?= $id_nivel ?>" class="<?= $ver ?>" <?= $select ?>><?= $nombre ?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group col-lg-6 mt-2">
								<label>Foto de perfil</label>
								<input id="file" type="file" class="file" name="archivo" accept=".png,.jpg,.jpeg" data-preview-file-type="any">
							</div>
						</div>
						<div class="form-group mt-4 float-right">
							<button type="submit" class="btn btn-primary btn-sm" id="enviar_perfil">
								<i class="fa fa-save"></i>
								&nbsp;
								Guardar Cambios
							</button>
							<input type="hidden" name="perfil" value="<?= $datos['perfil'] ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
if (isset($_POST['nombre'])) {
	$instancia->editarPerfilControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/validaciones.js"></script>