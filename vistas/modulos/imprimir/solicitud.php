<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

class PDF extends FPDF
{
    const LINE_PRECISION = 5;
    const DEBUG_PDF = 0;
    const BORDER = 0;

    function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

        if (!isset($string)) {
            $string = "";
        }
    }


    function reducirMultiT($tam, $alt, $string, $tamanio, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->MultiCell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $ali, 0);
        //$this->MultiCell(25, 6, "Here's some text for display", 'LRT', 'L', 0);

        if (!isset($string)) {
            $string = "";
        }
    }

    
    function MemImage($data, $x=null, $y=null, $w=0, $h=0, $link='')
    {
        // Display the image contained in $data
        $v = 'img'.md5($data);
        $GLOBALS[$v] = $data;
        $a = getimagesize('var://'.$v);
        if(!$a)
            $this->Error('Invalid image data');
        $type = substr(strstr($a['mime'],'/'),1);
        $this->Image('var://'.$v, $x, $y, $w, $h, $type, $link);
        unset($GLOBALS[$v]);
    }

    function GDImage($im, $x=null, $y=null, $w=0, $h=0, $link='')
    {
        // Display the GD image associated with $im
        ob_start();
        imagepng($im);
        $data = ob_get_clean();
        $this->MemImage($data, $x, $y, $w, $h, $link);
    }


    private function grilla()
    {
        $this->SetDrawColor(255, 255, 255);
        if (PDF::DEBUG_PDF) {
            for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                $this->setXY(0, $i);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line(0, $i, 500, $i);
            }
            for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                $this->setXY($i, 0);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line($i, 0, $i, 2000);
            }
        }
        $this->SetDrawColor(0, 0, 0);
    }

    public function generar($id)
    {

        $instancia_padres = ControlPadres::singleton_padres();
        $formato_solicitud = $instancia_padres->consultarSolicitudIngresoControl($id);

        $id_formato = $formato_solicitud['id'];
        $id_acudiente = $formato_solicitud['id_acudiente'];

        $foto_est = $instancia_padres->fotoEstudianteControl($id_acudiente, $id_formato);
        $nombre_foto_est = $foto_est['nombre'];

        $foto_padre = $instancia_padres->fotoPadreControl($id_acudiente, $id_formato);
        $nombre_foto_padre = $foto_padre['nombre'];

        $foto_madre = $instancia_padres->fotoMadreControl($id_acudiente, $id_formato);
        $nombre_foto_madre = $foto_madre['nombre'];

        $grado_ap = $formato_solicitud['grado_ap'];
        $nom_est = $formato_solicitud['nom_est'];
        $lug_nac = $formato_solicitud['lug_nac'];
        $edad_est = $formato_solicitud['edad_est'];
        $col_est = $formato_solicitud['col_est'];
        $grado_est = $formato_solicitud['grado_est'];
        $rel_est = $formato_solicitud['rel_est'];
        $num_hermanos = $formato_solicitud['num_hermanos'];
        $lug_ocup = $formato_solicitud['lug_ocup'];
        $nom_padre = $formato_solicitud['nom_padre'];
        $dir_padre = $formato_solicitud['dir_padre'];
        $tel_padre = $formato_solicitud['tel_padre'];
        $ocup_padre = $formato_solicitud['ocup_padre'];
        $tel_of_padre = $formato_solicitud['tel_of_padre'];
        $correo_padre = $formato_solicitud['correo_padre'];
        $cel_padre = $formato_solicitud['cel_padre'];
        $emp_padre = $formato_solicitud['emp_padre'];
        $cargo_padre = $formato_solicitud['cargo_padre'];
        $nom_madre = $formato_solicitud['nom_madre'];
        $dir_madre = $formato_solicitud['dir_madre'];
        $tel_madre = $formato_solicitud['tel_madre'];
        $ocup_madre = $formato_solicitud['ocup_madre'];
        $tel_of_madre = $formato_solicitud['tel_of_madre'];
        $correo_madre = $formato_solicitud['correo_madre'];
        $cel_madre = $formato_solicitud['cel_madre'];
        $emp_madre = $formato_solicitud['emp_madre'];
        $cargo_madre = $formato_solicitud['cargo_madre'];
        $vive_ambos = $formato_solicitud['vive_ambos'];
        $resp_est = $formato_solicitud['resp_est'];
        $nom_resp = $formato_solicitud['nom_resp'];
        $paren_resp = $formato_solicitud['paren_resp'];
        $dir_resp = $formato_solicitud['dir_resp'];
        $tel_resp = $formato_solicitud['tel_resp'];
        $cel_resp = $formato_solicitud['cel_resp'];
        $fam_col = $formato_solicitud['fam_col'];
        $fam_curso = $formato_solicitud['fam_curso'];
        $nom_medico = $formato_solicitud['nom_medico'];
        $tel_medico = $formato_solicitud['tel_medico'];
        $enf_est = $formato_solicitud['enf_est'];
        $cuid_esp = $formato_solicitud['cuid_esp'];
        $ayu_prof = $formato_solicitud['ayu_prof'];
        $profe_prof = $formato_solicitud['profe_prof'];
        $cal_col = $formato_solicitud['cal_col'];
        $nom_cal_col = $formato_solicitud['nom_cal_col'];
        $nom_ref = $formato_solicitud['nom_ref'];
        $paren_ref = $formato_solicitud['paren_ref'];
        $dir_ref = $formato_solicitud['dir_ref'];
        $tel_ref = $formato_solicitud['tel_ref'];
        $reco_jar = $formato_solicitud['reco_jar'];
        $int_per = $formato_solicitud['int_per'];
        $observacion = $formato_solicitud['observacion'];
        $fechareg = $formato_solicitud['fechareg'];
        $cual_enf = $formato_solicitud['cual_enf'];
        $cual_esp = $formato_solicitud['cual_esp'];
        $nom_prof = $formato_solicitud['nom_prof'];
        $tel_prof = $formato_solicitud['tel_prof'];

        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->pagina1(
            $grado_ap,
            $nom_est,
            $lug_nac,
            $edad_est,
            $col_est,
            $grado_est,
            $rel_est,
            $num_hermanos,
            $lug_ocup,
            $nom_padre,
            $dir_padre,
            $tel_padre,
            $ocup_padre,
            $tel_of_padre,
            $correo_padre,
            $cel_padre,
            $emp_padre,
            $cargo_padre,
            $nom_madre,
            $dir_madre,
            $tel_madre,
            $ocup_madre,
            $tel_of_madre,
            $correo_madre,
            $cel_madre,
            $emp_madre,
            $cargo_madre,
            $vive_ambos,
            $resp_est,
            $nom_resp,
            $paren_resp,
            $dir_resp,
            $tel_resp,
            $cel_resp,
            $fam_col,
            $fam_curso,
            $nom_medico,
            $tel_medico,
            $enf_est,
            $cuid_esp,
            $ayu_prof,
            $profe_prof,
            $cal_col,
            $nom_cal_col,
            $nom_ref,
            $paren_ref,
            $dir_ref,
            $tel_ref,
            $reco_jar,
            $fechareg,
            $cual_enf,
            $cual_esp,
            $nom_prof,
            $tel_prof,
            $nombre_foto_est
        );
        $this->AddPage();
        $this->pagina2($int_per, $observacion, $nombre_foto_padre, $nombre_foto_madre);
    }

    private function pagina1(
        $grado_ap,
        $nom_est,
        $lug_nac,
        $edad_est,
        $col_est,
        $grado_est,
        $rel_est,
        $num_hermanos,
        $lug_ocup,
        $nom_padre,
        $dir_padre,
        $tel_padre,
        $ocup_padre,
        $tel_of_padre,
        $correo_padre,
        $cel_padre,
        $emp_padre,
        $cargo_padre,
        $nom_madre,
        $dir_madre,
        $tel_madre,
        $ocup_madre,
        $tel_of_madre,
        $correo_madre,
        $cel_madre,
        $emp_madre,
        $cargo_madre,
        $vive_ambos,
        $resp_est,
        $nom_resp,
        $paren_resp,
        $dir_resp,
        $tel_resp,
        $cel_resp,
        $fam_col,
        $fam_curso,
        $nom_medico,
        $tel_medico,
        $enf_est,
        $cuid_esp,
        $ayu_prof,
        $profe_prof,
        $cal_col,
        $nom_cal_col,
        $nom_ref,
        $paren_ref,
        $dir_ref,
        $tel_ref,
        $reco_jar,
        $fechareg,
        $cual_enf,
        $cual_esp,
        $nom_prof,
        $tel_prof,
        $nombre_foto_est
    ) {
        $this->Image(PUBLIC_PATH . 'img/pdfs/solicitud/Solicitud_de_Ingreso_page-0001.jpg', '0', '0', '210', '297', 'JPG');
        $this->grilla();

        $anio_fecha = date('Y', strtotime($fechareg));
        $dia_fecha = date('d', strtotime($fechareg));
        $meses = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        $mes_letra = $meses[(date('m', strtotime($fechareg)) * 1) - 1];



        //$this->Cell(15, 15, $this->Image(PUBLIC_PATH . 'img/fondo.jpg', $this->GetX(), $this->GetY(), 15, 15), 1);

        //IMAGEN ESTUDIANTE
        $this->SetXY(172, 5);
        $this->reducirT(32, 38, $this->Image(PUBLIC_PATH . 'upload/' . $nombre_foto_est, $this->GetX(), $this->GetY(), 32, 38), 1, 0, 'C');


        // FECHA
        //DIA - MES
        $this->SetXY(41, 40);
        $this->reducirT(28, 5, $dia_fecha . ' - ' . $mes_letra, 1, 0, 'C');

        //ANIO
        $this->SetXY(77, 40);
        $this->reducirT(31, 5, $anio_fecha, 1, 0, 'C');

        //APLICA GRADO
        $this->SetXY(175, 50);
        $this->reducirT(31, 5, $grado_ap, 1, 0, 'C');

        //NOMBRE ESTUDIANTE
        $this->SetXY(76, 60);
        $this->reducirT(126, 5, $nom_est, 1, 0, 'C');

        //LUGAR Y FECHA DE NACIMIENTO
        $this->SetXY(62, 65.5);
        $this->reducirT(73, 5, $lug_nac, 1, 0, 'C');

        //EDAD
        $this->SetXY(152, 65.5);
        $this->reducirT(50, 5, $edad_est, 1, 0, 'C');

        //NOMBRE COLEGIO PASADO
        $this->SetXY(97, 71);
        $this->reducirT(40, 5, $col_est, 1, 0, 'C');

        //GRADO PASADO
        $this->SetXY(152, 71);
        $this->reducirT(50, 5, $grado_est, 1, 0, 'C');

        //RELIGION
        $this->SetXY(23, 78);
        $this->reducirT(58, 5, $rel_est, 1, 0, 'C');

        //NUMERO DLE HERMANOS
        $this->SetXY(127, 78);
        $this->reducirT(10, 5, $num_hermanos, 1, 0, 'C');

        //LUGAR QUE OCUPA ENTRE ELLOS
        $this->SetXY(192, 78);
        $this->reducirT(10, 5, $lug_ocup, 1, 0, 'C');

        //NOMBRE DEL PADRE
        $this->SetXY(46, 98);
        $this->reducirT(158, 5, $nom_padre, 1, 0, 'C');

        //DIRECCION DE RESIDENCIA
        $this->SetXY(46, 103);
        $this->reducirT(90, 5, $dir_padre, 1, 0, 'C');

        //TELEFONO DE RESIDENCIA
        $this->SetXY(177, 103);
        $this->reducirT(28, 5, $tel_padre, 1, 0, 'C');

        //OCUPACION
        $this->SetXY(30, 108);
        $this->reducirT(108, 5, $ocup_padre, 1, 0, 'C');

        //TELEFONO OFICINA
        $this->SetXY(170, 108);
        $this->reducirT(35, 5, $tel_of_padre, 1, 0, 'C');

        //CORREO ELECTRONICO
        $this->SetXY(42, 113);
        $this->reducirT(95, 5, $correo_padre, 1, 0, 'C');

        //CELULAR
        $this->SetXY(153, 113);
        $this->reducirT(50, 5, $cel_padre, 1, 0, 'C');

        //EMPEZA DONDE TRABAJA
        $this->SetXY(51, 119);
        $this->reducirT(85, 5, $emp_padre, 1, 0, 'C');

        //CARGO
        $this->SetXY(151, 119);
        $this->reducirT(52, 5, $cargo_padre, 1, 0, 'C');

        //NOMBRE DE LA MADRE
        $this->SetXY(50, 130);
        $this->reducirT(152, 5, $nom_madre, 1, 0, 'C');

        //DIRECCION DE RESIDENCIA
        $this->SetXY(47, 135);
        $this->reducirT(90, 5, $dir_madre, 1, 0, 'C');

        //TELEFONO DE RESIDENCIA
        $this->SetXY(175, 135);
        $this->reducirT(30, 5, $tel_madre, 1, 0, 'C');

        //OCUPACION
        $this->SetXY(30, 140);
        $this->reducirT(105, 5, $ocup_madre, 1, 0, 'C');

        //OFICINA
        $this->SetXY(170, 140);
        $this->reducirT(33, 5, $tel_of_madre, 1, 0, 'C');

        //CORREO ELECTRONICO
        $this->SetXY(45, 146);
        $this->reducirT(90, 5, $correo_madre, 1, 0, 'C');

        //CELULAR
        $this->SetXY(155, 145);
        $this->reducirT(50, 5, $cel_madre, 1, 0, 'C');

        //EMPRESA DONDE TRABAJA
        $this->SetXY(51, 151);
        $this->reducirT(85, 5, $emp_madre, 1, 0, 'C');

        //CARGO
        $this->SetXY(152, 151);
        $this->reducirT(50, 6, $cargo_madre, 1, 0, 'C');

        if ($vive_ambos == 'si') {
            //VIVE CON AMBOS - SI
            $this->SetXY(94, 162);
            $this->reducirT(15, 5, 'X', 1, 0, 'C');
        } else {
            //VIVE CON AMBOS - NO
            $this->SetXY(122, 162);
            $this->reducirT(15, 5, 'X', 1, 0, 'C');
        }

        //AUSENCIA
        $this->SetXY(118, 167);
        $this->reducirT(85, 5, $resp_est, 1, 0, 'C');

        //NOMBRE
        $this->SetXY(25, 173);
        $this->reducirT(110, 5, $nom_resp, 1, 0, 'C');

        //PARENTESCO
        $this->SetXY(162, 173);
        $this->reducirT(40, 5, $paren_resp, 1, 0, 'C');

        //DIRECCION DE RESIDENCIA
        $this->SetXY(48, 178);
        $this->reducirT(85, 5, $dir_resp, 1, 0, 'C');

        //TELEFONO RESIDENCIA
        $this->SetXY(178, 178);
        $this->reducirT(25, 5, $tel_resp, 1, 0, 'C');

        //CELULAR
        $this->SetXY(153, 183);
        $this->reducirT(50, 5, $cel_resp, 1, 0, 'C');

        //FAMILIARES
        $this->SetXY(78, 189);
        $this->reducirT(55, 5, $fam_col, 1, 0, 'C');

        //CURSOS
        $this->SetXY(153, 189);
        $this->reducirT(50, 5, $fam_curso, 1, 0, 'C');

        //NOMBRE MEDICO
        $this->SetXY(60, 208);
        $this->reducirT(80, 5, $nom_medico, 1, 0, 'C');

        //TELEFONO
        $this->SetXY(160, 208);
        $this->reducirT(43, 5, $tel_medico, 1, 0, 'C');

        if ($enf_est == 'si') {
            //ENFERMEDAD O ALERGIA - SI
            $this->SetXY(85.5, 214);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        } else {

            //ENFERMEDAD O ALERGIA - NO
            $this->SetXY(104.5, 214);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        //ENFERMEDAD O ALERGIA - CUAL
        $this->SetXY(131, 213);
        $this->reducirT(75, 4, $cual_enf, 1, 0, 'C');

        if ($cuid_esp == 'si') {
            //CUIDADO ESPECIAL - SI
            $this->SetXY(85.5, 218.5);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        } else {
            //CUIDADO ESPECIAL - NO
            $this->SetXY(104.5, 218.5);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        //CUIDADO ESPECIAL - CUAL
        $this->SetXY(131, 218);
        $this->reducirT(75, 4, $cual_esp, 1, 0, 'C');

        if ($ayu_prof == 'si') {
            //AYUDA PROFESIONAL - SI
            $this->SetXY(85.5, 223);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        } else {
            //AYUDA PROFESIONAL - NO
            $this->SetXY(104.5, 223);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        if ($profe_prof == 'Terapia Ocupacional') {
            //TERAPIA OCUP
            $this->SetXY(43.5, 228);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        if ($profe_prof == 'Terapia del Lenguaje') {
            //TERAPIA LENGUAJE
            $this->SetXY(92.7, 228);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        if ($profe_prof == 'Terapia Psicológica') {
            //TERAPIA PSICOLOGIA
            $this->SetXY(140, 228);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        if ($profe_prof == 'Fonoaudiología') {
            //FONO
            $this->SetXY(180, 228);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        if ($profe_prof == 'Otros') {
            //OTROS
            $this->SetXY(200, 228);
            $this->reducirT(5, 4, 'X', 1, 0, 'C');
        }

        //NOMBRE DEL PROFESIONAL
        $this->SetXY(78, 232);
        $this->reducirT(65, 4, $nom_prof, 1, 0, 'C');

        //TELEFONO
        $this->SetXY(160, 232);
        $this->reducirT(45, 4, $tel_prof, 1, 0, 'C');

        if ($cal_col == 'A') {
            //CALENDARIO - A
            $this->SetXY(157, 241);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        } else {
            //CALENDARIO - B
            $this->SetXY(175, 241);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        //NOMBRE DEL COLEGIO
        $this->SetXY(45, 246);
        $this->reducirT(158, 4, $nom_cal_col, 1, 0, 'C');

        //NOMBRE FAMILIAR
        $this->SetXY(23, 265);
        $this->reducirT(115, 5, $nom_ref, 1, 0, 'C');

        //PARENTESCO
        $this->SetXY(162, 265);
        $this->reducirT(40, 5, $paren_ref, 1, 0, 'C');

        //DIRECCION RESIDENCIA
        $this->SetXY(48, 271);
        $this->reducirT(90, 5, $dir_ref, 1, 0, 'C');

        //TELEFONO
        $this->SetXY(178, 271);
        $this->reducirT(25, 5, $tel_ref, 1, 0, 'C');

        //RECOMIENDA
        $this->SetXY(68, 277);
        $this->reducirT(130, 5, $reco_jar, 1, 0, 'C');
    }

    private function pagina2($int_per, $observacion, $nombre_foto_padre, $nombre_foto_madre)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/solicitud/Solicitud_de_Ingreso_page-0002.jpg', '0', '0', '210', '297', 'JPG');
        $this->grilla();

        //INTERESADOS
        $this->SetXY(8, 15);
        $this->reducirMultiT(195, 7, $int_per, 1, 'L');

        //OBSERVACION
        $this->SetXY(8, 165);
        $this->reducirMultiT(195, 7, $observacion, 1, 'L');

        //IMAGEN MAMA
        $this->SetXY(134, 78.5);
        $this->reducirT(32, 38, $this->Image(PUBLIC_PATH . 'upload/' . $nombre_foto_padre, $this->GetX(), $this->GetY(), 32, 38), 1, 0, 'C');

        //IMAGEN PADRE
        $this->SetXY(170, 78.5);
        $this->reducirT(32, 38, $this->Image(PUBLIC_PATH . 'upload/' . $nombre_foto_madre, $this->GetX(), $this->GetY(), 32, 38), 1, 0, 'C');
    }
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 8);
$pdf->SetTitle("Formato solicitud de ingreso", true);
$pdf->generar(base64_decode($_GET['acudiente']));
$pdf->Output();
