<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'reportes' . DS . 'ControlReportes.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlReporte::singleton_reporte();
$instancia_perfil = ControlPerfil::singleton_perfil();

$super_empresa = $_SESSION['super_empresa'];

if (isset($_GET['reporte'])) {

    $id_reporte = base64_decode($_GET['reporte']);

    $datos_reporte = $instancia->mostrarInformacionSolucionReporteControl($id_reporte);
    $datos_super_empresa = $instancia_perfil->mostrarDatosSuperEmpresaControl($super_empresa, 'encabezado2');

    $horas = date('H:i:s', strtotime($datos_reporte['fecha_respuesta']));
    $fecha = date('Y-m-d', strtotime($datos_reporte['fecha_respuesta']));


    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
            /* $this->setJPEGQuality(90);
            $this->Image(PUBLIC_PATH . 'img/' . $this->logo, 0, 0, 210, 35);
            $this->Ln(30);
            $this->Cell(90);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
            $this->Cell(12, 50, 'ENTREGA DE INVENTARIO', 0, 0, 'C'); */
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }
    }

    // create a PDF object
    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document (meta) information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->setData($datos_super_empresa['imagen']);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Reporte solucion');
    $pdf->SetSubject('Reporte solucion');
    $pdf->SetKeywords('Reporte solucion');
    $pdf->AddPage();

    $pdf->Ln(0);
    $pdf->Cell(5);
    $html = '
        <table style="width:98%;" border="1">
        <tr style="text-align:center; font-size: 0.8em; font-weight: bold;">
        <td colspan="2" style="border:none;" rowspan="1"><img src="' . PUBLIC_PATH . 'img/' . $datos_super_empresa['imagen'] . '" border="0" width="120"></td>
        <td colspan="3" rowspan="1" style="border:none;">
        <br>
        <br>
        REPORTE OPERATIVO
        </td>
        <td colspan="1" rowspan="1">
        <br>
        <br>
        Version 1
        <br>
        30-03-2020
        </td>
        </tr>
        </table>';

    // output the HTML content
    $pdf->writeHTMLCell(185, 0, '', '', $html, '', 1, 0, true, 'C', true);

    $pdf->Ln(5);
    $encabezado = '
		<table cellpadding="2" cellspacing="10" style="width: 100%; font-size: 0.9em;">
			<tr>
				<td style="width: 33%;"><strong>Nombre:</strong> ' . $datos_reporte['usuario'] . '</td>
				<td style="width: 33%;"><strong>Area:</strong> ' . $datos_reporte['area'] . '</td>
				<td style="width: 33%;"><strong>Fecha:</strong> ' . $datos_reporte['fechareg'] . '</td>
			</tr>
		</table>
		';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Cell(10);
    $pdf->writeHTMLCell(200, 0, '', '', $encabezado, '', 1, 0, true, 'L', true);

    $pdf->Ln(5);
    $pdf->Cell(5);

    $tabla = '
	<table cellpadding="2" border="1" style="font-size:8.5px; width:100%; font-size: 0.8em; ">
	<tr style="text-align:center; font-weight:bold; text-transform: uppercase;">
		<th style="width: 10%;">ID</th>
		<th style="width: 43%;">DESCRIPCION</th>
		<th style="width: 20%;">MARCA</th>
		<th style="width: 26%;">ESTADO</th>
	</tr>
        <tr style="text-align:center;">
            <td>' . $datos_reporte['id_inventario'] . '</td>
            <td>' . $datos_reporte['descripcion'] . '</td>
            <td>' . $datos_reporte['marca'] . '</td>
            <td>' . $datos_reporte['nombre_estado'] . '</td>
        </tr>
        <tr style="text-align:center;">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="text-align:center;">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
	</table>
	';
    $pdf->writeHTML($tabla, true, false, true, false, '');


    $pdf->Ln(-2);
    $pdf->Cell(25);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
    $pdf->Cell(15, 5, $datos_reporte['usuario'], 0, 0, 'C');

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
    $pdf->Ln(-2);
    $pdf->Cell(65, 12, '__________________________', 0, 0, 'C');
    $pdf->Cell(65, 12, '__________________________', 0, 0, 'C');
    $pdf->Cell(65, 12, '__________________________', 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 8);
    $pdf->Cell(65, 12, 'Reporte Realizado Por', 0, 0, 'C');
    $pdf->Cell(65, 12, 'V°B° Directora Administrativa', 0, 0, 'C');
    $pdf->Cell(65, 12, 'Reporte Remitido a', 0, 0, 'C');


    $pdf->Ln(15);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->MultiCell(0, 5, 'Observacion: ' . $datos_reporte['observacion'], 0, 'L', 0, 0, '', '', true);

    $ln = 10;

    $pdf->Ln($ln);
    $pdf->Cell(5);
    $pdf->setJPEGQuality(90);
    $pdf->Image(PUBLIC_PATH . 'upload/' . $datos_reporte['firma_solucionado'], 50, $ln + 85, 65, 12);
    $pdf->Cell(100, 5, 'Solicitud recibida por: _____________________________________', 0, 0, 'L');
    $pdf->Cell(1);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'U', 8);
    $pdf->Cell(40, 5, 'Hora:       ' . $horas . '        ', 0, 0, 'L');
    $pdf->Cell(1);
    $pdf->Cell(40, 5, 'Fecha:       ' . $fecha . '        ', 0, 0, 'L');

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
    $pdf->Ln($ln);
    $pdf->Cell(5);
    $pdf->setJPEGQuality(90);
    $pdf->Image(PUBLIC_PATH . 'upload/' . $datos_reporte['firma_solucionado'], 50, $ln + 95, 65, 12);
    $pdf->Cell(141, 5, 'Solucionado por: _________________________________________________________________________', 0, 0, 'L');
    $pdf->Cell(1);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'U', 8);
    $pdf->Cell(40, 5, 'Fecha:       ' . $fecha . '        ', 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
    $pdf->setJPEGQuality(90);
    $pdf->Image(PUBLIC_PATH . 'upload/' . $datos_reporte['firma_responsable'], 50, $ln + 105, 65, 12);
    $pdf->Cell(141, 5, 'Recibido conforme por: _____________________________________________________________________', 0, 0, 'L');
    $pdf->Cell(1);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'U', 8);
    $pdf->Cell(40, 5, 'Fecha:        ' . $fecha . '        ', 0, 0, 'L');



    $pdf->Output('reporte_' . date('Y-m-d-H-i-s') . '.pdf', 'I');
}
