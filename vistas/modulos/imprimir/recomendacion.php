<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

class PDF extends FPDF
{
    const LINE_PRECISION = 5;
    const DEBUG_PDF = 0;
    const BORDER = 0;

    function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

        if (!isset($string)) {
            $string = "";
        }
    }


    function reducirMultiT($tam, $alt, $string, $tamanio, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->MultiCell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $ali, 0);
        //$this->MultiCell(25, 6, "Here's some text for display", 'LRT', 'L', 0);

        if (!isset($string)) {
            $string = "";
        }
    }

    private function grilla()
    {
        $this->SetDrawColor(255, 255, 255);
        if (PDF::DEBUG_PDF) {
            for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                $this->setXY(0, $i);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line(0, $i, 500, $i);
            }
            for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                $this->setXY($i, 0);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line($i, 0, $i, 2000);
            }
        }
        $this->SetDrawColor(0, 0, 0);
    }

    public function generar($id)
    {

        $instancia_padres = ControlPadres::singleton_padres();
        $formato = $instancia_padres->consultarFormatoRecomendacionControl($id);
        $fam_recomend = $formato['fam_recomend'];
        $anio_conoce = $formato['anio_conoce'];
        $nom_est = $formato['nom_est'];
        $grado_asp = $formato['grado_asp'];
        $vincu_union = $formato['vincu_union'];
        $cump_filo = $formato['cump_filo'];
        $vinculo_une = $formato['vinculo_une'];

        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->pagina1($fam_recomend, $anio_conoce, $nom_est, $grado_asp, $vincu_union, $cump_filo, $vinculo_une);
    }

    private function pagina1($fam_recomend, $anio_conoce, $nom_est, $grado_asp, $vincu_union, $cump_filo, $vinculo_une)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/carta/Carta_Recomendación_Familiar_page-0001.jpg', '0', '0', '210', '297', 'JPG');
        $this->grilla();

        /* $anio_fecha = date('Y', strtotime($fechareg));
        $dia_fecha = date('d', strtotime($fechareg));
        $meses = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'jCarta_Recomendación_Familiar_page-0001 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        $mes_letra = $meses[(date('m', strtotime($fechareg)) * 1) - 1]; */

        //FAMILIA
        $this->SetXY(65, 37);
        $this->reducirT(105, 5, $fam_recomend, 1, 0, 'C');

        //ANIOS CONOCER
        $this->SetXY(38, 43);
        $this->reducirT(15, 5, $anio_conoce, 1, 0, 'C');

        //NOMBRE DEL ESTUDIANTE
        $this->SetXY(18, 70);
        $this->reducirT(78, 5, $nom_est, 1, 0, 'L');

        //GRADO AL QUE ASPIRA
        $this->SetXY(112, 70);
        $this->reducirT(78, 5, $grado_asp, 1, 0, 'L');


        //VINCULOS QUE UNEN
        if ($vincu_union == 'Familiar') {
            //FAMILIA
            $this->SetXY(40, 108);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($vincu_union == 'Amistad') {
            //AMISTAD
            $this->SetXY(87, 108);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($vincu_union == 'Laboral') {
            //LABORAL
            $this->SetXY(137, 108);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($cump_filo == 'si') {
            //SI
            $this->SetXY(20, 166);
            $this->reducirT(20, 5, 'X', 1, 0, 'C');
        }

        if ($cump_filo == 'no') {
            //NO
            $this->SetXY(58, 166);
            $this->reducirT(20, 5, 'X', 1, 0, 'C');
        }

        //VINCULO QUE UNE AL COLEGIO
        $this->SetXY(18, 187);
        $this->reducirMultiT(182, 8, $vinculo_une, 0, 'L');
    }
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 8);
$pdf->SetTitle("Formato solicitud de ingreso", true);
$pdf->generar(base64_decode($_GET['id_formato']));
$pdf->Output();
