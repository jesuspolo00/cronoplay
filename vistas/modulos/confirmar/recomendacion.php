<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

$instancia = ControlPadres::singleton_padres();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 22, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['id_formato'])) {

    $id_formato = base64_decode($_GET['id_formato']);
    $formato = $instancia->consultarFormatoRecomendacionControl($id_formato);
    $id_acudiente = $formato['id_acudiente'];

?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>confirmar/solicitud?acudiente=<?= base64_encode($id_acudiente) ?>" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Carta de recomendacion
                        </h4>
                    </div>
                    <div class="card-body">
                        <form class="p-3" method="POST">
                            <input type="hidden" value="<?= $id_formato ?>" name="id_formato">
                            <input type="hidden" value="<?= $formato['id'] ?>" name="id_recomendacion">
                            <input type="hidden" value="<?= $formato['id_acudiente'] ?>" name="id_acudiente">
                            <!------------------------------------------------------------------------>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label>Familia a recomendar</label>
                                    <input type="text" class="form-control" name="fam_recomend" value="<?= $formato['fam_recomend'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Cuanto hace conoce a la familia</label>
                                    <input type="text" class="form-control" name="anio_conoce" value="<?= $formato['anio_conoce'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Nombre del estudiante</label>
                                    <input type="text" class="form-control" name="nom_est" value="<?= $formato['nom_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Grado al que aspira</label>
                                    <input type="text" class="form-control" name="grado_asp" value="<?= $formato['grado_asp'] ?>">
                                </div>
                                <?php
                                $vincu_union_fam = ($formato['vincu_union'] == 'Familiar') ? 'checked' : '';
                                $vincu_union_amis = ($formato['vincu_union'] == 'Amistad') ? 'checked' : '';
                                $vincu_union_lab = ($formato['vincu_union'] == 'Laboral') ? 'checked' : '';
                                ?>
                                <div class="form-group col-lg-6">
                                    <label>Vinculos que nos unen a esta familia</label>
                                    <div class="col-lg-12 form-inline mt-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="Familiar" value="Familiar" <?= $vincu_union_fam ?> name="vincu_union" required>
                                            <label class="custom-control-label" for="Familiar">Familiar</label>
                                        </div>
                                        <div class="custom-control custom-radio ml-4">
                                            <input type="radio" class="custom-control-input" id="Amistad" value="Amistad" <?= $vincu_union_amis ?> name="vincu_union" required>
                                            <label class="custom-control-label" for="Amistad">Amistad</label>
                                        </div>
                                        <div class="custom-control custom-radio ml-4">
                                            <input type="radio" class="custom-control-input" id="Laboral" value="Laboral" <?= $vincu_union_lab ?> name="vincu_union" required>
                                            <label class="custom-control-label" for="Laboral">Laboral</label>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $considero_si = ($formato['cump_filo'] == 'si') ? 'checked' : '';
                                $considero_no = ($formato['cump_filo'] == 'no') ? 'checked' : '';
                                ?>
                                <div class="form-group col-lg-12">
                                    <label>Considero que la familia que recomiendo cumplirá con la filosofía y las obligaciones que contraerán con el colegio</label>
                                    <div class="custom-control custom-radio ml-4">
                                        <input type="radio" class="custom-control-input" id="si" value="si" <?= $considero_si ?> name="considero" required>
                                        <label class="custom-control-label" for="si">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio ml-4">
                                        <input type="radio" class="custom-control-input" id="no" value="no" <?= $considero_no ?> name="considero" required>
                                        <label class="custom-control-label" for="no">No</label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Vínculo que me une al colegio</label>
                                    <textarea name="vinculo" id="" class="form-control" cols="30" rows="5">
                                        <?= $formato['vinculo_une'] ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="row p-1">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6 text-right">
                                    <button class="btn btn-success btn-sm" type="submit">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Confirmar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_GET['id_formato'])) {
        $instancia->confirmarRecomendacionControl();
    }
}
