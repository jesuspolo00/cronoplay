<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'admisiones' . DS . 'ControlAdmisiones.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

$instancia = ControlAdmisiones::singleton_admisiones();
$instancia_padres = ControlPadres::singleton_padres();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 22, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['id_formato'])) {
    $id_formato = base64_decode($_GET['id_formato']);
    $fotos_formato = $instancia_padres->fotosFormatoControl($id_formato);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>admisiones/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Fotos
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row p-2">
                            <div class="list-group col-lg-12">
                                <?php
                                foreach ($fotos_formato as $fotos) {
                                    $id_foto = $fotos['id'];
                                    $nombre = $fotos['nombre'];
                                    $tipo_foto = $fotos['tipo_foto'];

                                    if ($tipo_foto == 'fot_est') {
                                        $tipo = 'Foto estudiante';
                                    }

                                    if ($tipo_foto == 'fot_padre') {
                                        $tipo = 'Foto padre';
                                    }

                                    if ($tipo_foto == 'fot_madre') {
                                        $tipo = 'Foto madre';
                                    }

                                    if ($tipo_foto == 'cert_banc') {
                                        $tipo = 'Certificado bancario';
                                    }
                                ?>
                                    <a href="<?= PUBLIC_PATH ?>upload/<?= $nombre ?>" download class="list-group-item list-group-item-action">
                                        <?= $tipo ?>
                                        <i class="fa fa-download float-right text-primary"></i>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';
}
