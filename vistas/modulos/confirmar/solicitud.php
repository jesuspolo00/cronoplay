<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'admisiones' . DS . 'ControlAdmisiones.php';
require_once CONTROL_PATH . 'padres' . DS . 'ControlPadres.php';

$instancia = ControlAdmisiones::singleton_admisiones();
$instancia_padres = ControlPadres::singleton_padres();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 22, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['acudiente'])) {

    $id_acudiente = base64_decode($_GET['acudiente']);
    $formato_solicitud = $instancia_padres->consultarSolicitudIngresoControl($id_acudiente);

?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>admisiones/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Solicitid de ingreso
                        </h4>
                    </div>
                    <div class="card-body">
                        <form class="p-3" method="POST">
                            <input type="hidden" value="<?= $id_acudiente ?>" name="id_acudiente">
                            <input type="hidden" value="<?= $formato_solicitud['id'] ?>" name="id_formato">

                            <!------------------------------------------------------------------------>
                            <h5 class="font-weight-bold">1. INFORMACI&Oacute;N DEL ESTUDIANTE</h5>
                            <div class="row mt-4">
                                <div class="form-group col-lg-4">
                                    <label>Aplica a grado</label>
                                    <input type="text" class="form-control" name="grado_ap" value="<?= $formato_solicitud['grado_ap'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Nombre completo del (la) estudiante</label>
                                    <input type="text" class="form-control" name="nom_est" value="<?= $formato_solicitud['nom_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Lugar y fecha de nacimiento</label>
                                    <input type="text" class="form-control" name="lug_nac" value="<?= $formato_solicitud['lug_nac'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Edad</label>
                                    <input type="text" class="form-control" name="edad_est" value="<?= $formato_solicitud['edad_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Nombre del colegio al que asistio el a&ntilde;o pasado</label>
                                    <input type="text" class="form-control" name="col_est" value="<?= $formato_solicitud['col_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Grado</label>
                                    <input type="text" class="form-control" name="grado_est" value="<?= $formato_solicitud['grado_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Religi&oacute;n</label>
                                    <input type="text" class="form-control" name="rel_est" value="<?= $formato_solicitud['rel_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>N&uacute;mero de hermanos</label>
                                    <input type="text" class="form-control" name="num_hermanos" value="<?= $formato_solicitud['num_hermanos'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Lugar que ocupa entre ellos</label>
                                    <input type="text" class="form-control" name="lug_ocup" value="<?= $formato_solicitud['lug_ocup'] ?>">
                                </div>
                            </div>
                            <!------------------------------------------------------------------------>

                            <!------------------------------------------------------------------------>
                            <h5 class="font-weight-bold mt-4">2. INFORMACI&Oacute;N DE LA FAMILIA</h5>
                            <div class="row mt-4">
                                <div class="form-group col-lg-4">
                                    <label>Nombre del padre</label>
                                    <input type="text" class="form-control" name="nom_padre" value="<?= $formato_solicitud['nom_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Direccion de residencia</label>
                                    <input type="text" class="form-control" name="dir_padre" value="<?= $formato_solicitud['dir_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono residencia</label>
                                    <input type="text" class="form-control" name="tel_padre" value="<?= $formato_solicitud['tel_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Ocupaci&oacute;n</label>
                                    <input type="text" class="form-control" name="ocup_padre" value="<?= $formato_solicitud['ocup_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono oficina</label>
                                    <input type="text" class="form-control" name="tel_of_padre" value="<?= $formato_solicitud['tel_of_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Correo electr&oacute;nico</label>
                                    <input type="text" class="form-control" name="correo_padre" value="<?= $formato_solicitud['correo_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Celular</label>
                                    <input type="text" class="form-control" name="cel_padre" value="<?= $formato_solicitud['cel_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Empresa donde trabaja</label>
                                    <input type="text" class="form-control" name="emp_padre" value="<?= $formato_solicitud['emp_padre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Cargo</label>
                                    <input type="text" class="form-control" name="cargo_padre" value="<?= $formato_solicitud['cargo_padre'] ?>">
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="form-group col-lg-4">
                                    <label>Nombre del madre</label>
                                    <input type="text" class="form-control" name="nom_madre" value="<?= $formato_solicitud['nom_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Direccion de residencia</label>
                                    <input type="text" class="form-control" name="dir_madre" value="<?= $formato_solicitud['dir_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono</label>
                                    <input type="text" class="form-control" name="tel_madre" value="<?= $formato_solicitud['tel_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Ocupaci&oacute;n</label>
                                    <input type="text" class="form-control" name="ocup_madre" value="<?= $formato_solicitud['ocup_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono oficina</label>
                                    <input type="text" class="form-control" name="tel_of_madre" value="<?= $formato_solicitud['tel_of_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Correo electr&oacute;nico</label>
                                    <input type="text" class="form-control" name="correo_madre" value="<?= $formato_solicitud['correo_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Celular</label>
                                    <input type="text" class="form-control" name="cel_madre" value="<?= $formato_solicitud['cel_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Empresa donde trabaja</label>
                                    <input type="text" class="form-control" name="emp_madre" value="<?= $formato_solicitud['emp_madre'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Cargo</label>
                                    <input type="text" class="form-control" name="cargo_madre" value="<?= $formato_solicitud['cargo_madre'] ?>">
                                </div>
                            </div>

                            <?php
                            $vive_check_si = ($formato_solicitud['vive_ambos'] == 'si') ? 'checked' : '';
                            $vive_check_no = ($formato_solicitud['vive_ambos'] == 'no') ? 'checked' : '';
                            ?>

                            <div class="row mt-4">
                                <div class="form-group col-lg-4">
                                    <label>Vive el(la) estudiante con ambos padres ?</label>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="si" value="si" <?= $vive_check_si ?> name="vive_ambos" required>
                                        <label class="custom-control-label" for="si">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="no" value="no" <?= $vive_check_no ?> name="vive_ambos" required>
                                        <label class="custom-control-label" for="no">No</label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>En ausencia de ambos quien responde por el(la) estudiante ?</label>
                                    <input type="text" class="form-control" name="resp_est" value="<?= $formato_solicitud['resp_est'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="nom_resp" value="<?= $formato_solicitud['nom_resp'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Parentesco</label>
                                    <input type="text" class="form-control" name="paren_resp" value="<?= $formato_solicitud['paren_resp'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Direccion residencia</label>
                                    <input type="text" class="form-control" name="dir_resp" value="<?= $formato_solicitud['dir_resp'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono residencia</label>
                                    <input type="text" class="form-control" name="tel_resp" value="<?= $formato_solicitud['tel_resp'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Celular</label>
                                    <input type="text" class="form-control" name="cel_resp" value="<?= $formato_solicitud['cel_resp'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Familiares en el colegio (especifique)</label>
                                    <input type="text" class="form-control" name="fam_col" value="<?= $formato_solicitud['fam_col'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Curso</label>
                                    <input type="text" class="form-control" name="fam_curso" value="<?= $formato_solicitud['fam_curso'] ?>">
                                </div>
                            </div>
                            <!------------------------------------------------------------------------>

                            <h5 class="font-weight-bold mt-4">3. INFORMACI&Oacute;N ADICIONAL</h5>
                            <div class="row mt-4">
                                <div class="form-group col-lg-4">
                                    <label>Nombre del m&eacute;dico/pediatra</label>
                                    <input type="text" class="form-control" name="nom_medico" value="<?= $formato_solicitud['nom_medico'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono</label>
                                    <input type="text" class="form-control" name="tel_medico" value="<?= $formato_solicitud['nom_medico'] ?>">
                                </div>
                                <?php
                                $enf_est_check_si = ($formato_solicitud['enf_est'] == 'si') ? 'checked' : '';
                                $enf_est_check_no = ($formato_solicitud['enf_est'] == 'no') ? 'checked' : '';
                                ?>
                                <div class="form-group col-lg-6">
                                    <label>¿ Sufre alguna enfermedad o alergia ?</label>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="enf_est_si" value="si" <?= $enf_est_check_si ?> name="enf_est" required>
                                        <label class="custom-control-label" for="enf_est_si">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="enf_est_no" value="no" <?= $enf_est_check_no ?> name="enf_est" required>
                                        <label class="custom-control-label" for="enf_est_no">No</label>
                                    </div>
                                    <label class="mt-4">¿Cuál?</label>
                                    <input type="text" class="form-control" name="cual_enf" value="<?= $formato_solicitud['cual_enf'] ?>">
                                </div>
                                <?php
                                $cuid_esp_check_si = ($formato_solicitud['cuid_esp'] == 'si') ? 'checked' : '';
                                $cuid_esp_check_no = ($formato_solicitud['cuid_esp'] == 'no') ? 'checked' : '';
                                ?>
                                <div class="form-group col-lg-6">
                                    <label>¿ Necesita alg&uacute;n cuidado especial ?</label>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="cuid_esp_si" value="si" <?= $cuid_esp_check_si ?> name="cuid_esp" required>
                                        <label class="custom-control-label" for="cuid_esp_si">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="cuid_esp_no" value="no" <?= $cuid_esp_check_no ?> name="cuid_esp" required>
                                        <label class="custom-control-label" for="cuid_esp_no">No</label>
                                    </div>
                                    <label class="mt-4">¿Cuál?</label>
                                    <input type="text" class="form-control" name="cual_esp" value="<?= $formato_solicitud['cual_esp'] ?>">
                                </div>
                                <?php
                                $ayu_prof_check_si = ($formato_solicitud['ayu_prof'] == 'si') ? 'checked' : '';
                                $ayu_prof_check_no = ($formato_solicitud['ayu_prof'] == 'no') ? 'checked' : '';
                                ?>
                                <div class="form-group col-lg-12">
                                    <label>¿ Recibe alg&uacute;n tipo de ayuda profesional ?</label>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="ayu_prof_si" value="si" <?= $ayu_prof_check_si ?> name="ayu_prof" required>
                                        <label class="custom-control-label" for="ayu_prof_si">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="ayu_prof_no" value="no" <?= $ayu_prof_check_no ?> name="ayu_prof" required>
                                        <label class="custom-control-label" for="ayu_prof_no">No</label>
                                    </div>
                                    <?php
                                    $profe_prof_check_terap = ($formato_solicitud['profe_prof'] == 'Terapia Ocupacional') ? 'checked' : '';
                                    $profe_prof_check_terap_len = ($formato_solicitud['profe_prof'] == 'Terapia del Lenguaje') ? 'checked' : '';
                                    $profe_prof_check_terap_psi = ($formato_solicitud['profe_prof'] == 'Terapia Psicológica') ? 'checked' : '';
                                    $profe_prof_check_fono = ($formato_solicitud['profe_prof'] == 'Fonoaudiología') ? 'checked' : '';
                                    $profe_prof_check_otro = ($formato_solicitud['profe_prof'] == 'Otros') ? 'checked' : '';
                                    ?>
                                    <label class="mt-4">Seleccione a continuacion</label>
                                    <div class="col-lg-12 form-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="Terapia Ocupacional" value="Terapia Ocupacional" <?= $profe_prof_check_terap ?> name="profe_prof" required>
                                            <label class="custom-control-label" for="Terapia Ocupacional">Terapia Ocupacional</label>
                                        </div>
                                        <div class="custom-control custom-radio ml-4">
                                            <input type="radio" class="custom-control-input" id="Terapia del Lenguaje" value="Terapia del Lenguaje" <?= $profe_prof_check_terap_len ?> name="profe_prof" required>
                                            <label class="custom-control-label" for="Terapia del Lenguaje">Terapia del Lenguaje</label>
                                        </div>
                                        <div class="custom-control custom-radio ml-4">
                                            <input type="radio" class="custom-control-input" id="Terapia Psicológica" value="Terapia Psicológica" <?= $profe_prof_check_terap_psi ?> name="profe_prof" required>
                                            <label class="custom-control-label" for="Terapia Psicológica">Terapia Psicológica</label>
                                        </div>
                                        <div class="custom-control custom-radio ml-4">
                                            <input type="radio" class="custom-control-input" id="Fonoaudiología" value="Fonoaudiología" <?= $profe_prof_check_fono ?> name="profe_prof" required>
                                            <label class="custom-control-label" for="Fonoaudiología">Fonoaudiología</label>
                                        </div>
                                        <div class="custom-control custom-radio ml-4">
                                            <input type="radio" class="custom-control-input" id="Otros" value="Otros" <?= $profe_prof_check_otro ?> name="profe_prof" required>
                                            <label class="custom-control-label" for="Otros">Otros</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 mt-4">
                                    <label>Nombre del profesional que lo (la) trata</label>
                                    <input type="text" class="form-control" name="nom_prof" value="<?= $formato_solicitud['nom_prof'] ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-4">
                                    <label>Tel&eacute;fono</label>
                                    <input type="text" class="form-control" name="tel_prof" value="<?= $formato_solicitud['tel_prof'] ?>">
                                </div>
                                <?php
                                $cal_col_check_si = ($formato_solicitud['cal_col'] == 'A') ? 'checked' : '';
                                $cal_col_check_no = ($formato_solicitud['cal_col'] == 'B') ? 'checked' : '';
                                ?>
                                <div class="form-group col-lg-4 mt-4">
                                    <label>Al terminar el colegio preescolar su hijo aspira ingresar al colegio calendario</label>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="cal_col_si" value="A" <?= $cal_col_check_si ?> name="cal_col" required>
                                        <label class="custom-control-label" for="cal_col_si">A</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="cal_col_no" value="B" <?= $cal_col_check_no ?> name="cal_col" required>
                                        <label class="custom-control-label" for="cal_col_no">B</label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Nombre del colegio</label>
                                    <input type="text" class="form-control" name="nom_cal_col" value="<?= $formato_solicitud['nom_cal_col'] ?>">
                                </div>
                            </div>
                            <!------------------------------------------------------------------------>


                            <h5 class="font-weight-bold mt-4">4. REFERENCIAS FAMILIARES Y/O PERSONALES</h5>
                            <div class="row mt-4">
                                <div class="form-group col-lg-4">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="nom_ref" value="<?= $formato_solicitud['nom_cal_col'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Parentesco</label>
                                    <input type="text" class="form-control" name="paren_ref" value="<?= $formato_solicitud['paren_ref'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Direcci&oacute;n residencia</label>
                                    <input type="text" class="form-control" name="dir_ref" value="<?= $formato_solicitud['dir_ref'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>Tel&eacute;fono residencia</label>
                                    <input type="text" class="form-control" name="tel_ref" value="<?= $formato_solicitud['tel_ref'] ?>">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>¿Quien le recomienda el jard&iacute;n?</label>
                                    <input type="text" class="form-control" name="reco_jar" value="<?= $formato_solicitud['reco_jar'] ?>">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Por qu&eacute; est&aacute;n interesados en pertenecer a nuestra familia Play and Learn?</label>
                                    <textarea name="int_per" id="" cols="30" rows="5" class="form-control" value="<?= $formato_solicitud['int_per'] ?>"></textarea>
                                </div>
                            </div>
                            <!------------------------------------------------------------------------>
                            <h5 class="font-weight-bold mt-4">5. REQUISITOS PARA PROCEDIMIENTO DE ADMISIÓN</h5>
                            <div class="row mt-4">
                                <div class="form-group col-lg-12">
                                    <label><span class="text-danger">*</span> Solicitud de ingreso debidamente diligenciada con foto reciente del estudiante, padre y madre.</label>
                                    <br>
                                    <label><span class="text-danger">*</span> Una referencia bancaria.</label>
                                    <br>
                                    <label><span class="text-danger">*</span> Diligenciar los dos formatos de carta de recomendacion personal de familaias vinculadas a nuestra institucion.</label>
                                    <br>
                                    <label><span class="text-danger">*</span> Constancia de estudios del colegio actual.</label>
                                    <br>
                                    <label><span class="text-danger">*</span> Informe evaluativo ultimo a&nacute;o cursado.</label>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Observaciones</label>
                                    <textarea name="observacion" id="" cols="30" rows="5" class="form-control">
                                    <?= $formato_solicitud['observacion'] ?>
                                    </textarea>
                                </div>
                            </div>

                            <div class="row p-1">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6 text-right">
                                    <button class="btn btn-success btn-sm" type="submit">
                                        <i class="fas fa-check-double"></i>
                                        &nbsp;
                                        Confirmar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_POST['id_formato'])) {
        $instancia_padres->confirmarFormatoSolicitudControl();
    }
}
