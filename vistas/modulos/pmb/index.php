<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'pmb' . DS . 'ControlPmb.php';

$instancia = ControlPmb::singleton_pmb();

$datos_libros = $instancia->librosPrestadosControl();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 19, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Libros prestados
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control filtro" placeholder="Buscar...">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Libro</th>
                                    <th scope="col">Categoria</th>
                                    <th scope="col">Asignatura</th>
                                    <th scope="col">Codigo de barras</th>
                                    <th scope="col">Fecha Prestado</th>
                                    <th scope="col">Fecha Devolver</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_libros as $libros) {
                                    $documento = $libros['documento'];
                                    $usuario = $libros['nombre'];
                                    $libro = $libros['titulo'];
                                    $codigo = $libros['codigo_barra'];
                                    $asignatura = $libros['asignatura'];
                                    $categoria = $libros['categoria'];
                                    $fecha_prestado = $libros['prestado_el'];
                                    $fecha_devolver = $libros['devolver_el'];
                                ?>
                                    <tr class="text-center">
                                        <td><?= $documento ?></td>
                                        <td><?= $usuario ?></td>
                                        <td><?= $libro ?></td>
                                        <td><?= $categoria ?></td>
                                        <td><?= $asignatura ?></td>
                                        <td><?= $codigo ?></td>
                                        <td><?= $fecha_prestado ?></td>
                                        <td><?= $fecha_devolver ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';