<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
  $er = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia_perfil = ControlPerfil::singleton_perfil();
$instancia_permiso = ControlPermisos::singleton_permisos();

$id_log = $_SESSION['id'];
$id_super_empresa = $_SESSION['super_empresa'];

$datos_usuario = $instancia_perfil->mostrarDatosPerfilControl($id_log);
$datos_super_empresa = $instancia_perfil->mostrarDatosSuperEmpresaControl($id_super_empresa, 'logo');
$foto_perfil = ($datos_usuario['imagen'] == '') ? 'img/user.svg' : 'upload/' . $datos_usuario['imagen'];
$nivel = $datos_usuario['id_nivel'];
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= BASE_URL ?>inicio">
    <div class="sidebar-brand-icon">
      <!-- <i class="fas fa-user text-primary"></i> -->
      <img src="<?= PUBLIC_PATH ?>img/<?= $datos_super_empresa['imagen'] ?>" alt="" width="60">
    </div>
    <div class="sidebar-brand-text ml-2 text-primary mt-3">
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?= BASE_URL ?>inicio">
      <i class="fas fa-home text-primary"></i>
      <span class="text-muted">Inicio</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider bg-gray">

  <?php
  $permisos = $instancia_permiso->permisosUsuarioControl(1, 16, 1, $id_log);
  if ($permisos || $_SESSION['rol'] == 3) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>inventario/listado" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-shapes text-primary"></i>
        <span class="text-muted">Inventario</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(1, 7, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>reportes/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-tools text-primary"></i>
        <span class="text-muted">Reportes</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>usuarios/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-users text-primary"></i>
        <span class="text-muted">Usuarios</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 3, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>inventario/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-barcode text-primary"></i>
        <span class="text-muted">Inventario</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>areas/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-map-marker-alt text-primary"></i>
        <span class="text-muted">Areas</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 9, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>areas/reasignar" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-sync-alt text-primary"></i>
        <span class="text-muted">Re-asignar area</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 10, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>mantenimientos/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-tools text-primary"></i>
        <span class="text-muted">Mantenimientos</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 11, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>categorias/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-list-ul text-primary"></i>
        <span class="text-muted">Categorias</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 12, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>listado/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-list-ol text-primary"></i>
        <span class="text-muted">Listado</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 17, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>inventario/trabajoCasa" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-briefcase text-primary"></i>
        <span class="text-muted">Trabajo en casa</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 18, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>material/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-cubes text-primary"></i>
        <span class="text-muted">Material didactico</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 19, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>pmb/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-book text-primary"></i>
        <span class="text-muted">Libros prestados</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(1, 20, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>pmb/prestamos" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-book-open text-primary"></i>
        <span class="text-muted">Biblioteca</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 22, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>admisiones/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-notes-medical text-primary"></i>
        <span class="text-muted">Admisiones</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(2, 23, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>recursos/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-id-card-alt text-primary"></i>
        <span class="text-muted">Gestión humana</span>
      </a>
    </li>
  <?php }
  $permisos = $instancia_permiso->permisosUsuarioControl(1, 21, 1, $id_log);
  if ($permisos) {
  ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="https://sites.google.com/royalschool.edu.co/intranet" target="_blank" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-globe text-primary"></i>
        <span class="text-muted">Intranet Colegio Real</span>
      </a>
    </li>
  <?php } ?>
  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block bg-gray">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars text-primary"></i>
      </button>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-success" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>

        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <!-- Counter - Alerts -->
            <span class="badge badge-danger badge-counter">3+</span>
          </a>
          <!-- Dropdown - Alerts -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
              Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-primary">
                  <i class="fas fa-file-alt text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 12, 2019</div>
                <span class="font-weight-bold">A new monthly report is ready to download!</span>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-success">
                  <i class="fas fa-donate text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 7, 2019</div>
                $290.29 has been deposited into your account!
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-warning">
                  <i class="fas fa-exclamation-triangle text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 2, 2019</div>
                Spending Alert: We've noticed unusually high spending for your account.
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
          </div>
        </li>

        <!-- Nav Item - Messages -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <!-- Counter - Messages -->
            <span class="badge badge-danger badge-counter">7</span>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">
              Message Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div class="font-weight-bold">
                <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                <div class="small text-gray-500">Emily Fowler · 58m</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                <div class="status-indicator"></div>
              </div>
              <div>
                <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                <div class="small text-gray-500">Jae Chun · 1d</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                <div class="status-indicator bg-warning"></div>
              </div>
              <div>
                <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                <div class="small text-gray-500">Morgan Alvarez · 2d</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div>
                <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                <div class="small text-gray-500">Chicken the Dog · 2w</div>
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
          </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido'] ?></span>
            <img class="img-profile rounded-circle" src="<?= PUBLIC_PATH . $foto_perfil ?>">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?= BASE_URL ?>perfil/index">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Perfil
            </a>
            <?php
            $permisos = $instancia_permiso->permisosUsuarioControl(2, 1, 1, $id_log);
            if ($permisos) {
            ?>
              <a class="dropdown-item" href="<?= BASE_URL ?>configuracion/index">
                <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
                Configuracion
              </a>
            <?php } ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= BASE_URL ?>salir">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Cerrar sesion
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <?php
      include_once VISTA_PATH . 'script_and_final.php';
      ?>