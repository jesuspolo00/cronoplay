<!DOCTYPE html>
<html lang="en">

<head>
    <title>Gesti&oacute;n administrativa</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= PUBLIC_PATH ?>img/icono.png">
    <!-- <link rel="shortcut icon" href="<?= PUBLIC_PATH ?>img/icono.ico" mce_href="favicon.ico" type="image/x-icon" /> -->
    <meta name="theme-color" content="#ffffff">
    <!-- Custom fonts for this template-->
    <link href="<?= PUBLIC_PATH ?>vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="<?= PUBLIC_PATH ?>css/bootstrapClockPicker.css">
    <!-- Custom styles for this template-->
    <link href="<?= PUBLIC_PATH ?>css/sb-admin-2.css" rel="stylesheet">
    <link href="<?= PUBLIC_PATH ?>css/main.css" rel="stylesheet">
    <link href="<?= PUBLIC_PATH ?>css/fileinput.css" rel="stylesheet">
</head>

<body class="bg-image">
    <div id="ohsnap"></div>
    <div id="wrapper">