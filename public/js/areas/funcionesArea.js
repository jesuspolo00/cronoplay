$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

    $("#id_area").change(function() {
        var id = $(this).val();
        usuarioResponsable(id);
    });


    function usuarioResponsable(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/areas/usuarioResponsable.php',
                method: "POST",
                data: { 'id_area': id },
                cache: false,
                success: function(r) {
                    if (r != 'error') {
                        $("#usuario").val(r);
                    } else {
                        $("#usuario").val('No existe responsable');
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});