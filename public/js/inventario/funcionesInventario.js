$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

    $("#enviar_inventario").on(tipoEvento, function(e) {
        e.preventDefault();
        var formulario = new FormData();

        formulario.append('archivo', $("#file")[0].files[0]);
        formulario.append('descripcion', $('#descripcion').prop('value'));
        formulario.append('marca', $('#marca').prop('value'));
        formulario.append('modelo', $('#modelo').prop('value'));
        formulario.append('precio', $('#precio').prop('value'));
        formulario.append('fecha', $('#fecha').prop('value'));
        formulario.append('usuario', $('#usuario').prop('value'));
        formulario.append('area', $('#area').prop('value'));
        formulario.append('cantidad', $('#cantidad').prop('value'));
        formulario.append('super_empresa', $('#super_empresa').prop('value'));
        formulario.append('id_log', $('#id_log').prop('value'));
        formulario.append('categoria', $('#categoria').prop('value'));

        if ($('#cantidad').val() == '' || $('#area').val() == '' || $('#usuario').val() == '' || $("#descripcion") == '' || $('#categoria').val() == '') {
            $('#cantidad').focus();
            $('#area').focus();
            $('#usuario').focus();
            $('#descripcion').focus();
            ohSnap("Campos obligatorios vacios!", { color: "red", 'duration': '1000' });
        } else {
            if (fileValidation() != true) {
                ohSnap("Campos obligatorios vacios!", { color: "red", 'duration': '1000' });
            } else {
                $.ajax({
                    type: "POST",
                    url: '../vistas/ajax/inventario/inventarioTemp.php',
                    data: formulario,
                    processData: false,
                    cache: false,
                    dataType: "text",
                    contentType: false,
                    success: function(r) {
                        $("#tabla_inventario").append(r);
                        $("#carta_entrega").attr('disabled', false);
                        $("#agr_inventario")[0].reset();
                        $("#carta_entrega").attr('disabled', false);
                        ohSnap("Registrado correctamente!", { color: "green", 'duration': '1000' });
                    }
                });
            }
        }
    });


    $("#file").change(function() {
        var id = $(this).attr('id');
        cambiarFile(id);
    });

    function cambiarFile(id) {
        const input = document.getElementById(id);
        var fileSize = input.files[0]['size'];
        var siezekiloByte = parseInt(fileSize / 1024);
        if (input.files && input.files[0] && siezekiloByte < 3072) {
            $("#archivo_" + id).text(input.files[0]['name']);
            $(".tooltip").hide();
        } else {
            $(".tooltip").hide();
            $(".tooltip").show();
            $("#" + id).focus();
            $("#" + id).attr('data-toggle', 'tooltip');
            $("#" + id).addClass('border border-danger');
            $("#" + id).tooltip({ title: "Limite de 3mb excedido", trigger: "focus", placement: "bottom" });
            $("#" + id).tooltip('show');
            $("#" + id).val('');
            $("#archivo_" + id).text('');
        }
    }


    function fileValidation() {
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
        if (!allowedExtensions.exec(filePath)) {
            fileInput.value = '';
            return false;
        } else {
            return true;
        }
    }
});