$(document).ready(function() {

    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

    $("#enviar_datos").on(tipoEvento, function(e) {
        e.preventDefault();

        var doc = $("#doc_user").val();

        if (doc == "") {
            e.preventDefault();
            ohSnap("Favor completar este campo!", { color: "red", 'duration': '1000' });
        } else {
            validarDocumento(doc);
        }
    });


    function validarDocumento(doc) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/validarDocumento.php',
                method: 'POST',
                data: { 'documento': doc },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $(".tooltip").show();
                        $("#doc_user").focus();
                        $("#doc_user").attr('data-toggle', 'tooltip');
                        $("#doc_user").addClass('border border-danger');
                        $("#doc_user").tooltip({ title: "Documento ya esta registrado en el sistema", trigger: "focus", placement: "right" });
                        $("#doc_user").tooltip('show');
                    } else {
                        $("#form_enviar").submit();
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

});