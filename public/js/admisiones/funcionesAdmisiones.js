$(document).ready(function() {

    $(".habilitar_form").click(function() {
        var id = $(this).attr('id');
        habilitarFormato(id);
    });


    $(".archivar").click(function() {
        var id = $(this).attr('id');
        archivarFormato(id);
    });

    $(".seleccionar").click(function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-id');
        seleccionar(id, id_log);
    });

    function habilitarFormato(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/admisiones/habilitar.php',
                method: 'POST',
                data: { 'id': id },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Habilitado correctamente!", { color: "green", "duration": "1000" });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error!", { color: "red", "duration": "1000" });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }



    function archivarFormato(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/admisiones/archivar.php',
                method: 'POST',
                data: { 'id': id },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Archivado correctamente!", { color: "green", "duration": "1000" });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error!", { color: "red", "duration": "1000" });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }



    function seleccionar(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/admisiones/seleccionar.php',
                method: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Preseleccionado correctamente!", { color: "green", "duration": "1000" });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error!", { color: "red", "duration": "1000" });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }


    function recargarPagina() {
        window.location.replace("index");
    }

});