<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita551c242730647677085066830947932
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Picqer\\Barcode\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Picqer\\Barcode\\' => 
        array (
            0 => __DIR__ . '/..' . '/picqer/php-barcode-generator/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita551c242730647677085066830947932::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita551c242730647677085066830947932::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
