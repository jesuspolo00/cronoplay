<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'inventario' . DS . 'ModeloInventario.php';
require_once CONTROL_PATH . 'hash.php';

class ControlInventario
{

    private static $instancia;

    public static function singleton_inventario()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function mostrarDatosTemporalesControl($id_log, $super_empresa)
    {
        $mostrar = ModeloInventario::mostrarDatosTemporalesModel($id_log, $super_empresa);
        return $mostrar;
    }


    public function mostrarCategoriasControl($super_empresa)
    {
        $mostrar = ModeloInventario::mostrarCategoriasModel($super_empresa);
        return $mostrar;
    }


    public function informacionReporteControl($id)
    {
        $mostrar = ModeloInventario::informacionReporteModel($id);
        return $mostrar;
    }


    public function mostrarArticulosLiberadosControl($super_empresa)
    {
        $mostrar = ModeloInventario::mostrarArticulosLiberadosModel($super_empresa);
        return $mostrar;
    }

    public function mostrarEquipoComputoControl($super_empresa)
    {
        $mostrar = ModeloInventario::mostrarEquipoComputoModel($super_empresa);
        return $mostrar;
    }

    public function mostrarDatosArticulosControl($super_empresa)
    {
        $mostrar = ModeloInventario::mostrarDatosArticulosModel($super_empresa);
        return $mostrar;
    }

    public function mostrarDatosArticuloIdControl($id)
    {
        $mostrar = ModeloInventario::mostrarDatosArticuloIdModel($id);
        return $mostrar;
    }

    public function historialArticuloControl($id)
    {
        $mostrar = ModeloInventario::historialArticuloModel($id);
        return $mostrar;
    }


    public function buscarReporteLiberadoControl($id)
    {
        $mostrar = ModeloInventario::buscarReporteLiberadoModel($id);
        return $mostrar;
    }


    public function historialReportesControl($super_empresa)
    {
        $mostrar = ModeloInventario::historialReportesModel($super_empresa);
        return $mostrar;
    }


    public function articulosComputoAreaControl($id)
    {
        $mostrar = ModeloInventario::articulosComputoAreaControl($id);
        return $mostrar;
    }


    public function mostrarArticulosUsuarioControl($id)
    {
        $mostrar = ModeloInventario::mostrarArticulosUsuarioModel($id);
        return $mostrar;
    }


    public function mostrarMaterialDidacticoControl($super_empresa)
    {
        $mostrar = ModeloInventario::mostrarMaterialDidacticoModel($super_empresa);
        return $mostrar;
    }

    public function trabajoCasaControl($super_empresa)
    {
        $mostrar = ModeloInventario::trabajoCasaModel($super_empresa);
        return $mostrar;
    }

    public function buscarInventarioControl($datos)
    {

        $area = ($datos['area'] == '') ? '' : ' AND iv.id_area = ' . $datos['area'];
        $usuario = ($datos['usuario'] == '') ? '' : ' AND iv.id_user = ' . $datos['usuario'];
        $articulo = ($datos['articulo'] == '') ? '' : ' AND iv.descripcion like "%' . $datos['articulo'] . '%"';

        $datos_buscar = array(
            'area' => $area,
            'usuario' => $usuario,
            'articulo' => $articulo
        );

        $mostrar = ModeloInventario::buscarInventarioModel($datos_buscar);
        return $mostrar;
    }


    public function buscarInventarioDetalleControl($datos)
    {

        $area = ($datos['area'] == '') ? '' : ' AND iv.id_area = ' . $datos['area'];
        $usuario = ($datos['usuario'] == '') ? '' : ' AND iv.id_user = ' . $datos['usuario'];
        $articulo = ($datos['articulo'] == '') ? '' : ' AND iv.descripcion like "%' . $datos['articulo'] . '%"';

        $datos_buscar = array(
            'area' => $area,
            'usuario' => $usuario,
            'articulo' => $articulo
        );

        $mostrar = ModeloInventario::buscarInventarioDetalleModel($datos_buscar);
        return $mostrar;
    }


    public function eliminarTemporalControl($datos)
    {
        $eliminar = ModeloInventario::eliminarTemporalModel($datos);
        return $eliminar;
    }


    public function guardarInventarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_temp_log']) &&
            !empty($_POST['id_temp_log']) &&
            isset($_POST['id_temp_super_empresa']) &&
            !empty($_POST['id_temp_super_empresa'])
        ) {

            echo '
            <script>
            window.open("' . BASE_URL . 'imprimir/cartaEntrega?id_log=' . base64_encode($_POST['id_temp_log']) .
                '&super_empresa=' . base64_encode($_POST['id_temp_super_empresa']) . '");
            </script>';

            $temporal = array(
                'id_log' => $_POST['id_temp_log'],
                'id_super_empresa' => $_POST['id_temp_super_empresa']
            );

            $guardar_articulo = ModeloInventario::guardarInventarioModel($temporal);

            if ($guardar_articulo == TRUE) {
                $guardar_evidencias = ModeloInventario::guardarEvidenciaModel($temporal);
                if ($guardar_evidencias == TRUE) {
                    $actualizar_temporal = ModeloInventario::actualizarTemporalModel($temporal);
                    if ($actualizar_temporal == TRUE) {
                        echo '
                        <script>
                        ohSnap("Registrados Correctamente!", {color: "green", "duration": "1000"});
                        setTimeout(recargarPagina,1050);

                        function recargarPagina(){
                            window.location.replace("' . BASE_URL . 'inventario/index");
                        }
                        </script>';
                    }
                }
            }
        }
    }



    public function guardarInventarioTempControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion']) &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario']) &&
            isset($_POST['area']) &&
            !empty($_POST['area']) &&
            isset($_POST['cantidad']) &&
            !empty($_POST['cantidad'])
        ) {

            $cantidad = $_POST['cantidad'];
            mt_srand(6);

            $fecha_compra = ($_POST['fecha'] == '') ? '0000-00-00' : $_POST['fecha'];

            for ($i = 0; $i < $cantidad; $i++) {

                $codigo = $this->numeroAleatorio();

                $datos = array(
                    'descripcion' => $_POST['descripcion'],
                    'marca' => $_POST['marca'],
                    'modelo' => $_POST['modelo'],
                    'precio' => $_POST['precio'],
                    'estado' => 1,
                    'fecha_compra' => $fecha_compra,
                    'id_user' => $_POST['usuario'],
                    'id_area' => $_POST['area'],
                    'id_log' => $_POST['id_log'],
                    'super_empresa' => $_POST['super_empresa'],
                    'codigo' => $codigo,
                    'id_categoria' => $_POST['categoria']
                );

                $guardar_temp = ModeloInventario::guardarInventarioTempModel($datos);

                if ($guardar_temp['guardar'] == TRUE) {

                    $id_temp = $guardar_temp['id'];

                    $datos_evidencia_temp = array(
                        'id_temp' => $id_temp,
                        'archivo' => $_FILES['archivo']['name'],
                        'id_log' => $_POST['id_log'],
                        'id_super_empresa' => $_POST['super_empresa']
                    );

                    $guardo_evidencia_temp = $this->guardarDocumentoControl($datos_evidencia_temp);
                }
            }

            if ($guardo_evidencia_temp == TRUE) {
                $datos_return = array(
                    'descripcion' => $_POST['descripcion'],
                    'marca' => $_POST['marca'],
                    'modelo' => $_POST['modelo'],
                    'precio' => $_POST['precio'],
                    'fecha_compra' => $_POST['fecha'],
                    'cantidad' => $_POST['cantidad']
                );
            } else {
                $datos_return = array();
            }

            return $datos_return;
        }
    }


    public function numeroAleatorio()
    {
        $numero = rand();

        $codigo = ModeloInventario::verificarCodigoModel($numero);

        if ($codigo['codigo'] != '') {
            $this->numeroAleatorio();
        } else {
            $num_codigo = $numero;
        }

        return $num_codigo;
    }


    public function guardarDocumentoControl($datos)
    {
        //obtener el nombre del archivo
        $nom_arch = $datos['archivo'];
        //extraer la extencion del archivo de el archivo
        $ext_arch = explode(".", $nom_arch);
        $ext_arch = end($ext_arch);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['id_temp'] . '_' . $fecha_arch)) . '.' . $ext_arch;

        $datos_temp = array(
            'nombre' => $nombre_archivo,
            'id_inventario_temp' => $datos['id_temp'],
            'id_log' => $datos['id_log'],
            'id_super_empresa' => $datos['id_super_empresa']
        );

        $guardar_evidencia = ModeloInventario::guardarEvidenciaTempModel($datos_temp);

        if ($guardar_evidencia == TRUE) {
            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
                move_uploaded_file($_FILES['archivo']['tmp_name'], $ruta_img);
            }

            return TRUE;
        }
    }



    public function liberarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log_lib']) &&
            !empty($_POST['id_log_lib']) &&
            isset($_POST['super_empresa_lib']) &&
            !empty($_POST['super_empresa_lib']) &&
            isset($_POST['id_inventario_lib']) &&
            !empty($_POST['id_inventario_lib'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_lib'],
                'id_log' => $_POST['id_log_lib'],
                'id_super_empresa' => $_POST['super_empresa_lib'],
                'id_user' => $_POST['id_user_lib'],
                'id_area' => $_POST['id_area_lib']
            );

            $liberar = ModeloInventario::liberarArticuloModel($datos);

            if ($liberar == TRUE) {
                echo '
                <script>
                ohSnap("Liberado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/panelControl");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function descontinuarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log_desc']) &&
            !empty($_POST['id_log_desc']) &&
            isset($_POST['super_empresa_desc']) &&
            !empty($_POST['super_empresa_desc']) &&
            isset($_POST['id_inventario_desc']) &&
            !empty($_POST['id_inventario_desc'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_desc'],
                'id_log' => $_POST['id_log_desc'],
                'id_super_empresa' => $_POST['super_empresa_desc'],
                'id_user' => $_POST['id_user_desc'],
                'id_area' => $_POST['id_area_desc'],
                'observacion' => $_POST['observacion']
            );

            $descontinuar = ModeloInventario::descontinuarArticuloModel($datos);

            if ($descontinuar == TRUE) {
                echo '
                <script>
                ohSnap("Descontinuado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/panelControl");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function mantenimientoArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_mant']) &&
            !empty($_POST['id_inventario_mant']) &&
            isset($_POST['fecha']) &&
            !empty($_POST['fecha'])
        ) {

            $fecha = ($_POST['fecha'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha'];

            $datos = array(
                'id_inventario' => $_POST['id_inventario_mant'],
                'observacion' => $_POST['observacion'],
                'id_log' => $_POST['id_log_mant'],
                'id_user' => $_POST['id_user_mant'],
                'fechareg' => $fecha,
                'id_super_empresa' => $_POST['id_super_empresa'],
                'estado' => 6,
                'tipo_reporte' => 2,
                'id_area' => $_POST['id_area_mant']
            );

            $mantenimiento = ModeloInventario::mantenimientoArticuloModel($datos);

            if ($mantenimiento == TRUE) {
                echo '
                <script>
                ohSnap("Reportado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'mantenimientos/index");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function reportarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_rep']) &&
            !empty($_POST['id_inventario_rep'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_rep'],
                'observacion' => $_POST['observacion'],
                'id_log' => $_POST['id_log_rep'],
                'id_user' => $_POST['id_user_rep'],
                'id_area' => $_POST['id_area_rep'],
                'estado' => 2,
                'tipo_reporte' => 1,
                'super_empresa' => $_POST['super_empresa_rep']
            );

            $reporte = ModeloInventario::reportarArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Reportado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/panelControl");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function reportarArticuloListadoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_rep']) &&
            !empty($_POST['id_inventario_rep'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_rep'],
                'observacion' => $_POST['observacion'],
                'id_log' => $_POST['id_log_rep'],
                'id_user' => $_POST['id_user_rep'],
                'id_area' => $_POST['id_area_rep'],
                'estado' => 2,
                'tipo_reporte' => 1,
                'super_empresa' => $_POST['super_empresa_rep']
            );

            $reporte = ModeloInventario::reportarArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Reportado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/listado");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }


    public function reasignarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_super_empresa']) &&
            !empty($_POST['id_super_empresa']) &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user']) &&
            isset($_POST['id_area']) &&
            !empty($_POST['id_area']) &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {

            $datos = array(
                'id_inventario' => $_POST['id_inventario'],
                'id_log' => $_POST['id_log'],
                'id_super_empresa' => $_POST['id_super_empresa'],
                'id_user' => $_POST['id_user'],
                'id_area' => $_POST['id_area'],
                'id_reporte' => $_POST['id_reporte'],
                'fecha_respuesta' => date('Y-m-d H:i:s')
            );

            $reporte = ModeloInventario::reasignarArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Re-asignado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/reasignar");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function programarMantenimientoArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario']) &&
            isset($_POST['frec_mant']) &&
            !empty($_POST['frec_mant'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario'],
                'frec_mant' => $_POST['frec_mant']
            );

            $actualizar = ModeloInventario::programarMantenimientoArticuloModel($datos);

            if ($actualizar == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'mantenimientos/index");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }


    public function programarMantenimientoAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log_area']) &&
            !empty($_POST['id_log_area']) &&
            isset($_POST['frec_mant']) &&
            !empty($_POST['frec_mant'])
        ) {


            $array_inventario = array();
            $array_inventario = $_POST['id_inventario_area'];

            foreach ($array_inventario as $a) {

                $id_inventario = $a;

                $datos = array(
                    'id_inventario' => $id_inventario,
                    'frec_mant' => $_POST['frec_mant']
                );

                $actualizar = ModeloInventario::programarMantenimientoArticuloModel($datos);
            }

            if ($actualizar == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'mantenimientos/areas");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }


    public function trabajoCasaListadoArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_trab_home']) &&
            !empty($_POST['id_inventario_trab_home'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_trab_home'],
                'observacion' => 'Trabajo en casa',
                'id_log' => $_POST['id_log_trab_home'],
                'id_user' => $_POST['id_user_trab_home'],
                'id_area' => $_POST['id_area_trab_home'],
                'estado' => 8,
                'tipo_reporte' => 3,
                'super_empresa' => $_POST['super_empresa_trab_home']
            );

            $reporte = ModeloInventario::trabajoCasaArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/listado");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }


    public function trabajoCasaArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_trab_home']) &&
            !empty($_POST['id_inventario_trab_home'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_trab_home'],
                'observacion' => 'Trabajo en casa',
                'id_log' => $_POST['id_log_trab_home'],
                'id_user' => $_POST['id_user_trab_home'],
                'id_area' => $_POST['id_area_trab_home'],
                'estado' => 8,
                'tipo_reporte' => 3,
                'super_empresa' => $_POST['super_empresa_trab_home']
            );

            $reporte = ModeloInventario::trabajoCasaArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/panelControl");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function trabajoCasaMaterialControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_trab_home']) &&
            !empty($_POST['id_inventario_trab_home'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_trab_home'],
                'observacion' => 'Trabajo en casa',
                'id_log' => $_POST['id_log_trab_home'],
                'id_user' => $_POST['id_user_trab_home'],
                'id_area' => $_POST['id_area_trab_home'],
                'estado' => 8,
                'tipo_reporte' => 3,
                'super_empresa' => $_POST['super_empresa_trab_home']
            );

            $reporte = ModeloInventario::trabajoCasaArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'material/index");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function removerTrabajoCasaArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_rem_home']) &&
            !empty($_POST['id_inventario_rem_home'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_rem_home'],
                'observacion' => 'Removido de trabajo en casa',
                'id_log' => $_POST['id_log_rem_home'],
                'id_user' => $_POST['id_user_rem_home'],
                'id_area' => $_POST['id_area_rem_home'],
                'estado' => 9,
                'tipo_reporte' => 3,
                'super_empresa' => $_POST['super_empresa_rem_home']
            );

            $reporte = ModeloInventario::removerTrabajoCasaArticuloModel($datos);

            if ($reporte == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'inventario/panelControl");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function editarInventarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario_edit']) &&
            !empty($_POST['id_inventario_edit']) &&
            isset($_POST['descripcion_edit']) &&
            !empty($_POST['descripcion_edit']) &&
            isset($_POST['area_edit']) &&
            !empty($_POST['area_edit'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario_edit'],
                'descripcion' => $_POST['descripcion_edit'],
                'marca' => $_POST['marca_edit'],
                'modelo' => $_POST['modelo_edit'],
                'precio' => $_POST['precio_edit'],
                'id_user' => $_POST['user_edit'],
                'id_area' => $_POST['area_edit']
            );

            $guardar = ModeloInventario::editarInventarioModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Actualizado Correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . BASE_URL . 'listado/index");
                }
                </script>';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                </script>
                ';
            }
        }
    }



    public function agregarMaterialControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_super_empresa']) &&
            !empty($_POST['id_super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion']) &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {

            $area = ModeloInventario::buscarAreaUsuarioControl($_POST['usuario']);
            $id_area = $area['id_area'];
            mt_srand(6);

            for ($i = 0; $i < $_POST['cantidad']; $i++) {

                $codigo = $this->numeroAleatorio();

                $datos = array(
                    'id_super_empresa' => $_POST['id_super_empresa'],
                    'descripcion' => $_POST['descripcion'],
                    'id_user' => $_POST['usuario'],
                    'id_area' => $id_area,
                    'id_categoria' => 6,
                    'estado' => $_POST['estado'],
                    'codigo' => $codigo,
                    'id_log' => $_POST['id_log'],
                    'observacion' => 'Trabajo en casa'
                );

                $guardar = ModeloInventario::agregarMaterialTempModel($datos);
            }

            if ($guardar == TRUE) {

                $temporal = array(
                    'id_log' => $_POST['id_log'],
                    'id_super_empresa' => $_POST['id_super_empresa']
                );

                $guardar_articulo = ModeloInventario::guardarMaterialControl($temporal);

                if ($guardar_articulo == TRUE) {
                    echo '
                    <script>
                    ohSnap("Registrado Correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);
    
                    function recargarPagina(){
                        window.location.replace("' . BASE_URL . 'material/index");
                    }
                    </script>';
                } else {
                    echo '
                    <script>
                    ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
                    </script>
                    ';
                }
            }
        }
    }
}
