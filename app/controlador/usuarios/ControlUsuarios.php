<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';

class ControlUsuarios
{

    private static $instancia;

    public static function singleton_usuarios()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function mostrarUsuariosControl($super_empresa)
    {
        $mostrar = ModeloUsuarios::mostrarUsuariosModel($super_empresa);
        return $mostrar;
    }


    public function guardarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['documento']) &&
            !empty($_POST['documento']) &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre']) &&
            isset($_POST['correo']) &&
            !empty($_POST['correo'])
        ) {
            $pass = $_POST['password'];
            $conf_pass = $_POST['conf_password'];

            if ($conf_pass == $pass) {

                $pass_hash = Hash::hashpass($conf_pass);

                $datos = array(
                    'super_empresa' => $_POST['super_empresa'],
                    'id_log' => $_POST['id_log'],
                    'documento' => $_POST['documento'],
                    'nombre' => $_POST['nombre'],
                    'apellido' => $_POST['apellido'],
                    'telefono' => $_POST['telefono'],
                    'correo' => $_POST['correo'],
                    'usuario' => $_POST['usuario'],
                    'perfil' => $_POST['perfil'],
                    'asignatura' => $_POST['asignatura'],
                    'pass' => $pass_hash,
                );

                $guardar = ModeloUsuarios::guardarUsuarioModel($datos);

                if ($guardar == TRUE) {
                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Error al crear usuario", {color: "red"});
                    </script>
                    ';
                }
            } else {
                echo '
				<script>
				ohSnap("Contraseñas no coinciden", {color: "red"});
				</script>
				';
            }
        }
    }




    public function editarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user']) &&
            isset($_POST['nombre_edit']) &&
            !empty($_POST['nombre_edit']) &&
            isset($_POST['perfil_edit']) &&
            !empty($_POST['perfil_edit'])
        ) {
            $pass = $_POST['pass_editar'];
            $conf_pass = $_POST['conf_pass_editar'];
            $pass_old = $_POST['pass_old'];

            if ($pass == '' && $conf_pass == '') {

                $datos = array(
                    'id_user' => $_POST['id_user'],
                    'documento' => $_POST['documento_edit'],
                    'nombre' => $_POST['nombre_edit'],
                    'apellido' => $_POST['apellido_edit'],
                    'telefono' => $_POST['telefono_edit'],
                    'perfil' => $_POST['perfil_edit'],
                    'asignatura' => $_POST['asignatura_edit'],
                    'pass' => $pass_old
                );
            } else if ($conf_pass == $pass) {

                $pass_hash = Hash::hashpass($conf_pass);

                $datos = array(
                    'id_user' => $_POST['id_user'],
                    'nombre' => $_POST['nombre_edit'],
                    'apellido' => $_POST['apellido_edit'],
                    'telefono' => $_POST['telefono_edit'],
                    'perfil' => $_POST['perfil_edit'],
                    'asignatura' => $_POST['asignatura_edit'],
                    'pass' => $pass_hash
                );
            } else {
                echo '
				<script>
				ohSnap("Contraseñas no coinciden", {color: "red"});
				</script>
				';
            }

            $guardar = ModeloUsuarios::editarUsuarioModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al crear usuario", {color: "red"});
                </script>
                ';
            }
        }
    }





    public function verificarDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {
            $documento = $_POST['documento'];

            $buscar = ModeloUsuarios::buscarDocumentoModel($documento);

            if ($buscar['id_user'] != "") {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }



    public function verificarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {
            $usuario = $_POST['usuario'];

            $buscar = ModeloUsuarios::verificarUsuarioModel($usuario);

            if ($buscar['id_user'] != "") {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }



    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha = date('Y-m-d H:i:s');

            $datos = array('id_user' => $id_user, 'fecha' => $fecha);

            $buscar = ModeloUsuarios::inactivarUsuarioModel($datos);

            if ($buscar == TRUE) {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }



    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha = date('Y-m-d H:i:s');

            $datos = array('id_user' => $id_user, 'fecha' => $fecha);

            $buscar = ModeloUsuarios::activarUsuarioModel($datos);

            if ($buscar == TRUE) {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }
}
