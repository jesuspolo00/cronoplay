<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'admisiones' . DS . 'ModeloAdmisiones.php';
require_once MODELO_PATH . 'matricula' . DS . 'ModeloMatricula.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once CONTROL_PATH . 'hash.php';

class ControlAdmisiones
{

    private static $instancia;

    public static function singleton_admisiones()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function datosClinicosControl($id)
    {
        $mostrar = ModeloAdmisiones::datosClinicosModel($id);
        return $mostrar;
    }


    public function mostrarHermanosControl($id)
    {
        $mostrar = ModeloAdmisiones::mostrarHermanosModel($id);
        return $mostrar;
    }


    public function mostrarAcudientesControl($super_empresa)
    {
        $mostrar = ModeloAdmisiones::mostrarAcudientesModel($super_empresa);
        return $mostrar;
    }


    public function guardarAcudienteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['documento']) &&
            !empty($_POST['documento']) &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre']) &&
            isset($_POST['correo']) &&
            !empty($_POST['correo'])
        ) {

            $pass_hash = Hash::hashpass('cronoplay123456@');

            $datos = array(
                'super_empresa' => 1,
                'id_log' => $_POST['id_log'],
                'documento' => $_POST['documento'],
                'nombre' => $_POST['nombre'],
                'apellido' => $_POST['apellido'],
                'telefono' => $_POST['telefono'],
                'correo' => $_POST['correo'],
                'usuario' => $_POST['documento'],
                'perfil' => 6,
                'asignatura' => '',
                'pass' => $pass_hash,
                'id_nivel' => 5
            );

            $guardar = ModeloAdmisiones::guardarAcudienteModel($datos);

            if ($guardar == TRUE) {
                echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
            } else {
                echo '
                    <script>
                    ohSnap("Error al crear usuario", {color: "red"});
                    </script>
                    ';
            }
        }
    }


    public function habilitarFormatoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $id = $_POST['id'];

            $actualizar = ModeloAdmisiones::habilitarFormatoModel($id);

            $rs = ($actualizar == TRUE) ? 'ok' : 'error';
            return $rs;
        }
    }


    public function archivarFormatoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $id = $_POST['id'];

            $actualizar = ModeloAdmisiones::archivarFormatoModel($id);

            $rs = ($actualizar == TRUE) ? 'ok' : 'error';
            return $rs;
        }
    }


    public function seleccionarFormatoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $id = $_POST['id'];

            $actualizar = ModeloAdmisiones::seleccionarFormatoModel($id);


            if ($actualizar == TRUE) {

                $pass_hash = Hash::hashpass('cronoplay123456@');

                $datos_usuario = ModeloAdmisiones::datosAcudienteModelo($id);

                $datos_registro = array(
                    'documento' => $datos_usuario['documento'],
                    'nombre' => $datos_usuario['nombre'],
                    'apellido' => $datos_usuario['apellido'],
                    'correo' => $datos_usuario['correo'],
                    'telefono' => $datos_usuario['telefono'],
                    'user' => $datos_usuario['user'],
                    'pass' => $pass_hash,
                    'perfil' => 2,
                    'id_log' => $_POST['id_log']
                );

                $matricula = ModeloMatricula::registrarUsuarioMatriculaModel($datos_registro);

                if ($matricula == TRUE) {
                    $mensaje = '<p>Estimado usuario ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . ', su registro ha sido exitoso en el siguiente correo le informamos su usuario y contrase&ntilde;a para poder ingresar al software.
                    <ul>
                        <li>Usuario: ' . $datos_usuario['documento'] . '</li>
                        <li>Contrase&ntilde;a: cronoplay123456@</li>
                    </ul>

                    <span style="color:crimson">Favor cambiar la contrase&ntilde;a una vez ingrese al sistema en el apartado de perfil, para que no se presente ningun inconveniente.</span>
                    </p>';


                    $datos_correo = array(
                        'asunto' => 'Registro en software de matriculas',
                        'correo' => $datos_usuario['correo'],
                        'user' => $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'],
                        'mensaje' => $mensaje
                    );


                    $enviar = Correo::enviarCorreoModel($datos_correo);

                    $rs = 'ok';
                }
            } else {
                $rs = 'error';
            }
            return $rs;
        }
    }
}
