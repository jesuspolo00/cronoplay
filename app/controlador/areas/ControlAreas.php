<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'areas' . DS . 'ModeloAreas.php';

class ControlAreas
{

    private static $instancia;

    public static function singleton_areas()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarAreasControl($super_empresa)
    {
        $mostrar = ModeloAreas::mostrarAreasModel($super_empresa);
        return $mostrar;
    }


    public function guradarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre'])
        ) {
            $datos = array(
                'id_log' => $_POST['id_log'],
                'nombre' => $_POST['nombre'],
                'super_empresa' => $_POST['super_empresa']
            );

            $guardar = ModeloAreas::guardarAreaModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al crear usuario", {color: "red"});
                </script>
                ';
            }
        }
    }




    public function editarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_area']) &&
            !empty($_POST['id_area']) &&
            isset($_POST['nom_edit']) &&
            !empty($_POST['nom_edit'])
        ) {
            $datos = array(
                'id_area' => $_POST['id_area'],
                'nombre' => $_POST['nom_edit']
            );

            $guardar = ModeloAreas::editarAreaModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al crear usuario", {color: "red"});
                </script>
                ';
            }
        }
    }


    public function usuarioResponsableControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_area']) &&
            !empty($_POST['id_area'])
        ) {
            $id = $_POST['id_area'];

            $guardar = ModeloAreas::usuarioResponsableModel($id);

            if ($guardar != FALSE) {
                $rs = $guardar['usuario'];
            } else {
                $rs = 'error';
            }

            return $rs;
        }
    }


    public function asignarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_area']) &&
            !empty($_POST['id_area'])
        ) {
            $datos = array(
                'id_area' => $_POST['id_area'],
                'id_user' => $_POST['usuario']
            );

            $guardar = ModeloAreas::asignarAreaModel($datos);

            if($guardar == TRUE){
                echo '
                <script>
                ohSnap("Asignado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("reasignar");
                }
                </script>
                ';
            }
        }
    }
}
