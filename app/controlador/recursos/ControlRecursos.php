<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'recursos' . DS . 'ModeloRecursos.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';

class ControlRecursos
{

    private static $instancia;

    public static function singleton_recursos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarTipoDocumentoControl($super_empresa)
    {
        $mostrar = ModeloRecursos::mostrarTipoDocumentoModel($super_empresa);
        return $mostrar;
    }

    public function mostrarSolicitudesIdControl($id)
    {
        $mostrar = ModeloRecursos::mostrarSolicitudesIdModel($id);
        return $mostrar;
    }

    public function mostrarSolicitudesControl($super_empresa)
    {
        $mostrar = ModeloRecursos::mostrarSolicitudesControl($super_empresa);
        return $mostrar;
    }

    public function certificadoMostrarControl($id)
    {
        $mostrar = ModeloRecursos::certificadoMostrarModel($id);
        return $mostrar;
    }

    public function mostrarDatosTipoDocumentoControl($id)
    {
        $mostrar = ModeloRecursos::mostrarDatosTipoDocumentoModel($id);
        return $mostrar;
    }


    public function solicitarCertificadoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_super_empresa']) &&
            !empty($_POST['id_super_empresa']) &&
            isset($_POST['lugar']) &&
            !empty($_POST['lugar']) &&
            isset($_POST['cargo']) &&
            !empty($_POST['cargo'])
        ) {
            $datos = array(
                'id_user' => $_POST['id_log'],
                'id_super_empresa' => $_POST['id_super_empresa'],
                'lugar' => $_POST['lugar'],
                'cargo' => $_POST['cargo'],
                'nombre_entidad' => $_POST['nom_entidad'],
                'trabaja_act' => $_POST['trabaja'],
                'tipo_cert' => $_POST['tipo_cert'],
                'anio' => $_POST['anio']
            );

            $datos_usuario = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
            $tipo_doc = ModeloRecursos::mostrarDatosTipoDocumentoModel($_POST['tipo_cert']);

            $guardar = ModeloRecursos::solicitarCertificadoModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("certificados");
                }
                </script>
                ';

                $mensaje = '
                <p style="font-size: 1.3em;">
                    El usuario <span style="font-weight: bold;">' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</span> ha realizado la solicitud para un certificado con la siguiente informacion diligenciada.
                    <ul style="font-size: 1.2em;">
                        <li><span style="font-weight: bold;">Documento: </span>' . $datos_usuario['documento'] . '</li>
                        <li><span style="font-weight: bold;">Lugar de expedición: </span>' . $_POST['lugar'] . '</li>
                        <li><span style="font-weight: bold;">Cargo: </span>' . $_POST['cargo'] . '</li>
                        <li><span style="font-weight: bold;">Nombre entidad: </span>' . $_POST['nom_entidad'] . '</li>
                        <li><span style="font-weight: bold;">Trabaja actualmente: </span>' . $_POST['trabaja'] . '</li>
                        <li><span style="font-weight: bold;">Certificado solicitado: </span>' . $tipo_doc['nombre'] . '</li>
                        <li><span style="font-weight: bold;">Año gravable: </span>' . $_POST['anio'] . '</li>
                    </ul>
                </p>
                ';

                $datos_correo = array(
                    'asunto' => 'Solicitud de certificado',
                    /* 'correo' => 'jesuspolo00@gmail.com',
                    'user' => 'Jesus Polo', */
                    'correo' => 'juan.lopez@royalschool.edu.co',
                    'user' => 'Juan Lopez',
                    'mensaje' => $mensaje
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
            } else {
                echo '
                <script>
                ohSnap("Error al crear usuario", {color: "red"});
                </script>
                ';
            }
        }
    }


    public function subirArchivoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_sol']) &&
            !empty($_POST['id_sol'])
        ) {

            $datos = array(
                'archivo' => $_FILES['archivo']['name'],
                'id_sol' => $_POST['id_sol'],
                'id_log' => $_POST['id_log']
            );

            $guardar = $this->guardarArchivoControl($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("solicitados");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al crear usuario", {color: "red"});
                </script>
                ';
            }
        }
    }


    public function guardarArchivoControl($datos)
    {
        //obtener el nombre del archivo
        $nom_arch = $datos['archivo'];
        //extraer la extencion del archivo de el archivo
        $ext_arch = explode(".", $nom_arch);
        $ext_arch = end($ext_arch);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['id_sol'] . '_' . $fecha_arch)) . '.' . $ext_arch;

        $datos_temp = array(
            'nombre' => $nombre_archivo,
            'id_sol' => $_POST['id_sol'],
            'id_log' => $_POST['id_log']
        );

        $guardar_cert = ModeloRecursos::subirArchivoModel($datos_temp);

        if ($guardar_cert == TRUE) {
            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
                move_uploaded_file($_FILES['archivo']['tmp_name'], $ruta_img);
            }

            return TRUE;
        }
    }
}
