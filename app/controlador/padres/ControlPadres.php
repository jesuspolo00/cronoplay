<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'padres' . DS . 'ModeloPadres.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPadres
{

    private static $instancia;

    public static function singleton_padres()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function consultarSolicitudIngresoControl($id)
    {
        $mostrar = ModeloPadres::consultarSolicitudIngresoModel($id);
        return $mostrar;
    }

    public function archivoRecomendacionControl($id, $formato)
    {
        $mostrar = ModeloPadres::archivoRecomendacionModel($id, $formato);
        return $mostrar;
    }

    public function fotoEstudianteControl($id, $formato)
    {
        $mostrar = ModeloPadres::fotoEstudianteModel($id, $formato);
        return $mostrar;
    }

    public function fotoPadreControl($id, $formato)
    {
        $mostrar = ModeloPadres::fotoPadreModel($id, $formato);
        return $mostrar;
    }

    public function fotoMadreControl($id, $formato)
    {
        $mostrar = ModeloPadres::fotoMadreModel($id, $formato);
        return $mostrar;
    }

    public function consultarFormatoRecomendacionControl($id)
    {
        $mostrar = ModeloPadres::consultarFormatoRecomendacionModel($id);
        return $mostrar;
    }

    public function consultarIdFormatoControl($id)
    {
        $mostrar = ModeloPadres::consultarIdFormatoModel($id);
        return $mostrar;
    }

    public function consultarFormatoLlenoControl($id)
    {
        $mostrar = ModeloPadres::consultarFormatoLlenoModel($id);
        return $mostrar;
    }

    public function fotosFormatoControl($id)
    {
        $mostrar = ModeloPadres::fotosFormatoModel($id);
        return $mostrar;
    }

    public function guardarFormatoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente'])
        ) {
            $id_acudiente = $_POST['id_acudiente'];
            $grado_ap = $_POST['grado_ap'];
            $nom_est = $_POST['nom_est'];
            $lug_nac = $_POST['lug_nac'];
            $edad_est = $_POST['edad_est'];
            $col_est = $_POST['col_est'];
            $grado_est = $_POST['grado_est'];
            $rel_est = $_POST['rel_est'];
            $num_hermanos = $_POST['num_hermanos'];
            $lug_ocup = $_POST['lug_ocup'];
            $nom_padre = $_POST['nom_padre'];
            $dir_padre = $_POST['dir_padre'];
            $tel_padre = $_POST['tel_padre'];
            $ocup_padre = $_POST['ocup_padre'];
            $tel_of_padre = $_POST['tel_of_padre'];
            $correo_padre = $_POST['correo_padre'];
            $cel_padre = $_POST['cel_padre'];
            $emp_padre = $_POST['emp_padre'];
            $cargo_padre = $_POST['cargo_padre'];
            $nom_madre = $_POST['nom_madre'];
            $dir_madre = $_POST['dir_madre'];
            $tel_madre = $_POST['tel_madre'];
            $ocup_madre = $_POST['ocup_madre'];
            $tel_of_madre = $_POST['tel_of_madre'];
            $correo_madre = $_POST['correo_madre'];
            $cel_madre = $_POST['cel_madre'];
            $emp_madre = $_POST['emp_madre'];
            $cargo_madre = $_POST['cargo_madre'];
            $vive_ambos = $_POST['vive_ambos'];
            $resp_est = $_POST['resp_est'];
            $nom_resp = $_POST['nom_resp'];
            $paren_resp = $_POST['paren_resp'];
            $dir_resp = $_POST['dir_resp'];
            $tel_resp = $_POST['tel_resp'];
            $cel_resp = $_POST['cel_resp'];
            $fam_col = $_POST['fam_col'];
            $fam_curso = $_POST['fam_curso'];
            $nom_medico = $_POST['nom_medico'];
            $tel_medico = $_POST['tel_medico'];
            $enf_est = $_POST['enf_est'];
            $cuid_esp = $_POST['cuid_esp'];
            $ayu_prof = $_POST['ayu_prof'];
            $profe_prof = $_POST['profe_prof'];
            $cal_col = $_POST['cal_col'];
            $nom_cal_col = $_POST['nom_cal_col'];
            $nom_ref = $_POST['nom_ref'];
            $paren_ref = $_POST['paren_ref'];
            $dir_ref = $_POST['dir_ref'];
            $tel_ref = $_POST['tel_ref'];
            $reco_jar = $_POST['reco_jar'];
            $int_per = $_POST['int_per'];
            $observacion = $_POST['observacion'];
            $cual_enf = $_POST['cual_enf'];
            $cual_esp = $_POST['cual_esp'];
            $nom_prof = $_POST['nom_prof'];
            $tel_prof = $_POST['tel_prof'];

            $datos = array(
                'id_acudiente' =>  $id_acudiente,
                'grado_ap' =>  $grado_ap,
                'nom_est' =>  $nom_est,
                'lug_nac' =>  $lug_nac,
                'edad_est' =>  $edad_est,
                'col_est' =>  $col_est,
                'grado_est' =>  $grado_est,
                'rel_est' =>  $rel_est,
                'num_hermanos' =>  $num_hermanos,
                'lug_ocup' =>  $lug_ocup,
                'nom_padre' =>  $nom_padre,
                'dir_padre' =>  $dir_padre,
                'tel_padre' =>  $tel_padre,
                'ocup_padre' =>  $ocup_padre,
                'tel_of_padre' =>  $tel_of_padre,
                'correo_padre' =>  $correo_padre,
                'cel_padre' =>  $cel_padre,
                'emp_padre' =>  $emp_padre,
                'cargo_padre' =>  $cargo_padre,
                'nom_madre' =>  $nom_madre,
                'dir_madre' =>  $dir_madre,
                'tel_madre' =>  $tel_madre,
                'ocup_madre' =>  $ocup_madre,
                'tel_of_madre' =>  $tel_of_madre,
                'correo_madre' =>  $correo_madre,
                'cel_madre' =>  $cel_madre,
                'emp_madre' =>  $emp_madre,
                'cargo_madre' =>  $cargo_madre,
                'vive_ambos' =>  $vive_ambos,
                'resp_est' =>  $resp_est,
                'nom_resp' =>  $nom_resp,
                'paren_resp' =>  $paren_resp,
                'dir_resp' =>  $dir_resp,
                'tel_resp' =>  $tel_resp,
                'cel_resp' =>  $cel_resp,
                'fam_col' =>  $fam_col,
                'fam_curso' =>  $fam_curso,
                'nom_medico' =>  $nom_medico,
                'tel_medico' =>  $tel_medico,
                'enf_est' =>  $enf_est,
                'cuid_esp' =>  $cuid_esp,
                'ayu_prof' =>  $ayu_prof,
                'profe_prof' =>  $profe_prof,
                'cal_col' =>  $cal_col,
                'nom_cal_col' =>  $nom_cal_col,
                'nom_ref' =>  $nom_ref,
                'paren_ref' =>  $paren_ref,
                'dir_ref' =>  $dir_ref,
                'tel_ref' =>  $tel_ref,
                'reco_jar' =>  $reco_jar,
                'int_per' =>  $int_per,
                'observacion' =>  $observacion,
                'cual_enf' => $cual_enf,
                'cual_esp' => $cual_esp,
                'nom_prof' => $nom_prof,
                'tel_prof' => $tel_prof
            );

            $guardar = ModeloPadres::guardarFormatoModel($datos);

            if ($guardar == TRUE) {

                //$formato = ModeloPadres::formatoLlenoModel($id_acudiente);

                echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("inicio/recomendacion");
                    }
                    </script>
                    ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function guardarRecomendacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_formato']) &&
            !empty($_POST['id_formato'])
        ) {
            //obtener el nombre del archivo
            $nom_arch = $_FILES['carta']['name'];
            //extraer la extencion del archivo de el archivo
            $ext_arch = explode(".", $nom_arch);
            $ext_arch = end($ext_arch);
            $fecha_arch = date('YmdHis');

            $nombre_archivo = strtolower(md5($_POST['id_acudiente'] . '_carta' . $fecha_arch)) . '.' . $ext_arch;

            $datos_temp = array(
                'nombre' => $nombre_archivo,
                'id_acudiente' => $_POST['id_acudiente'],
                'id_formato' => $_POST['id_formato']
            );

            $guardar_evidencia = ModeloPadres::guardarRecomendacionModel($datos_temp);

            if ($guardar_evidencia == TRUE) {
                //ruta donde de alojamiento el archivo
                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img = $carp_destino . $nombre_archivo;

                //verificar si subio el archivo y se mueve a su destino
                if (is_uploaded_file($_FILES['carta']['tmp_name'])) {
                    move_uploaded_file($_FILES['carta']['tmp_name'], $ruta_img);
                }

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("archivos");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function guardarArchivosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente'])
        ) {

            if (!empty($_FILES['fot_est']['name'])) {

                $datos_fot_est = array(
                    'id_acudiente' => $_POST['id_acudiente'],
                    'archivo' => $_FILES['fot_est']['name'],
                    'tipo_arch' => 'fot_est',
                    'id_formato' => $_POST['id_formato']
                );

                $guardar = $this->guardarDocumentoControl($datos_fot_est);
            }

            if (!empty($_FILES['fot_padre']['name'])) {

                $datos_fot_padre = array(
                    'id_acudiente' => $_POST['id_acudiente'],
                    'archivo' => $_FILES['fot_padre']['name'],
                    'tipo_arch' => 'fot_padre',
                    'id_formato' => $_POST['id_formato']
                );
                $guardar = $this->guardarDocumentoControl($datos_fot_padre);
            }

            if (!empty($_FILES['fot_madre']['name'])) {
                $datos_fot_madre = array(
                    'id_acudiente' => $_POST['id_acudiente'],
                    'archivo' => $_FILES['fot_madre']['name'],
                    'tipo_arch' => 'fot_madre',
                    'id_formato' => $_POST['id_formato']
                );
                $guardar = $this->guardarDocumentoControl($datos_fot_madre);
            }

            if (!empty($_FILES['cert_banc']['name'])) {
                $datos_fot_madre = array(
                    'id_acudiente' => $_POST['id_acudiente'],
                    'archivo' => $_FILES['cert_banc']['name'],
                    'tipo_arch' => 'cert_banc',
                    'id_formato' => $_POST['id_formato']
                );
                $guardar = $this->guardarDocumentoControl($datos_fot_madre);
            }

            if ($guardar == TRUE) {

                $formato = ModeloPadres::formatoLlenoModel($_POST['id_acudiente']);

                $datos_acudiente = ModeloPadres::datosPadresModelo($_POST['id_acudiente']);

                if ($formato == TRUE) {


                    $mensaje = '<p>El siguiente correo es para informar que el usuario ' . $datos_acudiente['nombre'] . ' ' . $datos_acudiente['apellido'] . ' ha completado el proceso de admision.</p>';

                    $datos_correo = array(
                        'asunto' => 'Proceso de admision',
                        'correo' => 'angel.vargas@royalschool.edu.co',
                        /* 'correo' => 'jesuspolo00@gmail.com', */
                        'user' => $datos_acudiente['nombre'] . ' ' . $datos_acudiente['apellido'],
                        'mensaje' => $mensaje
                    );

                    $enviar = Correo::enviarCorreoModel($datos_correo);


                    echo '
                        <script>
                        ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                        setTimeout(recargarPagina,1050);
    
                        function recargarPagina(){
                            window.location.replace("../salir");
                        }
                        </script>
                        ';
                } else {
                    echo '
                        <script>
                        ohSnap("Ha ocurrido un error", {color: "red"});
                        </script>
                        ';
                }
            }
        }
    }


    public function guardarDocumentoControl($datos)
    {
        //obtener el nombre del archivo
        $nom_arch = $datos['archivo'];
        //extraer la extencion del archivo de el archivo
        $ext_arch = explode(".", $nom_arch);
        $ext_arch = end($ext_arch);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['tipo_arch'] . '_' . $fecha_arch)) . '.' . $ext_arch;

        $datos_temp = array(
            'nombre' => $nombre_archivo,
            'id_acudiente' => $datos['id_acudiente'],
            'id_formato' => $datos['id_formato'],
            'tipo' => $datos['tipo_arch']
        );

        $guardar_evidencia = ModeloPadres::guardarDocumentoModel($datos_temp);

        if ($guardar_evidencia == TRUE) {
            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES[$datos['tipo_arch']]['tmp_name'])) {
                move_uploaded_file($_FILES[$datos['tipo_arch']]['tmp_name'], $ruta_img);
            }

            return TRUE;
        }
    }



    public function confirmarFormatoSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_formato']) &&
            !empty($_POST['id_formato'])
        ) {
            $id_formato = $_POST['id_formato'];
            $id_acudiente = $_POST['id_acudiente'];
            $grado_ap = $_POST['grado_ap'];
            $nom_est = $_POST['nom_est'];
            $lug_nac = $_POST['lug_nac'];
            $edad_est = $_POST['edad_est'];
            $col_est = $_POST['col_est'];
            $grado_est = $_POST['grado_est'];
            $rel_est = $_POST['rel_est'];
            $num_hermanos = $_POST['num_hermanos'];
            $lug_ocup = $_POST['lug_ocup'];
            $nom_padre = $_POST['nom_padre'];
            $dir_padre = $_POST['dir_padre'];
            $tel_padre = $_POST['tel_padre'];
            $ocup_padre = $_POST['ocup_padre'];
            $tel_of_padre = $_POST['tel_of_padre'];
            $correo_padre = $_POST['correo_padre'];
            $cel_padre = $_POST['cel_padre'];
            $emp_padre = $_POST['emp_padre'];
            $cargo_padre = $_POST['cargo_padre'];
            $nom_madre = $_POST['nom_madre'];
            $dir_madre = $_POST['dir_madre'];
            $tel_madre = $_POST['tel_madre'];
            $ocup_madre = $_POST['ocup_madre'];
            $tel_of_madre = $_POST['tel_of_madre'];
            $correo_madre = $_POST['correo_madre'];
            $cel_madre = $_POST['cel_madre'];
            $emp_madre = $_POST['emp_madre'];
            $cargo_madre = $_POST['cargo_madre'];
            $vive_ambos = $_POST['vive_ambos'];
            $resp_est = $_POST['resp_est'];
            $nom_resp = $_POST['nom_resp'];
            $paren_resp = $_POST['paren_resp'];
            $dir_resp = $_POST['dir_resp'];
            $tel_resp = $_POST['tel_resp'];
            $cel_resp = $_POST['cel_resp'];
            $fam_col = $_POST['fam_col'];
            $fam_curso = $_POST['fam_curso'];
            $nom_medico = $_POST['nom_medico'];
            $tel_medico = $_POST['tel_medico'];
            $enf_est = $_POST['enf_est'];
            $cuid_esp = $_POST['cuid_esp'];
            $ayu_prof = $_POST['ayu_prof'];
            $profe_prof = $_POST['profe_prof'];
            $cal_col = $_POST['cal_col'];
            $nom_cal_col = $_POST['nom_cal_col'];
            $nom_ref = $_POST['nom_ref'];
            $paren_ref = $_POST['paren_ref'];
            $dir_ref = $_POST['dir_ref'];
            $tel_ref = $_POST['tel_ref'];
            $reco_jar = $_POST['reco_jar'];
            $int_per = $_POST['int_per'];
            $observacion = $_POST['observacion'];

            $datos = array(
                'id_formato' => $id_formato,
                'id_acudiente' =>  $id_acudiente,
                'grado_ap' =>  $grado_ap,
                'nom_est' =>  $nom_est,
                'lug_nac' =>  $lug_nac,
                'edad_est' =>  $edad_est,
                'col_est' =>  $col_est,
                'grado_est' =>  $grado_est,
                'rel_est' =>  $rel_est,
                'num_hermanos' =>  $num_hermanos,
                'lug_ocup' =>  $lug_ocup,
                'nom_padre' =>  $nom_padre,
                'dir_padre' =>  $dir_padre,
                'tel_padre' =>  $tel_padre,
                'ocup_padre' =>  $ocup_padre,
                'tel_of_padre' =>  $tel_of_padre,
                'correo_padre' =>  $correo_padre,
                'cel_padre' =>  $cel_padre,
                'emp_padre' =>  $emp_padre,
                'cargo_padre' =>  $cargo_padre,
                'nom_madre' =>  $nom_madre,
                'dir_madre' =>  $dir_madre,
                'tel_madre' =>  $tel_madre,
                'ocup_madre' =>  $ocup_madre,
                'tel_of_madre' =>  $tel_of_madre,
                'correo_madre' =>  $correo_madre,
                'cel_madre' =>  $cel_madre,
                'emp_madre' =>  $emp_madre,
                'cargo_madre' =>  $cargo_madre,
                'vive_ambos' =>  $vive_ambos,
                'resp_est' =>  $resp_est,
                'nom_resp' =>  $nom_resp,
                'paren_resp' =>  $paren_resp,
                'dir_resp' =>  $dir_resp,
                'tel_resp' =>  $tel_resp,
                'cel_resp' =>  $cel_resp,
                'fam_col' =>  $fam_col,
                'fam_curso' =>  $fam_curso,
                'nom_medico' =>  $nom_medico,
                'tel_medico' =>  $tel_medico,
                'enf_est' =>  $enf_est,
                'cuid_esp' =>  $cuid_esp,
                'ayu_prof' =>  $ayu_prof,
                'profe_prof' =>  $profe_prof,
                'cal_col' =>  $cal_col,
                'nom_cal_col' =>  $nom_cal_col,
                'nom_ref' =>  $nom_ref,
                'paren_ref' =>  $paren_ref,
                'dir_ref' =>  $dir_ref,
                'tel_ref' =>  $tel_ref,
                'reco_jar' =>  $reco_jar,
                'int_per' =>  $int_per,
                'observacion' =>  $observacion,
                'confirmado' => 'si'
            );

            $guardar = ModeloPadres::confirmarFormatoSolicitudModel($datos);

            if ($guardar == TRUE) {
                echo '
                    <script>
                    ohSnap("Actualizado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("../admisiones/index");
                    }
                    </script>
                    ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }


    public function confirmarRecomendacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_formato']) &&
            !empty($_POST['id_formato'])
        ) {
            $datos = array(
                'id_acudiente' => $_POST['id_acudiente'],
                'fam_recomend' => $_POST['fam_recomend'],
                'anio_conoce' => $_POST['anio_conoce'],
                'nom_est' => $_POST['nom_est'],
                'grado_asp' => $_POST['grado_asp'],
                'vincu_union' => $_POST['vincu_union'],
                'considero' => $_POST['considero'],
                'vinculo' => $_POST['vinculo'],
                'confirmado' => 'si',
                'id_formato' => $_POST['id_formato'],
                'id_recomendacion' => $_POST['id_recomendacion']
            );

            $guardar = ModeloPadres::confirmarRecomendacionModel($datos);

            if ($guardar == TRUE) {
                echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("../admisiones/index");
                    }
                    </script>
                    ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }
}
