<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'reportes' . DS . 'ModeloReportes.php';

class ControlReporte
{

    private static $instancia;

    public static function singleton_reporte()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarReporteDanoControl($super_empresa)
    {
        $mostrar = ModeloReportes::mostrarReporteDanoModel($super_empresa);
        return $mostrar;
    }


    public function mostrarReporteMantControl($super_empresa)
    {
        $mostrar = ModeloReportes::mostrarReporteMantModel($super_empresa);
        return $mostrar;
    }

    public function mostrarInformacionSolucionReporteControl($id)
    {
        $mostrar = ModeloReportes::mostrarInformacionSolucionReporteModel($id);
        return $mostrar;
    }


    public function solucionarReporteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['firma1']) &&
            !empty($_POST['firma1']) &&
            isset($_POST['user']) &&
            !empty($_POST['user']) &&
            isset($_POST['inventario']) &&
            !empty($_POST['inventario'])
        ) {

            $fecha_resp = ($_POST['fecha_res'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_res'];

            $datos_reporte = array(
                'id_inventario' => $_POST['inventario'],
                'id_resp' => $_POST['resp'],
                'id_log' => $_POST['resp'],
                'id_user' => $_POST['user'],
                'observacion' => $_POST['observacion'],
                'estado' => 3,
                'fecha_respuesta' => $fecha_resp,
                'tipo_reporte' => $_POST['tipo_reporte'],
                'id_reporte' => $_POST['id_reporte'],
                'id_area' => $_POST['id_area'],
                'super_empresa' => $_POST['super_empresa']
            );

            $guardar_reporte = ModeloReportes::solucionarReporteModel($datos_reporte);
            $id_reporte = $guardar_reporte['id'];
            $reporte = $guardar_reporte['guardar'];

            if ($reporte == TRUE) {

                $id_user = $_POST['user'];
                $id_responsable =  $_POST['resp'];

                /*-------------------Firma 1-----------------------------*/
                $img = $_POST['firma1'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);

                $im = imagecreatefromstring($data);
                $negro = imagecolorallocate($im, 0, 0, 0);

                if ($im !== false) {
                    $nombre_firma1 = md5($id_user) . '.png';
                    imagecolortransparent($im, $negro);
                    imagepng($im, PUBLIC_PATH_ARCH . 'upload' . DS . $nombre_firma1); //guardar a server 
                    imagedestroy($im); //liberar memoria  
                }
                /*-----------------------------------------------------------*/

                /*-------------------Firma 2-----------------------------*/
                $img2 = $_POST['firma2'];
                $img2 = str_replace('data:image/png;base64,', '', $img2);
                $img2 = str_replace(' ', '+', $img2);
                $data2 = base64_decode($img2);

                $im2 = imagecreatefromstring($data2);
                $negro2 = imagecolorallocate($im2, 0, 0, 0);

                if ($im2 !== false) {
                    $nombre_firma2 = md5($id_responsable) . '.png';
                    imagecolortransparent($im2, $negro2);
                    imagepng($im2, PUBLIC_PATH_ARCH . 'upload' . DS . $nombre_firma2); //guardar a server 
                    imagedestroy($im2); //liberar memoria  
                }
                /*-----------------------------------------------------------*/
                $datos_firma = array(
                    'id_inventario' => $_POST['inventario'],
                    'id_reporte' => $id_reporte,
                    'firma_responsable' => $nombre_firma1,
                    'firma_solucionado' => $nombre_firma2,
                    'firma_user' => $_POST['op'],
                    'id_user' => $id_user,
                    'id_responsable' => $id_responsable,
                    'super_empresa' => $_POST['super_empresa']
                );

                $firmas = ModeloReportes::guardarFirmasModel($datos_firma);

                if ($firmas == TRUE) {
                    $rs = 'ok';
                } else {
                    $rs = 'no';
                }

                return $rs;
            }
        }
    }
}
