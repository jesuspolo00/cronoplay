<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'pmb' . DS . 'ModeloPmb.php';

class ControlPmb
{

    private static $instancia;

    public static function singleton_pmb()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function librosPrestadosControl()
    {
        $mostrar = ModeloPmb::librosPrestadosModel();
        return $mostrar;
    }

    public function librosPrestadosDocumentoControl($documento)
    {
        $mostrar = ModeloPmb::librosPrestadosDocumentoModel($documento);
        return $mostrar;
    }
}
