<?php
require_once MODELO_PATH . 'IngresoModel.php';
require_once CONTROL_PATH . 'Session.php';
require_once CONTROL_PATH . 'hash.php';
require_once LIB_PATH . 'PHPMailer/PHPMailerAutoload.php';
require_once MODELO_PATH . 'configMail.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
@session_start();

class ingresoClass
{

    private static $instancia;
    private $objsession;

    public static function singleton_ingreso()
    {

        if (!isset(self::$instancia)) {

            $miclase = __CLASS__;

            self::$instancia = new $miclase;
        }

        return self::$instancia;
    }


    public function ingresaruser()
    {
        if (
            isset($_POST['user']) &&
            !empty($_POST['user']) &&
            isset($_POST['pass']) &&
            !empty($_POST['pass'])
        ) {

            $user = filter_var($_POST['user']);
            $pass = filter_var($_POST['pass']);
            $rslt = IngresoModel::verificarUser($user);

            if ($rslt) {
                if ($rslt['estado'] == 'activo') {
                    if ($rslt['user'] === $user) {
                        $hash = $rslt['pass'];
                        if (Hash::verificar($hash, $pass)) {
                            $this->objsession = new Session;
                            $this->objsession->iniciar();
                            $this->objsession->SetSession('id', $rslt['id_user']);
                            $this->objsession->SetSession('documento', $rslt['documento']);
                            $this->objsession->SetSession('nombre_admin', $rslt['nombre']);
                            $this->objsession->SetSession('apellido', $rslt['apellido']);
                            $this->objsession->SetSession('rol', $rslt['perfil']);
                            $this->objsession->SetSession('empresa', $rslt['id_empresa']);
                            $this->objsession->SetSession('super_empresa', $rslt['id_super_empresa']);
                            header('Location:inicio');
                        } else {
                            $er = '1';
                            $error = base64_encode($er);
                            header('Location:login?er=' . $error);
                        }
                    } else {
                        $er = '1';
                        $error = base64_encode($er);
                        header('Location:login?er=' . $error);
                    }
                } else {
                    $er = '4';
                    $error = base64_encode($er);
                    header('Location:login?er=' . $error);
                }
            } else {
                $er = '3';
                $error = base64_encode($er);
                header('Location:login?er=' . $error);
            }
        }
    }



    public function agregarAcudienteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {

            $pass_hash = Hash::hashpass('cronoplay123456@');

            $datos = array(
                'documento' => $_POST['documento'],
                'nombre' => $_POST['nombre'],
                'apellido' => $_POST['apellido'],
                'correo' => $_POST['correo'],
                'telefono' => $_POST['telefono'],
                'perfil' => 6,
                'usuario' => $_POST['documento'],
                'pass' => $pass_hash,
                'id_log' => $_POST['id_log'],
                'id_nivel' => 5
            );

            $guardar = IngresoModel::agregarAcudienteModel($datos);

            if ($guardar == TRUE) {

                $mensaje = '<p>Estimado usuario ' . $_POST['nombre'] . ' ' . $_POST['apellido'] . ', su registro ha sido exitoso en el siguiente correo le informamos su usuario y contrase&ntilde;a para poder ingresar al software, para realizar el proceso de admision.
                    <ul>
                        <li>Usuario: ' . $_POST['documento'] . '</li>
                        <li>Contrase&ntilde;a: cronoplay123456@</li>
                    </ul>

                    <span style="color:crimson">Favor cambiar la contrase&ntilde;a una vez ingrese al sistema en el apartado de perfil, para que no se presente ningun inconveniente.</span>
                    </p>';


                $datos_correo = array(
                    'asunto' => 'Registro en software de matriculas',
                    'correo' => $_POST['correo'],
                    /* 'correo' => 'jesuspolo00@gmail.com', */
                    'user' => $_POST['nombre'] . ' ' . $_POST['apellido'],
                    'mensaje' => $mensaje
                );


                $enviar = Correo::enviarCorreoModel($datos_correo);

                echo '
                <script>
                    ohSnap("Correo Enviado!", { color: "green", "duration": "1000" });
                    setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("login");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                    ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }
}
