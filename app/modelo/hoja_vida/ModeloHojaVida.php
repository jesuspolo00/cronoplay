<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloHojaVida extends conexion
{


    public function mostrarDatosArticuloModel($id, $super_empresa)
    {
        $tabla = 'hoja_vida';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT *, h.id as id_hoja_vida, iv.fechareg as fecha_registro,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area
        FROM " . $tabla . " h 
        LEFT JOIN inventario iv ON h.id_inventario = iv.id
        WHERE h.id_inventario = :id AND h.id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function actualizarHojaVidaModel($datos)
    {
        var_dump($datos);
        $tabla = 'hoja_vida';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE hoja_vida SET direccion_ip = :di, grupo_trabajo = :gt, tipo_conexion = :tc, fecha_adquisicion = :fa, 
        frecuencia_mantenimiento = :fm, fecha_garantia = :fg, contacto_garantia = :cg, fecha_update = NOW(), id_log = :il WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':di', $datos['ip']);
            $preparado->bindParam(':gt', $datos['grupo']);
            $preparado->bindParam(':tc', $datos['tipo_con']);
            $preparado->bindParam(':fa', $datos['fecha_ad']);
            $preparado->bindParam(':fm', $datos['frec_mant']);
            $preparado->bindParam(':fg', $datos['fecha_gant']);
            $preparado->bindParam(':cg', $datos['contacto']);
            $preparado->bindValue(':id', $datos['id_hoja_vida']);
            $preparado->bindParam(':il', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarComponentesHardwareModel($id)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*, 
        (SELECT IF(h.id = '', 'no', 'si') FROM hardware h WHERE h.id_inventario IN(iv.id) AND h.activo = 1) AS asignado
        FROM " . $tabla . " iv WHERE iv.id_user = :id AND iv.activo = 1 AND iv.estado NOT IN(2,4,5,6) AND iv.id_categoria = 5;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function asignarComponenteHardwareModel($datos)
    {
        $tabla = 'hardware';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_hoja_vida, id_inventario, id_log) VALUES (:idh, :idv, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idh', $datos['id_hoja_vida']);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarComponentesAsignadoHardwareModel($id)
    {
        $tabla = 'hardware';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        h.id,
        h.id_hoja_vida,
        h.id_inventario,
        h.fechareg,
        (SELECT iv.descripcion FROM inventario iv WHERE iv.id IN(h.id_inventario)) AS descripcion,
        (SELECT iv.marca FROM inventario iv WHERE iv.id IN(h.id_inventario)) AS marca,
        (SELECT iv.modelo FROM inventario iv WHERE iv.id IN(h.id_inventario)) AS modelo,
        (SELECT iv.codigo FROM inventario iv WHERE iv.id IN(h.id_inventario)) AS codigo,
        (SELECT iv.observacion FROM inventario iv WHERE iv.id IN(h.id_inventario)) AS observacion
        FROM hardware h WHERE h.id_hoja_vida = :id AND h.activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function guardarComponenteSoftwareControl($datos)
    {
        $tabla = 'software';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_hoja_vida, descripcion, version, fabricante, licencia, observacion, id_log, id_super_empresa) 
        VALUES (:idh,:d,:v,:f,:l,:ob,:il,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idh', $datos['id_hoja_vida']);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':v', $datos['version']);
            $preparado->bindParam(':f', $datos['fabricante']);
            $preparado->bindParam(':l', $datos['licencia']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarComponentesAsignadoSoftwareModel($id)
    {
        $tabla = 'software';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_hoja_vida = :id AND activo = 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarReportesArticuloControl($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT *, 
        (SELECT e.nombre FROM estado e WHERE e.id = estado) as nombre_estado 
        FROM " . $tabla . " WHERE id_inventario = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarFechaReportadoModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT rp.fechareg FROM " . $tabla . " rp WHERE rp.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
