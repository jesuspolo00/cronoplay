<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloRecursos extends conexion
{
    public function mostrarTipoDocumentoModel($super_empresa)
    {
        $tabla = 'tipo_documento';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $super_empresa);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function solicitarCertificadoModel($datos)
    {
        $tabla = 'sol_certificado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO sol_certificados (id_user, lugar, cargo, nombre_entidad, trabaja_act, tipo_cert, id_super_empresa, anio) 
        VALUES (:idu,:l,:c,:ne,:ta,:t,:ids,:an);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':l', $datos['lugar']);
            $preparado->bindParam(':c', $datos['cargo']);
            $preparado->bindParam(':ne', $datos['nombre_entidad']);
            $preparado->bindParam(':ta', $datos['trabaja_act']);
            $preparado->bindParam(':t', $datos['tipo_cert']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindParam(':an', $datos['anio']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarSolicitudesIdModel($id)
    {
        $tabla = 'sol_certificados';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT s.*, 
        (SELECT t.nombre FROM tipo_documento t WHERE t.id = s.tipo_cert) as documento
        FROM " . $tabla . " s WHERE s.id_user = :id AND s.tipo_cert IN(1,2);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarSolicitudesControl($id)
    {
        $tabla = 'sol_certificados';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT s.*, 
        (SELECT t.nombre FROM tipo_documento t WHERE t.id = s.tipo_cert) as documento,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS usuario
        FROM " . $tabla . " s WHERE s.id_super_empresa = :id AND s.tipo_cert IN(1,2);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function subirArchivoModel($datos)
    {
        $tabla = 'sol_certificados';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2 WHERE id = :id;
        INSERT INTO documentos (nombre, id_sol, id_log) VALUES (:n,:id,:idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_sol']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function certificadoMostrarModel($id)
    {
        $tabla = 'documentos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_sol = :id ORDER BY id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarDatosTipoDocumentoModel($id)
    {
        $tabla = 'tipo_documento';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
