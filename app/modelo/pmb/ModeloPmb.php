<?php
require_once MODELO_PATH . 'conexion_pmb.php';

class ModeloPmb extends conexion_pmb
{
    public function librosPrestadosModel()
    {
        $tabla = 'empr';
        $cnx = conexion_pmb::singleton_conexion();
        $cmdsql = "SELECT 
        empr_cb AS documento,
        CONCAT(empr_prenom, ' ', empr_nom) AS nombre,
        DATE( pret_date ) AS prestado_el, 
        pret_retour AS devolver_el, 
        tdoc_libelle AS categoria,
        expl_cb AS codigo_barra, 
        expl_cote AS asignatura, 
        tit1 AS titulo
        FROM " . $tabla . "
        INNER JOIN pret ON empr.id_empr = pret.pret_idempr
        INNER JOIN pret_archive ON pret_archive.arc_id = pret.pret_arc_id
        INNER JOIN exemplaires ON exemplaires.expl_id = pret.pret_idexpl
        INNER JOIN notices ON pret_archive.arc_expl_notice = notices.notice_id
        INNER JOIN docs_type ON pret_archive.arc_expl_typdoc = docs_type.idtyp_doc";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function librosPrestadosDocumentoModel($documento)
    {
        $tabla = 'empr';
        $cnx = conexion_pmb::singleton_conexion();
        $cmdsql = "SELECT 
        empr_cb AS documento,
        CONCAT(empr_prenom, ' ', empr_nom) AS nombre,
        DATE( pret_date ) AS prestado_el, 
        pret_retour AS devolver_el, 
        tdoc_libelle AS categoria,
        expl_cb AS codigo_barra, 
        expl_cote AS asignatura, 
        tit1 AS titulo
        FROM " . $tabla . "
        INNER JOIN pret ON empr.id_empr = pret.pret_idempr
        INNER JOIN pret_archive ON pret_archive.arc_id = pret.pret_arc_id
        INNER JOIN exemplaires ON exemplaires.expl_id = pret.pret_idexpl
        INNER JOIN notices ON pret_archive.arc_expl_notice = notices.notice_id
        INNER JOIN docs_type ON pret_archive.arc_expl_typdoc = docs_type.idtyp_doc
        WHERE empr.empr_cb = :doc";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':doc', $documento);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
