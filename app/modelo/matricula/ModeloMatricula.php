<?php
require_once MODELO_PATH . 'conexion_matricula.php';

class ModeloMatricula extends conexion_matricula
{

    public function registrarUsuarioMatriculaModel($datos)
    {
        $tabla = 'usuarios';
        $cnx = conexion_matricula::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE documento = :d;
        INSERT INTO " . $tabla . " (documento, nombre, apellido, correo, telefono, user, pass, perfil, user_log, tipo_user) 
        VALUES (:d, :n, :a, :c, :t, :u, :p, :ip, :ul,:t);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':u', $datos['user']);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':ip', $datos['perfil']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':t', 2);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}