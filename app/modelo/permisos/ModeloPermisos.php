<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPermisos extends conexion
{

    public function permisosUsuarioModel($id_modulo, $id_opcion, $id_accion, $id_user)
    {
        $tabla = 'cron_permisos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 1 FROM cron_permisos p WHERE p.id_modulo = :im 
        AND p.id_opcion = :ip 
        AND p.id_accion = :ia 
        AND p.id_user = :iu 
        AND p.activo = 1
        ORDER BY p.id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':im', $id_modulo);
            $preparado->bindParam(':ip', $id_opcion);
            $preparado->bindParam(':ia', $id_accion);
            $preparado->bindParam(':iu', $id_user);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
