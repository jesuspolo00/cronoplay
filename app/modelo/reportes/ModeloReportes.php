<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloReportes extends conexion
{
    public function mostrarReporteDanoModel($super_empresa)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
        (SELECT r.fechareg FROM reportes r WHERE r.id_inventario IN(iv.id) AND r.estado = 2 ORDER BY r.id DESC LIMIT 1) AS fecha_reporte,
        (SELECT r.id FROM reportes r WHERE r.id_inventario IN(iv.id) AND r.estado = 2 ORDER BY r.id DESC LIMIT 1) as id_reporte
        FROM " . $tabla . " iv WHERE iv.estado IN(2) AND iv.activo = 1 AND iv.id_super_empresa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $super_empresa);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarReporteMantModel($super_empresa)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
        (SELECT r.fechareg FROM reportes r WHERE r.id_inventario IN(iv.id) AND r.estado = 6 ORDER BY r.id DESC LIMIT 1) AS fecha_reporte,
        (SELECT r.id FROM reportes r WHERE r.id_inventario IN(iv.id) AND r.estado = 6 ORDER BY r.id DESC LIMIT 1) as id_reporte
        FROM " . $tabla . " iv WHERE iv.estado IN(6) AND iv.activo = 1 AND iv.id_super_empresa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $super_empresa);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function consultarSolucionReporteDanoModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT id FROM " . $tabla . " WHERE id_inventario = :id AND estado = 3 AND tipo_reporte = 1 
        AND id IN(SELECT f.id_reporte FROM firmas f WHERE f.id_inventario = :id ORDER BY f.id DESC) ORDER BY id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function consultarSolucionReporteMantModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT id FROM " . $tabla . " WHERE id_inventario = :id AND estado = 3 AND tipo_reporte = 2 
        AND id IN(SELECT f.id_reporte FROM firmas f WHERE f.id_inventario = :id ORDER BY f.id DESC) ORDER BY id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function solucionarReporteModel($datos)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, observacion, estado, id_log, id_resp, fecha_respuesta, tipo_reporte, id_reporte, id_user, id_area) 
        VALUES(:idv, :ob, :e, :il, :ir, :fr, :tr, :idr, :idu, :ida);
        UPDATE inventario SET observacion = :ob, estado = :e WHERE id = :idv;
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) VALUES (:idv,:idu,:ida,:il,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ir', $datos['id_resp']);
            $preparado->bindValue(':fr', $datos['fecha_respuesta']);
            $preparado->bindValue(':tr', $datos['tipo_reporte']);
            $preparado->bindValue(':idr', $datos['id_reporte']);
            $preparado->bindValue(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id = $cnx->ultimoIngreso($tabla);
                $respuesta = array('id' => $id, 'guardar' => TRUE);
                return $respuesta;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function guardarFirmasModel($datos)
    {
        $tabla = 'firmas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, id_reporte, firma_responsable, firma_user, id_user, id_super_empresa, firma_solucionado, id_responsable) 
        VALUES (:idv, :ir, :fr, :fu, :iu, :ids, :fs, :idr);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ir', $datos['id_reporte']);
            $preparado->bindParam(':fr', $datos['firma_responsable']);
            $preparado->bindParam(':fu', $datos['firma_user']);
            $preparado->bindParam(':iu', $datos['id_user']);
            $preparado->bindParam(':fs', $datos['firma_solucionado']);
            $preparado->bindParam(':idr', $datos['id_responsable']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarInformacionSolucionReporteModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        rp.*,
        (SELECT iv.descripcion FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS descripcion,
        (SELECT iv.marca FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS marca,
        (SELECT (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS area,
        (SELECT f.firma_responsable FROM firmas f WHERE f.id_reporte IN(rp.id)) AS firma_responsable,
        (SELECT f.firma_solucionado FROM firmas f WHERE f.id_reporte IN(rp.id)) AS firma_solucionado,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(rp.id_user)) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id = rp.estado) AS nombre_estado
        FROM " . $tabla . " rp WHERE rp.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
