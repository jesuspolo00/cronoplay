<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPerfil extends conexion
{


    public function mostrarDatosPerfilModel($id)
    {
        $tabla = 'usuarios';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT u.*, (SELECT nombre from perfiles where id_perfil = u.perfil) as nom_perfil,
        (select nombre_foto from foto_perfil f where f.id_user = u.id_user and f.activo = 1) as imagen
        FROM " . $tabla . " u where u.id_user = :i";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':i', $id, PDO::PARAM_INT);
            if ($preparado->execute()) {
                if ($preparado->rowCount() == 1) {
                    return $preparado->fetch();
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarPerfilesModel($super_empresa)
    {
        $tabla = 'perfiles';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarDatosSuperEmpresaModel($super_empresa, $tipo_img)
    {
        $tabla = 'super_empresa';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT s.*, 
        (SELECT i.nombre FROM img_super_empresa i WHERE i.id_super_empresa = s.id AND i.tipo_img IN('$tipo_img')) AS imagen
        FROM " . $tabla . " s WHERE s.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function guardarPerfilModelo($datos)
    {
        $tabla = 'perfiles';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre,user_log,id_super_empresa,fechareg) VALUES (:n,:ul,:is,:fr)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ul', $datos['user_log']);
            $preparado->bindParam(':is', $datos['super_empresa']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            //$preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function editarPerfilesModel($datos)
    {
        $tabla = 'perfiles';
        $cnx = conexion::singleton_conexion();
        $sql = "UPDATE " . $tabla . " SET nombre = :n WHERE id_perfil = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindValue(':id', $datos['id_perfil']);
            if ($preparado->execute()) {
                $id = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => TRUE, 'id' => $id);
                return $resultado;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function editarPerfilModel($datos)
    {
        $tabla = 'usuarios';
        $cnx = conexion::singleton_conexion();
        $sql = "UPDATE " . $tabla . " SET nombre= :n,apellido= :a,correo= :c,telefono= :t,user= :u,pass= :p,perfil= :r, id_nivel = :nv WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':u', $datos['usuario']);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':r', $datos['perfil']);
            $preparado->bindParam(':nv', $datos['nivel']);
            $preparado->bindValue(':id', $datos['id_user']);
            if ($preparado->execute()) {
                $id = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => TRUE, 'id' => $id);
                return $resultado;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function eliminarPerfilModelo($id)
    {
        $tabla = 'perfiles';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_perfil = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function guardarFotoModel($datos)
    {
        $tabla = 'foto_perfil';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id_user = :id;
        INSERT INTO " . $tabla . " (nombre_foto, id_user) VALUES (:n,:id)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarNivelesModel($super_empresa)
    {
        $tabla = 'nivel';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $super_empresa);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
