<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloInventario extends conexion
{


    public function guardarInventarioModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " SELECT * FROM inventario_temp it
        WHERE it.estado = 1 AND it.activo = 1 AND it.user_log = :il AND it.id_super_empresa = :ids;
        INSERT INTO hoja_vida (id_inventario, id_super_empresa, fecha_update) SELECT it.id, it.id_super_empresa, it.fechareg FROM inventario it WHERE it.estado = 1
        AND it.activo = 1 AND it.user_log = :il AND it.id_super_empresa = :ids AND it.id NOT IN(SELECT h.id_inventario FROM hoja_vida h) AND it.id_categoria = 1;
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, id_super_empresa, estado) 
        SELECT it.id, it.id_user, it.id_area, it.user_log, it.id_super_empresa, it.estado FROM inventario it 
        WHERE it.estado = 1 AND it.activo = 1  AND it.id_super_empresa = :ids AND it.user_log = :il
        AND it.id NOT IN(SELECT h.id_inventario FROM inventario_log h);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function guardarEvidenciaModel($datos)
    {
        $tabla = 'evidencias';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " SELECT * FROM evidencias_temp ev
        WHERE ev.id_log = :il AND ev.id_super_empresa = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function actualizarTemporalModel($datos)
    {
        $tabla = 'inventario_temp';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE user_log = :il AND id_super_empresa = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function eliminarTemporalModel($datos)
    {
        $tabla = 'inventario_temp';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE user_log = :il AND id_super_empresa = :ids;
        DELETE FROM evidencias_temp WHERE id_log = :il AND id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function verificarCodigoModel($codigo)
    {
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT codigo FROM inventario WHERE codigo = :c
        UNION
        SELECT codigo FROM inventario_temp WHERE codigo = :c;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':c', $codigo);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function buscarInventarioModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
        COUNT(iv.id) AS cantidad,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area
        FROM " . $tabla . " iv WHERE iv.activo = 1 " . $datos['area'] . $datos['usuario'] . $datos['articulo'] . " 
        GROUP BY iv.descripcion, iv.id_area;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarCategoriasModel($super_empresa)
    {
        $tabla = 'categoria';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM categoria c WHERE c.activo = 1 AND c.id_super_empresa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function buscarInventarioDetalleModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area
        FROM " . $tabla . " iv WHERE iv.activo = 1 " . $datos['area'] . $datos['usuario'] . $datos['articulo'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function mostrarDatosTemporalesModel($id_log, $super_empresa)
    {
        $tabla = 'inventario_temp';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT t.*,
        COUNT(t.id) AS cantidad,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(t.id_user)) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id IN(t.estado)) AS estado
        FROM " . $tabla . " t WHERE t.estado = 1 AND t.activo = 0 AND t.user_log = :il AND t.id_super_empresa = :ids
        GROUP BY t.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':il', $id_log);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function guardarInventarioTempModel($datos)
    {
        $tabla = 'inventario_temp';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (descripcion, marca, modelo, precio, estado, fecha_compra, id_user, id_area, user_log, id_super_empresa
        , codigo, id_categoria) VALUES (:d,:mr,:md,:p,:e,:fc,:iu,:ia,:ul,:ids,:cd,:idc)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':mr', $datos['marca']);
            $preparado->bindParam(':md', $datos['modelo']);
            $preparado->bindParam(':p', $datos['precio']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':fc', $datos['fecha_compra']);
            $preparado->bindParam(':iu', $datos['id_user']);
            $preparado->bindParam(':ia', $datos['id_area']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            $preparado->bindParam(':cd', $datos['codigo']);
            $preparado->bindParam(':idc', $datos['id_categoria']);
            if ($preparado->execute()) {
                $id = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => TRUE, 'id' => $id);
                return $resultado;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function guardarEvidenciaTempModel($datos)
    {
        $tabla = 'evidencias_temp';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_inventario_temp, id_log, id_super_empresa) 
        VALUES (:n, :idt, :il, :ids)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idt', $datos['id_inventario_temp']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function liberarArticuloModel($datos)
    {
        $tabla = 'inventario_lib';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, id_log, id_super_empresa) 
        VALUES (:idv, :il, :ids);
        UPDATE inventario SET estado = :e, id_user = NULL WHERE id = :idv;
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) VALUES (:idv,:idu,:ida,:il,:e,:ids);
        INSERT INTO reportes (id_inventario, estado, id_user, id_log, id_area) VALUES (:idv,:e,:idu,:il,:ida);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindValue(':e', 4);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function descontinuarArticuloModel($datos)
    {
        $tabla = 'inventario_desc';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, id_log, id_super_empresa) 
        VALUES (:idv, :il, :ids);
        UPDATE inventario SET estado = :e, observacion = :ob WHERE id = :idv;
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) VALUES (:idv,:idu,:ida,:il,:e,:ids);
        INSERT INTO reportes (id_inventario, observacion, estado, id_user, id_log, id_area) VALUES (:idv,:ob,:e,:idu,:il,:ida);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindValue(':e', 5);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function mantenimientoArticuloModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
        INSERT INTO reportes (id_inventario, observacion, estado, id_user, id_log, tipo_reporte, fechareg, id_area) 
        VALUES	(:idv,:ob,:e,:idu,:idl,:tr,:fr,:ida);
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function reportarArticuloModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
        INSERT INTO reportes (id_inventario, observacion, estado, id_log, tipo_reporte, id_user, id_area) 
        VALUES(:idv,:ob,:e,:idl,:tr,:idu,:ida);
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) 
        VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function trabajoCasaArticuloModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
        INSERT INTO reportes (id_inventario, observacion, estado, id_log, tipo_reporte, id_user, id_area) 
        VALUES(:idv,:ob,:e,:idl,:tr,:idu,:ida);
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) 
        VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function removerTrabajoCasaArticuloModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
        INSERT INTO reportes (id_inventario, observacion, estado, id_log, tipo_reporte, id_user, id_area) 
        VALUES(:idv,:ob,:e,:idl,:tr,:idu,:ida);
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) 
        VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function informacionReporteModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT 
        rp.id,
        rp.id_log,
        rp.id_inventario,
        (SELECT iv.descripcion FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS descripcion,
        (SELECT iv.marca FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS marca,
        (SELECT iv.modelo FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS modelo,
        (SELECT (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS usuario,
        (SELECT (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS area,
        (SELECT iv.id_area FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS id_area,
        (SELECT e.nombre FROM estado e WHERE e.id IN(rp.estado)) AS estado,
        rp.observacion,
        rp.fechareg
        FROM " . $tabla . " rp WHERE rp.id_inventario = :idv ORDER BY rp.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idv', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarArticulosLiberadosModel($super_empresa)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS nombre_estado
        FROM " . $tabla . " iv WHERE iv.estado = 4 AND iv.activo = 1 AND iv.id_super_empresa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function reasignarArticuloModel($datos)
    {
        $tabla = 'inventario_log';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, id_user, id_area, id_log, id_super_empresa, estado) 
        VALUES (:idv,:idu,:ida,:idl,:ids,:e);
        UPDATE inventario SET id_user = :idu, id_area = :ida, estado = :e WHERE id = :idv;
        INSERT INTO reportes (id_inventario, estado, id_user, id_log, id_resp, id_reporte, fecha_respuesta) 
        VALUES (:idv,:e,:idu,:idl,:idr,:idp,:fr);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindValue(':e', 7);
            $preparado->bindParam(':idr', $datos['id_log']);
            $preparado->bindParam(':idp', $datos['id_reporte']);
            $preparado->bindParam(':fr', $datos['fecha_respuesta']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarEquipoComputoModel($super_empresa)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*, 
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
        (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
        IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg, 
        (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
        FROM " . $tabla . " iv WHERE iv.id_super_empresa = :ids AND iv.id_categoria = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarDatosArticulosModel($super_empresa)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*, 
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area
        FROM " . $tabla . " iv WHERE iv.id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarDatosArticuloIdModel($id)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area,
        (SELECT ev.nombre FROM evidencias ev WHERE ev.id_inventario = iv.id) AS evidencia
        FROM " . $tabla . " iv
        WHERE iv.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function historialArticuloModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT *,
        r.observacion AS observacion_reporte,
        r.fechareg AS fecha_reporte,
        r.estado AS estado_reporte,
        r.id AS id_reportado,
        (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = r.id_user) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id = r.estado) AS estado_nombre,
        (SELECT a.nombre FROM areas a WHERE a.id = r.id_area) AS area
        FROM " . $tabla . " r
        LEFT JOIN inventario iv ON r.id_inventario = iv.id
        WHERE r.id_inventario = :id;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function buscarReporteLiberadoModel($id)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_inventario = :id AND estado = 4 ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function historialReportesModel($super_empresa)
    {
        $tabla = 'reportes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT *,
        r.observacion AS observacion_reporte,
        r.fechareg AS fecha_reporte,
        (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = r.id_user) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id = r.estado) AS estado_nombre,
        (SELECT a.nombre FROM areas a WHERE a.id = r.id_area) AS area
        FROM " . $tabla . " r
        LEFT JOIN inventario iv ON r.id_inventario = iv.id
        WHERE iv.id_super_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function programarMantenimientoArticuloModel($datos)
    {
        $tabla = 'hoja_vida';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET frecuencia_mantenimiento = :fm, fecha_update = NOW() WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_inventario']);
            $preparado->bindValue(':fm', $datos['frec_mant']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function articulosComputoAreaControl($id)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*, 
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
        (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
        IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg, 
        (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
        FROM " . $tabla . " iv WHERE iv.id_area = :id AND iv.id_categoria = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarArticulosUsuarioModel($id)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*, 
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
        (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
        (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado,
        IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg, 
        (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
        FROM " . $tabla . " iv WHERE iv.id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function editarInventarioModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET descripcion = :d, marca = :mr, modelo = :md, precio = :pr, id_user = :idu, id_area = :ida 
        WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_inventario']);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':mr', $datos['marca']);
            $preparado->bindParam(':md', $datos['modelo']);
            $preparado->bindParam(':pr', $datos['precio']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function buscarAreaUsuarioControl($id)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT id_area FROM " . $tabla . " WHERE id_user = :id GROUP BY id_area;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function agregarMaterialTempModel($datos)
    {
        $tabla = 'inventario_temp';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (descripcion, estado, id_user, id_area, id_categoria, user_log, id_super_empresa, codigo, observacion)
        VALUES (:d, :e, :idu, :ida, :idc, :ul, :ids, :c, :ob);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idc', $datos['id_categoria']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindParam(':c', $datos['codigo']);
            $preparado->bindParam(':ob', $datos['observacion']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarMaterialDidacticoModel($id)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
        (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado,
        COUNT(id) AS cantidad
        FROM " . $tabla . " iv WHERE iv.id_super_empresa = :ids AND iv.id_categoria = 6 GROUP BY iv.descripcion, iv.id_user, iv.estado;;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function guardarMaterialControl($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO inventario SELECT * FROM inventario_temp WHERE id_super_empresa = :ids AND user_log = :idl AND id_categoria = 6;
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, id_super_empresa, estado) 
        SELECT it.id, it.id_user, it.id_area, it.user_log, it.id_super_empresa, it.estado FROM inventario it WHERE it.id_super_empresa = :ids
        AND it.user_log = :idl AND it.id_categoria = 6;
        DELETE FROM inventario_temp WHERE id_super_empresa = :ids AND user_log = :idl AND id_categoria = 6;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idl', $datos['id_log']);
            $preparado->bindValue(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function trabajoCasaModel($super_empresa)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT iv.*, 
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
        (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
        (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado
        FROM " . $tabla . " iv WHERE iv.estado = 8 AND iv.id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
