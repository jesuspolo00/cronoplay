<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPadres extends conexion
{
    public function guardarFormatoModel($datos)
    {
        $tabla = 'solicitud_ingreso';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            id_acudiente,
            grado_ap,
            nom_est,
            lug_nac,
            edad_est,
            col_est,
            grado_est,
            rel_est,
            num_hermanos,
            lug_ocup,
            nom_padre,
            dir_padre,
            tel_padre,
            ocup_padre,
            tel_of_padre,
            correo_padre,
            cel_padre,
            emp_padre,
            cargo_padre,
            nom_madre,
            dir_madre,
            tel_madre,
            ocup_madre,
            tel_of_madre,
            correo_madre,
            cel_madre,
            emp_madre,
            cargo_madre,
            vive_ambos,
            resp_est,
            nom_resp,
            paren_resp,
            dir_resp,
            tel_resp,
            cel_resp,
            fam_col,
            fam_curso,
            nom_medico,
            tel_medico,
            enf_est,
            cuid_esp,
            ayu_prof,
            profe_prof,
            cal_col,
            nom_cal_col,
            nom_ref,
            paren_ref,
            dir_ref,
            tel_ref,
            reco_jar,
            int_per,
            observacion,
            cual_esp,
            cual_enf,
            nom_prof,
            tel_prof
          )
          VALUES
            (
              '" . $datos['id_acudiente'] . "',
              '" . $datos['grado_ap'] . "',
              '" . $datos['nom_est'] . "',
              '" . $datos['lug_nac'] . "',
              '" . $datos['edad_est'] . "',
              '" . $datos['col_est'] . "',
              '" . $datos['grado_est'] . "',
              '" . $datos['rel_est'] . "',
              '" . $datos['num_hermanos'] . "',
              '" . $datos['lug_ocup'] . "',
              '" . $datos['nom_padre'] . "',
              '" . $datos['dir_padre'] . "',
              '" . $datos['tel_padre'] . "',
              '" . $datos['ocup_padre'] . "',
              '" . $datos['tel_of_padre'] . "',
              '" . $datos['correo_padre'] . "',
              '" . $datos['cel_padre'] . "',
              '" . $datos['emp_padre'] . "',
              '" . $datos['cargo_padre'] . "',
              '" . $datos['nom_madre'] . "',
              '" . $datos['dir_madre'] . "',
              '" . $datos['tel_madre'] . "',
              '" . $datos['ocup_madre'] . "',
              '" . $datos['tel_of_madre'] . "',
              '" . $datos['correo_madre'] . "',
              '" . $datos['cel_madre'] . "',
              '" . $datos['emp_madre'] . "',
              '" . $datos['cargo_madre'] . "',
              '" . $datos['vive_ambos'] . "',
              '" . $datos['resp_est'] . "',
              '" . $datos['nom_resp'] . "',
              '" . $datos['paren_resp'] . "',
              '" . $datos['dir_resp'] . "',
              '" . $datos['tel_resp'] . "',
              '" . $datos['cel_resp'] . "',
              '" . $datos['fam_col'] . "',
              '" . $datos['fam_curso'] . "',
              '" . $datos['nom_medico'] . "',
              '" . $datos['tel_medico'] . "',
              '" . $datos['enf_est'] . "',
              '" . $datos['cuid_esp'] . "',
              '" . $datos['ayu_prof'] . "',
              '" . $datos['profe_prof'] . "',
              '" . $datos['cal_col'] . "',
              '" . $datos['nom_cal_col'] . "',
              '" . $datos['nom_ref'] . "',
              '" . $datos['paren_ref'] . "',
              '" . $datos['dir_ref'] . "',
              '" . $datos['tel_ref'] . "',
              '" . $datos['reco_jar'] . "',
              '" . $datos['int_per'] . "',
              '" . $datos['observacion'] . "',
              '" . $datos['cual_esp'] . "',
              '" . $datos['cual_enf'] . "',
              '" . $datos['nom_prof'] . "',
              '" . $datos['tel_prof'] . "'
            );
          
          ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function guardarRecomendacionModel($datos)
    {
        $tabla = 'recomendacion';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_acudiente, nombre, id_formato) VALUES (:ida, :n, :idf)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ida', $datos['id_acudiente']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idf', $datos['id_formato']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function guardarDocumentoModel($datos)
    {
        $tabla = 'fotos_formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_acudiente, nombre, id_formato, tipo_foto) VALUES (:id,:n,:idf,:t);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_acudiente']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idf', $datos['id_formato']);
            $preparado->bindParam(':t', $datos['tipo']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function formatoLlenoModel($id)
    {
        $tabla = 'formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_acudiente) VALUES (:id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function consultarSolicitudIngresoModel($id)
    {
        $tabla = 'solicitud_ingreso';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " s WHERE s.id_acudiente = :id 
        AND  s.id_acudiente IN(SELECT f.id_acudiente FROM formato f 
        WHERE f.id_acudiente = s.id_acudiente AND f.activo = 1) ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function consultarIdFormatoModel($id)
    {
        $tabla = 'solicitud_ingreso';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " s WHERE s.id_acudiente = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function consultarFormatoLlenoModel($id)
    {
        $tabla = 'formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_acudiente = :id AND activo = 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function consultarFormatoRecomendacionModel($id)
    {
        $tabla = 'recomendacion';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " r 
        WHERE r.id_formato = :id
        AND r.id_acudiente IN(SELECT f.id_acudiente FROM formato f WHERE f.activo = 1) 
        ORDER BY r.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function fotosFormatoModel($id)
    {
        $tabla = 'fotos_formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_formato = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function archivoRecomendacionModel($id, $formato)
    {
        $tabla = 'recomendacion';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM recomendacion WHERE id_acudiente = :id AND id_formato = :f ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':f', $formato);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function fotoEstudianteModel($id, $formato)
    {
        $tabla = 'fotos_formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_acudiente = :id AND id_formato = :f AND tipo_foto IN('fot_est') ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':f', $formato);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function fotoPadreModel($id, $formato)
    {
        $tabla = 'fotos_formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_acudiente = :id AND id_formato = :f AND tipo_foto IN('fot_padre') ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':f', $formato);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function fotoMadreModel($id, $formato)
    {
        $tabla = 'fotos_formato';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_acudiente = :id AND id_formato = :f AND tipo_foto IN('fot_madre') ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':f', $formato);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function datosPadresModelo($id)
    {
        $tabla = 'usuarios';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function confirmarFormatoSolicitudModel($datos)
    {
        var_dump($datos);
        $tabla = 'solicitud_ingreso';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
        grado_ap = '" . $datos['grado_ap'] . "',
        nom_est = '" . $datos['nom_est'] . "',
        lug_nac = '" . $datos['lug_nac'] . "',
        edad_est = '" . $datos['edad_est'] . "',
        col_est = '" . $datos['col_est'] . "',
        grado_est = '" . $datos['grado_est'] . "',
        rel_est = '" . $datos['rel_est'] . "',
        num_hermanos = '" . $datos['num_hermanos'] . "',
        lug_ocup = '" . $datos['lug_ocup'] . "',
        nom_padre = '" . $datos['nom_padre'] . "',
        dir_padre = '" . $datos['dir_padre'] . "',
        tel_padre = '" . $datos['tel_padre'] . "',
        ocup_padre = '" . $datos['ocup_padre'] . "',
        tel_of_padre = '" . $datos['tel_of_padre'] . "',
        correo_padre = '" . $datos['correo_padre'] . "',
        cel_padre = '" . $datos['cel_padre'] . "',
        emp_padre = '" . $datos['emp_padre'] . "',
        cargo_padre = '" . $datos['cargo_padre'] . "',
        nom_madre = '" . $datos['nom_madre'] . "',
        dir_madre = '" . $datos['dir_madre'] . "',
        tel_madre = '" . $datos['tel_madre'] . "',
        ocup_madre = '" . $datos['ocup_madre'] . "',
        tel_of_madre = '" . $datos['tel_of_madre'] . "',
        correo_madre = '" . $datos['correo_madre'] . "',
        cel_madre = '" . $datos['cel_madre'] . "',
        emp_madre = '" . $datos['emp_madre'] . "',
        cargo_madre = '" . $datos['cargo_madre'] . "',
        vive_ambos = '" . $datos['vive_ambos'] . "',
        resp_est = '" . $datos['resp_est'] . "',
        nom_resp = '" . $datos['nom_resp'] . "',
        paren_resp = '" . $datos['paren_resp'] . "',
        dir_resp = '" . $datos['dir_resp'] . "',
        tel_resp = '" . $datos['tel_resp'] . "',
        cel_resp = '" . $datos['cel_resp'] . "',
        fam_col = '" . $datos['fam_col'] . "',
        fam_curso = '" . $datos['fam_curso'] . "',
        nom_medico = '" . $datos['nom_medico'] . "',
        tel_medico = '" . $datos['tel_medico'] . "',
        enf_est = '" . $datos['enf_est'] . "',
        cuid_esp = '" . $datos['cuid_esp'] . "',
        ayu_prof = '" . $datos['ayu_prof'] . "',
        profe_prof = '" . $datos['profe_prof'] . "',
        cal_col = '" . $datos['cal_col'] . "',
        nom_cal_col = '" . $datos['nom_cal_col'] . "',
        nom_ref = '" . $datos['nom_ref'] . "',
        paren_ref = '" . $datos['paren_ref'] . "',
        dir_ref = '" . $datos['dir_ref'] . "',
        tel_ref = '" . $datos['tel_ref'] . "',
        reco_jar = '" . $datos['reco_jar'] . "',
        int_per = '" . $datos['int_per'] . "',
        observacion = '" . $datos['observacion'] . "',
        confirmado = '" . $datos['confirmado'] . "'
      WHERE id = :id AND id_acudiente = :idc";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_formato']);
            $preparado->bindValue(':idc', $datos['id_acudiente']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function confirmarRecomendacionModel($datos)
    {
        $tabla = 'recomendacion';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
        fam_recomend = '" . $datos['fam_recomend'] . "',
        anio_conoce = '" . $datos['anio_conoce'] . "',
        nom_est = '" . $datos['nom_est'] . "',
        grado_asp = '" . $datos['grado_asp'] . "',
        vincu_union = '" . $datos['vincu_union'] . "',
        cump_filo = '" . $datos['considero'] . "',
        vinculo_une = '" . $datos['vinculo'] . "',
        confirmado = '" . $datos['confirmado'] . "'
      WHERE id = :id  AND id_formato = :idf;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_recomendacion']);
            $preparado->bindValue(':idf', $datos['id_formato']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
