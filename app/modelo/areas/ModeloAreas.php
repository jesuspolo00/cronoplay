<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloAreas extends conexion
{


    public function guardarAreaModel($datos)
    {
        $tabla = 'areas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, user_log, id_super_empresa) VALUES (:n,:ul,:ids)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function editarAreaModel($datos)
    {
        $tabla = 'areas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindValue(':id', $datos['id_area']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarAreasModel($super_empresa)
    {
        $tabla = 'areas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function usuarioResponsableModel($id_area)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = a.id_user) AS usuario 
        FROM " . $tabla . " a WHERE a.id_area = :id GROUP BY a.id_user, a.id_area;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id_area);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function asignarAreaModel($datos)
    {
        $tabla = 'inventario';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_user = :idu WHERE id_area = :ida";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindValue(':ida', $datos['id_area']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
